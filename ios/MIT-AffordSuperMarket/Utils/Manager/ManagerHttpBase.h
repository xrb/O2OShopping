//
//  ManagerHttpBase.h
//  MIT_itrade
//
//  Created by wanc on 14-4-2.
//  Copyright (c) 2014年 ___ZhengDaXinXi___. All rights reserved.
//

/*********************************
 文件名: ManagerHttpBase.h
 功能描述: 请求数据基类
 创建人: GT
 修改日期: 2015.10.30
 *********************************/

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
@interface ManagerHttpBase : AFHTTPRequestOperationManager
+ (instancetype)sharedManager;
- (void)managerReachability;

- (BOOL)isRegister;

//只提供成功块的基础请求接口
- (AFHTTPRequestOperation*)parameters:(NSMutableDictionary *)parameters
                           customPOST:(NSString*)postAddress
                              success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success;
//将错误块开放的基础请求接口
- (AFHTTPRequestOperation*)parameters:(NSMutableDictionary *)parameters
                           customPOST:(NSString*)postAddress
                              success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                              failure:(void (^)(bool isFailure))failure;


//注册请求接口
- (AFHTTPRequestOperation*)rigisterAppSuccess:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                                      failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

//检测更新
- (AFHTTPRequestOperation*)rigisterTestingUpdate:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                                         failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;
@end

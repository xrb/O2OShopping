//
//  UIImage+ColorToImage.m
//  MIT_Integrated
//
//  Created by apple on 15/9/15.
//  Copyright (c) 2015年 apple. All rights reserved.
//

#import "UIImage+ColorToImage.h"
#import "NSString+Conversion.h"
#import "Global.h"
@implementation UIImage (ColorToImage)
+(UIImage*) createImageWithColor:(UIColor*) color frame:(CGRect)frame
{
    UIGraphicsBeginImageContext(frame.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, frame);
    UIImage *theImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return theImage;
}
+(UIImage*) createImageWithImageUrlString:(NSString*)urlStr {
    NSString *appendUrlStr = [NSString stringWithFormat:@"%@%@",baseImageUrl,[NSString stringISNull:urlStr]];
    NSURL *imageUrl = [NSURL URLWithString:appendUrlStr];
    NSData *imageData = [NSData dataWithContentsOfURL:imageUrl];
    return [UIImage imageWithData:imageData];
}
@end

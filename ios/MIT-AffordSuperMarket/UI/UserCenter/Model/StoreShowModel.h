//
//  StoreShowModel.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/12/10.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： StoreShowModel
 Created_Date： 20151210
 Created_People： GT
 Function_description： 门店展示CellModel
 ***************************************/

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface StoreShowModel : NSObject
@property(nonatomic, strong)NSString *content;
@property(nonatomic, strong)NSString *buttonImageName;
@property (nonatomic, assign, readonly) CGFloat cellHeight;
+ (instancetype)messageWithDic:(NSDictionary *)dic;
@end

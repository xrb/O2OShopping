//
//  ReceivedAddressModel.m
//
//  Created by apple  on 16/1/25
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "ReceivedAddressModel.h"
#import "ReceivedAddressOBJECT.h"


NSString *const kReceivedAddressModelMSG = @"MSG";
NSString *const kReceivedAddressModelSTATE = @"STATE";
NSString *const kReceivedAddressModelOBJECT = @"OBJECT";


@interface ReceivedAddressModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation ReceivedAddressModel

@synthesize mSG = _mSG;
@synthesize sTATE = _sTATE;
@synthesize oBJECT = _oBJECT;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.mSG = [self objectOrNilForKey:kReceivedAddressModelMSG fromDictionary:dict];
            self.sTATE = [self objectOrNilForKey:kReceivedAddressModelSTATE fromDictionary:dict];
    NSObject *receivedReceivedAddressOBJECT = [dict objectForKey:kReceivedAddressModelOBJECT];
    NSMutableArray *parsedReceivedAddressOBJECT = [NSMutableArray array];
    if ([receivedReceivedAddressOBJECT isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedReceivedAddressOBJECT) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedReceivedAddressOBJECT addObject:[ReceivedAddressOBJECT modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedReceivedAddressOBJECT isKindOfClass:[NSDictionary class]]) {
       [parsedReceivedAddressOBJECT addObject:[ReceivedAddressOBJECT modelObjectWithDictionary:(NSDictionary *)receivedReceivedAddressOBJECT]];
    }

    self.oBJECT = [NSArray arrayWithArray:parsedReceivedAddressOBJECT];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.mSG forKey:kReceivedAddressModelMSG];
    [mutableDict setValue:self.sTATE forKey:kReceivedAddressModelSTATE];
    NSMutableArray *tempArrayForOBJECT = [NSMutableArray array];
    for (NSObject *subArrayObject in self.oBJECT) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForOBJECT addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForOBJECT addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForOBJECT] forKey:kReceivedAddressModelOBJECT];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.mSG = [aDecoder decodeObjectForKey:kReceivedAddressModelMSG];
    self.sTATE = [aDecoder decodeObjectForKey:kReceivedAddressModelSTATE];
    self.oBJECT = [aDecoder decodeObjectForKey:kReceivedAddressModelOBJECT];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_mSG forKey:kReceivedAddressModelMSG];
    [aCoder encodeObject:_sTATE forKey:kReceivedAddressModelSTATE];
    [aCoder encodeObject:_oBJECT forKey:kReceivedAddressModelOBJECT];
}

- (id)copyWithZone:(NSZone *)zone
{
    ReceivedAddressModel *copy = [[ReceivedAddressModel alloc] init];
    
    if (copy) {

        copy.mSG = [self.mSG copyWithZone:zone];
        copy.sTATE = [self.sTATE copyWithZone:zone];
        copy.oBJECT = [self.oBJECT copyWithZone:zone];
    }
    
    return copy;
}


@end

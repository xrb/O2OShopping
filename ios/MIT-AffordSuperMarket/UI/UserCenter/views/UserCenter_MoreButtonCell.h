//
//  UserCenter_MoreButtonCell.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/16.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： UserCenter_MoreButtonCell
 Created_Date： 20151108
 Created_People： GT
 Function_description： 个人中心服务订单特殊cell
 ***************************************/

#import <UIKit/UIKit.h>
@protocol UserCenter_MoreButtonCellDelegate <NSObject>
- (void)selectMoreButtonCellItemWithIndex:(NSInteger)index;
@end

@interface UserCenter_MoreButtonCell : UITableViewCell
@property(nonatomic, unsafe_unretained)id<UserCenter_MoreButtonCellDelegate>moreButtonCellDelegate;
- (void)updateBorderStatus:(NSDictionary*)dataDic orderType:(NSString *)orderType;
-(instancetype)initWithStyle:(UITableViewCellStyle)style titles:(NSArray*)titles images:(NSArray*)images tag:(NSArray*)tag reuseIdentifier:(NSString *)reuseIdentifier;
@end

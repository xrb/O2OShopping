
#import "Global.h"
#import "UIColor+Hex.h"
#define kleftSpace 15
#define kcellHeight HightScalar(54)
#import "TitleValueMoreCell.h"

@interface TitleValueMoreCell ()
@property (strong, nonatomic) UILabel *titleLabel, *valueLabel;
@end

@implementation TitleValueMoreCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        if (!_titleLabel) {
            _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(kleftSpace, 0, 170, kcellHeight)];
            _titleLabel.backgroundColor = [UIColor clearColor];
            _titleLabel.font = [UIFont systemFontOfSize:FontSize(17)];
            _titleLabel.textColor = [UIColor blackColor];
            [self.contentView addSubview:_titleLabel];
        }
        if (!_valueLabel) {
            _valueLabel = [[UILabel alloc] initWithFrame:CGRectMake(120, 0, VIEW_WIDTH-(110+kleftSpace) - 30, kcellHeight)];
            _valueLabel.backgroundColor = [UIColor clearColor];
            _valueLabel.font = [UIFont systemFontOfSize:FontSize(15)];
            _valueLabel.textColor = [UIColor grayColor];
            _valueLabel.textAlignment = NSTextAlignmentRight;
            [self.contentView addSubview:_valueLabel];
        }
    }
    return self;
}


- (void)setTitleStr:(NSString *)title valueStr:(NSString *)value{
    _titleLabel.text = title;
    _valueLabel.text = value;
}


@end

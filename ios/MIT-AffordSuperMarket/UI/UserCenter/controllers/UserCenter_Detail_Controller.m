//
//  UserCenter_Detail_Controller.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/9.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "UserCenter_Detail_Controller.h"
#define DATA_DIC_LINE_KEY @"DATA_DIC_LINE_KEY"

#define DATA_DIC_BACK_IMG_KEY @"DATA_DIC_BACK_IMG_KEY"
#define DATA_DIC_ICON_KEY @"DATA_DIC_ICON_KEY"
#define DATA_DIC_TITLE_KEY @"DATA_DIC_TITLE_KEY"
#define DATA_DIC_CLASS_KEY @"DATA_DIC_CLASS_KEY"
#define DATA_DIC_JIANTOU_KEY @"DATA_DIC_JIANTOU_KEY"
//untils
#import "UIColor+Hex.h"
#import "ManagerGlobeUntil.h"
//views
#import "UserCenter_ListCell.h"
//controllers
#import "RegisterOneStepViewController.h"
#import "RegisterTwoStepViewController.h"
#import "UserInfo_Controller.h"
@interface UserCenter_Detail_Controller ()
@property (nonatomic,strong)UITableView *tableView;
@property (nonatomic,strong)NSMutableArray *listDatasArr;
@end

@implementation UserCenter_Detail_Controller

- (void)viewDidLoad {
    [super viewDidLoad];
   self.view.backgroundColor = [UIColor colorWithHexString:@"#efeeee"];
    self.title = @"个人中心";
    [self addBackNavItem];
    [self optionDataSorece];
    [self optionTableView];
}
#pragma mark -- 初始化数据
- (void)optionDataSorece {
    _listDatasArr = [[NSMutableArray alloc]init];
    
    NSMutableArray *sectionArr = [[NSMutableArray alloc]init];
    
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
    [dic setObject:@"YES" forKey:DATA_DIC_LINE_KEY];
    [dic setObject:@"YES" forKey:DATA_DIC_JIANTOU_KEY];
    [dic setObject:@"box-border-top" forKey:DATA_DIC_BACK_IMG_KEY];
    [dic setObject:@"userCenter_xinxi" forKey:DATA_DIC_ICON_KEY];
    [dic setObject:@"个人信息" forKey:DATA_DIC_TITLE_KEY];
    [dic setObject:NSStringFromClass([UserInfo_Controller class]) forKey:DATA_DIC_CLASS_KEY];
    [sectionArr addObject:dic];
    
    
    dic = [[NSMutableDictionary alloc]init];
    [dic setObject:@"YES" forKey:DATA_DIC_LINE_KEY];
    [dic setObject:@"YES" forKey:DATA_DIC_JIANTOU_KEY];
    [dic setObject:@"box-border-mid" forKey:DATA_DIC_BACK_IMG_KEY];
    [dic setObject:@"userCenter_mima" forKey:DATA_DIC_ICON_KEY];
    [dic setObject:@"修改密码" forKey:DATA_DIC_TITLE_KEY];
    [dic setObject:NSStringFromClass([RegisterOneStepViewController class]) forKey:DATA_DIC_CLASS_KEY];
    [sectionArr addObject:dic];
    
    
    
    dic = [[NSMutableDictionary alloc]init];
    [dic setObject:@"NO" forKey:DATA_DIC_LINE_KEY];
    [dic setObject:@"YES" forKey:DATA_DIC_JIANTOU_KEY];
    [dic setObject:@"box-border-bottom" forKey:DATA_DIC_BACK_IMG_KEY];
    [dic setObject:@"userCenter_Phone" forKey:DATA_DIC_ICON_KEY];
    [dic setObject:@"变更手机号码" forKey:DATA_DIC_TITLE_KEY];
    [dic setObject:NSStringFromClass([RegisterOneStepViewController class]) forKey:DATA_DIC_CLASS_KEY];
    [sectionArr addObject:dic];
    
    [_listDatasArr addObject:sectionArr];
    
    sectionArr = [[NSMutableArray alloc]init];
    
    dic = [[NSMutableDictionary alloc]init];
    [dic setObject:@"NO" forKey:DATA_DIC_LINE_KEY];
    [dic setObject:@"NO" forKey:DATA_DIC_JIANTOU_KEY];
    [dic setObject:@"login_Btn" forKey:DATA_DIC_BACK_IMG_KEY];
    [dic setObject:@"         安全退出" forKey:DATA_DIC_TITLE_KEY];
    [sectionArr addObject:dic];
    [_listDatasArr addObject:sectionArr];
}
#pragma mark -- 初始化视图
- (void)optionTableView {
    CGRect frame = CGRectMake(CGRectGetMinX(self.view.bounds) + 10,
                       CGRectGetMinY(self.view.bounds),
                       CGRectGetWidth(self.view.bounds) - 20,
                       CGRectGetHeight(self.view.bounds));
    _tableView = [[UITableView alloc] initWithFrame:frame style:UITableViewStylePlain];
    _tableView.delegate = (id<UITableViewDelegate>)self;
    _tableView.dataSource = (id<UITableViewDataSource>)self;
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [_tableView  setShowsHorizontalScrollIndicator:NO];
    [self.view addSubview:_tableView];

}

#pragma mark -- UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [_listDatasArr count];
}

- (NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section
{
    NSMutableArray *arr = [_listDatasArr objectAtIndex:section];
    return [arr count];
}

- (UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"AdministrateViewController";
    UserCenter_ListCell *cell = (UserCenter_ListCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UserCenter_ListCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor clearColor];

    }
    
    NSMutableArray *arr = [_listDatasArr objectAtIndex:indexPath.section];
    NSDictionary *dic = [arr objectAtIndex:indexPath.row];
    cell.contentTextLabel.text = [dic objectForKey:DATA_DIC_TITLE_KEY];
    
    //beijing
    cell.backImgView.image = [UIImage imageNamed:[dic objectForKey:DATA_DIC_BACK_IMG_KEY]];
    
    if (indexPath.section ==1) {
        cell.contentTextLabel.font = [UIFont systemFontOfSize:22];
        cell.contentTextLabel.textColor = [UIColor whiteColor];
    }
    
    //xian
    if ([[dic objectForKey:DATA_DIC_LINE_KEY] isEqualToString:@"YES"]) {
        cell.lineImgView.image = [UIImage imageNamed:@"news_line_center"];
    }
    //jiantou
    if ([[dic objectForKey:DATA_DIC_JIANTOU_KEY] isEqualToString:@"NO"]) {
        cell.jiantouImgView.hidden = YES;
    }
    //img
    cell.iconImgView.image = [UIImage imageNamed:[dic objectForKey:DATA_DIC_ICON_KEY]];
    return cell;
}

#pragma mark -- UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return 10;
    }
    return CGRectGetHeight(self.view.bounds)-200;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 1;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UIView *views = [[UIView alloc]init];
    views.backgroundColor = [UIColor clearColor];
    return views;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *views = [[UIView alloc]init];
    views.backgroundColor = [UIColor clearColor];
    return views;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSMutableArray *arr = [_listDatasArr objectAtIndex:indexPath.section];
    NSDictionary *dic = [arr objectAtIndex:indexPath.row];
    
    NSString *classStr = [dic objectForKey:DATA_DIC_CLASS_KEY];
    if (classStr) {
        
        UIViewController *ctrl = nil;
        if ([classStr isEqualToString:NSStringFromClass([RegisterOneStepViewController class])])
        {
            RegisterOneStepViewController *registerCtrl = [[RegisterOneStepViewController alloc]init];
            if (indexPath.row == 1) {//修改密码
                registerCtrl.typeOfView = OneStepIsModifiedPassword;
            }else if(indexPath.row == 2){//变更手机号
                registerCtrl.typeOfView = OneStepIsModifiedPhoneNum;
            }
            
            ctrl = registerCtrl;
        }
        else
        {
            ctrl = [[NSClassFromString(classStr) alloc]init];
        }
        
        [self.navigationController pushViewController:ctrl animated:YES];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示"
                                                       message:@"确定要退出登录吗？"
                                                      delegate:(id<UIAlertViewDelegate>)self
                                             cancelButtonTitle:@"取消"
                                             otherButtonTitles:@"确定",nil];
        [alert show];
    }
}

#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
        {
            break;
        }
        case 1:
        {
            //退出登录
            [ManagerGlobeUntil sharedManager].transactionLogin = NO;
            [self.navigationController popToRootViewControllerAnimated:YES];
            break;
        }
            
        default:
            break;
    }
}

-(void)backBtnAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
@end

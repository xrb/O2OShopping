//
//  ManageReceivedAddressController.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/18.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： ManageReceivedAddressController
 Created_Date： 20151117
 Created_People： GT
 Function_description： 管理收货地址页面
 ***************************************/

#import "BaseViewController.h"
typedef NS_ENUM(NSInteger, ManageReceivedAddressControllerSupperControllerType) {
    kUserCenterPushToManageReceivedAddressController = 0,//从个人中心push
    kSureOrderPushToManageReceivedAddressController //从确认订单push
};
@interface ManageReceivedAddressController : BaseViewController
@property(nonatomic)ManageReceivedAddressControllerSupperControllerType supperControllerType;
@end

//
//  ReSetNicknameController.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/18.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： ReSetNicknameController
 Created_Date： 20151117
 Created_People： GT
 Function_description： 设置昵称页面
 ***************************************/

#import "BaseViewController.h"

@interface ReSetNicknameController : BaseViewController
@property(nonatomic, strong)NSString *textValue;
@property(copy, nonatomic)void(^doneBlock)(NSString *textValue);
+ (instancetype)settingTextVCWithTitle:(NSString *)title textValue:(NSString *)textValue doneBlock:(void(^)(NSString *textValue))block;
@end

//
//  MyShiHuiBiHeadView.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/5/4.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "MyShiHuiBiHeadView.h"
#import "UIColor+Hex.h"
#import "NSString+TextSize.h"
#import "Global.h"
@interface MyShiHuiBiHeadView()
@property (nonatomic, strong)UILabel *shiHuiMoney;
@property (nonatomic, strong)UILabel *moneyDetails;
@end
@implementation MyShiHuiBiHeadView
-(instancetype)initWithFrame:(CGRect)frame
{
    self =[super initWithFrame:frame];
    if(self){
        [self setSubView];
        self.backgroundColor =[UIColor colorWithHexString:@"#343333"];
    }
    return self;
}
-(void)setSubViewDataWithMoneyMun:(NSString *)money
{
    money = [NSString stringWithFormat:@"%.2f",[money floatValue]];
    NSString *moneyText =[NSString stringWithFormat:@"%@ 实惠币",money];
    CGSize shiHuiMoneySize =[moneyText sizeWithFont:[UIFont systemFontOfSize:FontSize(50)] maxSize:CGSizeMake(VIEW_WIDTH, HightScalar(60))];
    _shiHuiMoney.frame= CGRectMake(30, 16, shiHuiMoneySize.width,  shiHuiMoneySize.height);
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:moneyText];
    [str addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:FontSize(50)] range:NSMakeRange(0, money.length)];
  
    _shiHuiMoney.attributedText =str;
    NSString *moneyDetailsText =@"可购物，可兑换现金，用处多多";
    CGSize moneyDetailsSize =[moneyDetailsText sizeWithFont:[UIFont systemFontOfSize:FontSize(16)] maxSize:CGSizeMake(VIEW_WIDTH-30, HightScalar(20))];
    _moneyDetails.frame = CGRectMake(CGRectGetMinX(_shiHuiMoney.frame), CGRectGetMaxY(_shiHuiMoney.frame)+5, moneyDetailsSize.width,  moneyDetailsSize.height);
    _moneyDetails.text = moneyDetailsText;
    
    
}
#pragma mark --  初始化视图
-(void)setSubView
{
    //金额
    _shiHuiMoney = [[UILabel alloc]initWithFrame:CGRectZero];
    _shiHuiMoney.font = [UIFont systemFontOfSize:FontSize(16)];
    _shiHuiMoney.textColor = [UIColor whiteColor];
    _shiHuiMoney.text=@"0";
    [self addSubview:_shiHuiMoney];
    //描述
    _moneyDetails =  [[UILabel alloc]initWithFrame:CGRectZero];
    _moneyDetails.font = [UIFont systemFontOfSize:FontSize(16)];
    _moneyDetails.textColor = [UIColor whiteColor];
    
    [self addSubview:_moneyDetails];
    
}


@end

//
//  Home_TableView_SubjectCollectionCell.m
//  DaShiHuiHome
//
//  Created by apple on 16/2/18.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "Home_TableView_SubjectCollectionCell.h"
//untils
#import "UIColor+Hex.h"
#import "Global.h"
#import "NSString+TextSize.h"
#import "UIImage+ColorToImage.h"
#import "NSString+Conversion.h"
#import "UIColor+Hex.h"
//vendor
#import "UIImageView+AFNetworking.h"
@interface Home_TableView_SubjectCollectionCell()
@property(nonatomic, strong)UIImageView *imageView;
@property(nonatomic, strong)UILabel *titleLabel;
@property(nonatomic, strong)UILabel *currentPriceLabel;
@property(nonatomic, strong)UILabel *oldPriceLabel;
@property(nonatomic, strong)UIButton *inToShopCar;
@end
@implementation Home_TableView_SubjectCollectionCell
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
        [self setupSubView];
        
    }
    
    return self;
}


#pragma makr -- 初始化子视图
- (void)setupSubView {
    //图片
    _imageView = [[UIImageView alloc]init];
     _imageView.frame =CGRectMake(0, 0, self.bounds.size.width -8,  self.bounds.size.width - 8);
    _imageView.image = [UIImage imageNamed:@"place_image"];
    [self.contentView addSubview:_imageView];
        //标题
    _titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMinX(_imageView.frame)+3, CGRectGetMaxY(_imageView.frame)+5, CGRectGetWidth(_imageView.bounds)-6, HightScalar(47))];
    _titleLabel.text = @"纯天然有机胡萝卜";
    _titleLabel.font = [UIFont systemFontOfSize:FontSize(13)];
    _titleLabel.textAlignment = NSTextAlignmentLeft;
    _titleLabel.numberOfLines =2;
    _titleLabel.textColor = [UIColor colorWithHexString:@"#000000"];
    [self.contentView addSubview:_titleLabel];
    
    //当前价格
    _currentPriceLabel = [[UILabel alloc]init];
    _currentPriceLabel.frame= CGRectMake(CGRectGetMinX(_titleLabel.frame), CGRectGetMaxY(_titleLabel.frame) , CGRectGetWidth(_titleLabel.bounds), HightScalar(17));
    _currentPriceLabel.text = @"￥8.00";
    _currentPriceLabel.font = [UIFont systemFontOfSize:FontSize(14)];
    _currentPriceLabel.textAlignment = NSTextAlignmentLeft;
    _currentPriceLabel.textColor = [UIColor colorWithHexString:@"#e20000"];
    [self.contentView addSubview:_currentPriceLabel];
    
    //优惠前价格
    NSString *olePrice = @"￥12.00";
    _oldPriceLabel = [[UILabel alloc]init];
    _oldPriceLabel.frame = CGRectMake(CGRectGetMinX(_currentPriceLabel.frame)+4, CGRectGetMaxY(_currentPriceLabel.frame) , CGRectGetWidth(_currentPriceLabel.bounds), HightScalar(14));
    _oldPriceLabel.font = [UIFont systemFontOfSize:FontSize(13)];
    _oldPriceLabel.textAlignment = NSTextAlignmentLeft;
    _oldPriceLabel.textColor = [UIColor colorWithHexString:@"#494949"];
    [self.contentView addSubview:_oldPriceLabel];
    
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:olePrice];
    [attrString addAttribute:NSStrikethroughStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlinePatternSolid | NSUnderlineStyleSingle] range:NSMakeRange(0, olePrice.length)];
    
    _oldPriceLabel.attributedText = attrString;
    
    //竖直分割线
    _leftSepLine = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_imageView.frame) + 7.3
                                                                           , CGRectGetMinY(_imageView.frame)
                                                                           , 0.6
                                                                            , CGRectGetHeight(self.bounds))];
    _leftSepLine.image =[UIImage createImageWithColor:[UIColor colorWithHexString:@"#dddddd"] frame:CGRectMake(0, 0, 0.7, 1)];
    [self.contentView addSubview:_leftSepLine];
       
    }
- (void)updateViewWithData:(NSDictionary*)dataDic
{
    if ([dataDic count] != 0) {
        [self.imageView setImageWithURL:[NSURL URLWithString:[NSString appendImageUrlWithServerUrl:[NSString stringTransformObject:[dataDic objectForKey:@"THUMB"]]]]placeholderImage:[UIImage imageNamed:@"place_image"]];
        self.titleLabel.text = [NSString stringISNull:[dataDic objectForKey:@"NAME"]];
        self.currentPriceLabel.text =[NSString stringWithFormat:@"￥%.2f", [[NSString stringTransformObject:[dataDic objectForKey:@"SELLPRICE"]] floatValue]];
        NSString *oldPriceStr =[NSString stringWithFormat:@"￥%.2f",[[NSString stringTransformObject:[dataDic objectForKey:@"MARKETPRICE"]] floatValue]];
        NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:oldPriceStr];
        [attrString addAttribute:NSStrikethroughStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlinePatternSolid | NSUnderlineStyleSingle] range:NSMakeRange(0, oldPriceStr.length)];
        self.oldPriceLabel.attributedText = attrString;
        
    }
}
@end

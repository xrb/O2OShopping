//
//  Home_Root_CollectionViewCell.m
//  DaShiHuiHome
//
//  Created by apple on 16/2/17.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "Home_Root_CollectionViewCell.h"
//untils
#import "UIColor+Hex.h"
#import "Global.h"
#import "NSString+TextSize.h"
#import "UIImage+ColorToImage.h"
#import "NSString+Conversion.h"
//vendor
#import "Masonry.h"
#import "UIImageView+AFNetworking.h"
#import "KZLinkLabel.h"
#define kimageHight HightScalar(171)
#define kimageWidth HightScalar(171)
@interface Home_Root_CollectionViewCell ()
@property(nonatomic, strong)UIImageView *imageView;
@property(nonatomic, strong)KZLinkLabel *titleLabel;
@property(nonatomic, strong)UILabel *currentPriceLabel;
@property(nonatomic, strong)UILabel *oldPriceLabel;
@property(nonatomic, strong)UIButton *inToShopCar;
@end
@implementation Home_Root_CollectionViewCell
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
        [self setupSubView];
        
    }
    
    return self;
}


#pragma makr -- 初始化子视图
- (void)setupSubView {
//    //点击背景颜色
//    UIView *cellSelectView =[[UIView alloc]initWithFrame:self.bounds];
//    cellSelectView.backgroundColor=[UIColor colorWithHexString:@"#dddddd"];
//    self.selectedBackgroundView =cellSelectView;
    
    //图片
    _imageView = [[UIImageView alloc]init];
    _imageView.image = [UIImage imageNamed:@"place_image"];
    [self.contentView addSubview:_imageView];
    [_imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.top.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(kimageHight);
    }];
    //标题
    _titleLabel = [[KZLinkLabel alloc]init];
    _titleLabel.text = @"纯天然有机胡萝卜";
    _titleLabel.font = [UIFont systemFontOfSize:FontSize(14)];
    _titleLabel.textAlignment = NSTextAlignmentLeft;
    _titleLabel.numberOfLines =2;
    _titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    [_titleLabel sizeToFit];
    _titleLabel.textColor = [UIColor colorWithHexString:@"#000000"];
    [self.contentView addSubview:_titleLabel];
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_imageView).offset(5);
        make.right.equalTo(_imageView).offset(-5);
        make.top.equalTo(_imageView.mas_bottom).offset(5);
        make.height.mas_equalTo(HightScalar(40));
    }];
    //当前价格
    _currentPriceLabel = [[UILabel alloc]init];
    _currentPriceLabel.text = @"￥8.00";
    _currentPriceLabel.font = [UIFont systemFontOfSize:FontSize(14)];
    _currentPriceLabel.textAlignment = NSTextAlignmentLeft;
    _currentPriceLabel.textColor = [UIColor colorWithHexString:@"#e20000"];
    [self.contentView addSubview:_currentPriceLabel];
    [_currentPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_titleLabel);
        make.bottom.mas_equalTo(HightScalar(-16));
        make.width.mas_equalTo(80);
        make.height.mas_equalTo(HightScalar(15));
    }];
    //优惠前价格
    NSString *olePrice = @"￥12.00";
    _oldPriceLabel = [[UILabel alloc]init];
    _oldPriceLabel.font = [UIFont systemFontOfSize:FontSize(13)];
    _oldPriceLabel.textAlignment = NSTextAlignmentLeft;
    _oldPriceLabel.textColor = [UIColor colorWithHexString:@"#494949"];
    [self.contentView addSubview:_oldPriceLabel];
    
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:olePrice];
    [attrString addAttribute:NSStrikethroughStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlinePatternSolid | NSUnderlineStyleSingle] range:NSMakeRange(0, olePrice.length)];
    
    _oldPriceLabel.attributedText = attrString;
    [_oldPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_currentPriceLabel).offset(4);
        make.width.equalTo(_currentPriceLabel);
        make.bottom.mas_equalTo(HightScalar(-4));
        make.height.mas_equalTo(HightScalar(12));
    }];
    //加入购物车按钮
    _inToShopCar = [UIButton  buttonWithType:UIButtonTypeCustom];
    [_inToShopCar setImage:[UIImage imageNamed:@"cvs_btn_addshopcar"] forState:UIControlStateNormal];
    [_inToShopCar setImage:[UIImage imageNamed:@"cvs_btn_addshopcar_press"] forState:UIControlStateHighlighted];
    [_inToShopCar addTarget:self action:@selector(inToShopCarClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:_inToShopCar];
    [_inToShopCar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-10);
        make.bottom.mas_equalTo(HightScalar(-4));
        make.width.mas_equalTo(HightScalar(32));
        make.height.mas_equalTo(HightScalar(29));
    }];
}
- (void)retSubViewWithData:(NSDictionary*)dataDic {
    if ([dataDic count] != 0) {
        [self.imageView setImageWithURL:[NSURL URLWithString:[NSString appendImageUrlWithServerUrl:[NSString stringTransformObject:[dataDic objectForKey:@"THUMB"]]]]placeholderImage:[UIImage imageNamed:@"place_image"]];
        NSString *isSelf =[NSString stringTransformObject:[dataDic objectForKey:@"ISSELF"]];
        if ([isSelf isEqualToString:@"1"]) {
            NSString *emojiString = [NSString stringWithFormat:@"[zhiying] %@",[NSString stringISNull:[dataDic objectForKey:@"NAME"]]];
            UIFont *font = [UIFont systemFontOfSize:FontSize(14)];
            NSDictionary *attributes = @{NSFontAttributeName: font};
            NSAttributedString *attributedString = [NSAttributedString emotionAttributedStringFrom:emojiString attributes:attributes];
            
            self.titleLabel.attributedText =attributedString;
        }else{
            self.titleLabel.text =[NSString stringISNull:[dataDic objectForKey:@"NAME"]];
        }

        self.currentPriceLabel.text =[NSString stringWithFormat:@"￥%.2f", [[NSString stringTransformObject:[dataDic objectForKey:@"SELLPRICE"]] floatValue]];
        NSString *oldPriceStr =[NSString stringWithFormat:@"￥%.2f",[[NSString stringTransformObject:[dataDic objectForKey:@"MARKETPRICE"]] floatValue]];
        NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:oldPriceStr];
        [attrString addAttribute:NSStrikethroughStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlinePatternSolid | NSUnderlineStyleSingle] range:NSMakeRange(0, oldPriceStr.length)];
        self.oldPriceLabel.attributedText = attrString;
        
    }
}
-(void)inToShopCarClick:(UIButton *)sender
{
    
    UIButton *button = (UIButton *)sender;
    CGPoint buttonPoint = [[button superview] convertPoint:CGPointMake(button.frame.origin.x, button.frame.origin.y) toView:self];
    if (_delegate && [_delegate respondsToSelector:@selector(selectedItemAtIndexPath:buttonPoint:)]) {
        [_delegate selectedItemAtIndexPath:self.indexPath buttonPoint:buttonPoint];
    }
    
}

@end

//
//  Home_TableView_recommendStyle.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/2/23.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//
/***************************************
 ClassName： Home_Root_HeaderView
 Created_Date： 20160217
 Created_People： JSQ
 Function_description： 精品推荐标题样式
 ***************************************/
#import <UIKit/UIKit.h>

@interface Home_TableView_recommendStyle : UITableViewCell
- (void)updateViewWithData:(NSArray *)titleArr;
@end

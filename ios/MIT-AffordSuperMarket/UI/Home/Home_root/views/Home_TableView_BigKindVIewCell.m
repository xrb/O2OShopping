//
//  Home_TableView_BigKindVIewCell.m
//  DaShiHuiHome
//
//  Created by apple on 16/2/17.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "Home_TableView_BigKindVIewCell.h"
#import "Global.h"
#import "UIColor+Hex.h"
#import "XiaoQuAndStoreInfoModel.h"
#import "ManagerGlobeUntil.h"
#import "UIImage+ColorToImage.h"
#import "Home_TableView_BigKindCollectionCell.h"
//controller
#import "AppDelegate.h"
#define kviewHigth  HightScalar(136)
#define knoticeViewHight HightScalar(36) //通知视图高度
#define knoticeViewLeftSpace 11
#define kVSepLineTopSpace 6 //垂直分割线上边距高度
#define khsepLineHight 1 //水平分割线高度
#define kvSepLineWidth 1 //竖直分割线宽度

@interface Home_TableView_BigKindVIewCell()<UICollectionViewDelegate,UICollectionViewDataSource>
{
   CGFloat _collectionHight ;//collection高度
   CGFloat _collectionItemHight;//item高度
}
@property(nonatomic, strong)NSMutableArray *dataSource;
@property(nonatomic, strong)UILabel *remindLabel;
@property(nonatomic, strong)UIImageView *noticeImageView;
@property(nonatomic, strong)UIImageView *vSepLineImageView;//垂直分割线视图
@property(nonatomic, strong)UIImageView *hSepLineImageView;//水平分割线视图
@property(nonatomic, strong)UICollectionView *collectionView;
@end
@implementation Home_TableView_BigKindVIewCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self =[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _dataSource = [[NSMutableArray alloc]init];
        _collectionHight = 0;
        _collectionItemHight = 0;
        [self setUpSubview];
    }
    return self;
}
#pragma mark 刷新视图
- (void)updateViewWithData:(NSArray*)dataArr sectionTitle:(NSString*)sectionTitle {
    [self.dataSource removeAllObjects];
    [self.dataSource addObjectsFromArray:dataArr];
    if (dataArr.count > 4) {
        _collectionHight = 2*_collectionItemHight;
    } else if (dataArr.count <=4 && dataArr.count != 0) {
        _collectionHight = _collectionItemHight;
    }
    self.frame = CGRectMake(0, 0, VIEW_WIDTH, _collectionHight + knoticeViewHight + khsepLineHight);
    self.collectionView.frame = CGRectMake(CGRectGetMinX(self.collectionView.frame)
                                           , CGRectGetMinY(self.collectionView.frame)
                                           , CGRectGetWidth(self.collectionView.bounds)
                                           , _collectionHight);
    self.remindLabel.text = sectionTitle;
    [self.collectionView reloadData];
    
}

#pragma mark 动态计算视图高度
- (CGFloat)cellFactHight {
    return knoticeViewHight + khsepLineHight + _collectionHight;
}

#pragma mark -- UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.dataSource count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    Home_TableView_BigKindCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Home_TableView_BigKindCollectionCell" forIndexPath:indexPath];
    [cell updateViewWithData:[_dataSource objectAtIndex:indexPath.row]];
    return cell;
}
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
- (void)collectionView:(UICollectionView *)collectionView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *Cell = [self.collectionView cellForItemAtIndexPath:indexPath];
    Cell.alpha = 0.2;
    
}
- (void)collectionView:(UICollectionView *)collectionView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *Cell = [self.collectionView cellForItemAtIndexPath:indexPath];
    Cell.alpha = 1;
}
#pragma mark -- UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    switch (indexPath.row) {
        case 0:
        {
            [ManagerGlobeUntil sharedManager].bigClassTitle = @"生鲜蔬果";
            [ManagerGlobeUntil sharedManager].bigClassID = @"010000000";
            [ManagerGlobeUntil sharedManager].isHomeToStore =YES;
            [appDelegate selectedControllerAtTabBarIndex:1];
        }
            break;
        case 1:
        {
            [ManagerGlobeUntil sharedManager].bigClassTitle = @"生活百货";
            [ManagerGlobeUntil sharedManager].bigClassID = @"030000000";
            [ManagerGlobeUntil sharedManager].isHomeToStore =YES;
            [appDelegate selectedControllerAtTabBarIndex:1];
        }
            break;
        case 2:
        {
            [ManagerGlobeUntil sharedManager].bigClassTitle = @"粮油调料";
            [ManagerGlobeUntil sharedManager].bigClassID = @"040000000";
            [ManagerGlobeUntil sharedManager].isHomeToStore =YES;
            [appDelegate selectedControllerAtTabBarIndex:1];
            
        }
            break;
        case 3:
        {
            [ManagerGlobeUntil sharedManager].bigClassTitle = @"酒水饮料";
            [ManagerGlobeUntil sharedManager].bigClassID = @"020000000";
            [ManagerGlobeUntil sharedManager].isHomeToStore =YES;
            [appDelegate selectedControllerAtTabBarIndex:1];
        }
            break;
            //        case 5:
            //        {
            //            //[ManagerGlobeUntil sharedManager].bigClassTitle = @"营养早餐";
            //            [ManagerGlobeUntil sharedManager].bigClassID = @"050000000";
            //            Strort_Breakfas_Controller *Strore_detailVc=[[Strort_Breakfas_Controller alloc]init];
            //            [self.navigationController pushViewController:Strore_detailVc animated:YES];
            //        }
            //            break;
            
        default:
            break;
    }

}
#pragma mark -- 初始化视图
- (void)setUpSubview {
    [self.contentView addSubview:self.noticeImageView];
    [self.contentView addSubview:self.vSepLineImageView];
    [self.contentView addSubview:self.remindLabel];
    [self.contentView addSubview:self.hSepLineImageView];
    [self.contentView addSubview:self.collectionView];
}


- (UICollectionView*)collectionView {
    if (!_collectionView) {
        _collectionItemHight = kviewHigth - knoticeViewHight - khsepLineHight;
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.itemSize = CGSizeMake(VIEW_WIDTH/4  , _collectionItemHight );
        layout.minimumInteritemSpacing = 0;
        layout.minimumLineSpacing =0;
        layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
        UICollectionView *collectionview = [[UICollectionView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.hSepLineImageView.frame), VIEW_WIDTH, _collectionItemHight) collectionViewLayout:layout];
        collectionview.backgroundColor = [UIColor whiteColor];
        [collectionview registerClass:[Home_TableView_BigKindCollectionCell class] forCellWithReuseIdentifier:@"Home_TableView_BigKindCollectionCell"];
        [collectionview registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"Home_TableView_BigKindCollectionCell"];
        collectionview.delegate = self;
        collectionview.dataSource = self;
        collectionview.userInteractionEnabled = YES;
        _collectionView = collectionview;
    }
    return _collectionView;
}
//通知logo
-(UIImageView *)noticeImageView
{
    if (!_noticeImageView) {
        UIImage *image = [UIImage imageNamed:@"home_img_notice"];
        UIImageView *imageView =[[UIImageView alloc]initWithFrame:CGRectMake(knoticeViewLeftSpace, (knoticeViewHight - image.size.height)/2
                                                                             , image.size.width
                                                                             , image.size.height)];
        imageView.image = image;
        imageView.backgroundColor = [UIColor clearColor];
        _noticeImageView = imageView;
    }
    return _noticeImageView;
}
//垂直分割线
- (UIImageView*)vSepLineImageView {
    if (!_vSepLineImageView) {
        //分割线
        UIImageView *vSepLine = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.noticeImageView.frame) + knoticeViewLeftSpace
                                                                             , kVSepLineTopSpace
                                                                             , kvSepLineWidth
                                                                             , knoticeViewHight - 2*kVSepLineTopSpace)];
        vSepLine.image = [UIImage createImageWithColor:[UIColor colorWithHexString:@"#e9e9e9"] frame:CGRectMake(0, 0, 1, 1)];
        _vSepLineImageView = vSepLine;
    }
    return _vSepLineImageView;
}

//通知内容
- (UILabel*)remindLabel {
    if (!_remindLabel) {
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.vSepLineImageView.frame) + knoticeViewLeftSpace
                                                                  , CGRectGetMinY(self.noticeImageView.frame)
                                                                  , VIEW_WIDTH - CGRectGetMaxX(self.vSepLineImageView.frame) - 2*knoticeViewLeftSpace
                                                                  , CGRectGetHeight(self.noticeImageView.bounds))];
        label.backgroundColor = [UIColor clearColor];
        label.font = [UIFont systemFontOfSize:FontSize(14)];
        label.textColor = [UIColor colorWithHexString:@"#000000"];
        _remindLabel = label;
    }
    return _remindLabel;
}

//水平分割线
-(UIImageView *)hSepLineImageView
{
    if (!_hSepLineImageView) {
        UIImageView *imageView =[[UIImageView alloc]initWithFrame:CGRectMake(0,knoticeViewHight - khsepLineHight
                                                                             , VIEW_WIDTH, khsepLineHight)];
        imageView.image =[UIImage createImageWithColor:[UIColor colorWithHexString:@"#dedede"] frame:CGRectMake(0, 0, khsepLineHight, khsepLineHight)];
        _hSepLineImageView =imageView;
    }
    return _hSepLineImageView;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

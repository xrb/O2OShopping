//
//  Home_Root_Controller.h
//  DaShiHuiHome
//
//  Created by apple on 16/2/17.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： Home_Root_Controller
 Created_Date： 20160217
 Created_People： GT
 Function_description： 首页
 ***************************************/

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface Home_Root_Controller : BaseViewController
@property(nonatomic, strong)UICollectionView *collectionView;
@end

//
//  StoreGoodsDetailViewModel.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/4/13.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： StoreGoodsDetailViewModel
 Created_Date： 20160413
 Created_People： GT
 Function_description： 商品明细viewModel
 ***************************************/

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface StoreGoodsDetailViewModel : NSObject
@property(nonatomic)BOOL isCollectedGoods;//是否收藏过该商品
@property(nonatomic)BOOL isHadTextAndImageDescribe;//是否有图文详情
@property(nonatomic, strong)NSString *markCount;//关注数量
@property(nonatomic, strong)NSArray *goodsLoopImages;//商品轮播image
@property(nonatomic, strong)UIImage *goodsLogo;//商品图片

- (void)requestGoodsDetailDataWithGoodsId:(NSString*)goodsID isSelf:(NSString*)isSelf success:(void (^)(NSArray *responseObjects))success failure:(void(^)(NSString *failureMessage))failureMessage;
- (void)markGoodsWithGoodsId:(NSString*)goodsID isSelf:(NSString*)isSelf success:(void (^)(NSString *successMsg))success failure:(void(^)(NSString *failureMessage))failureMessage;
- (void)cancelMarkGoodsWithGoodsId:(NSString*)goodsID success:(void (^)(NSString *successMsg))success failure:(void(^)(NSString *failureMessage))failureMessage;
- (void)addGoodsToShopingCart;
@end

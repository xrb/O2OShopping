//
//  StroreRebateAndRecommendCell.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/4/15.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： StroreRebateAndRecommendCell
 Created_Date： 20160415
 Created_People： GT
 Function_description： 商品详情推荐或返现cell
 ***************************************/

#import <UIKit/UIKit.h>

@interface StroreRebateAndRecommendCell : UITableViewCell
-(void)updateViewWithData:(NSDictionary *)dataDic;
- (CGFloat)cellFactHight;
@end

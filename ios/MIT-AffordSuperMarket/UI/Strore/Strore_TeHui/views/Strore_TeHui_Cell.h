//
//  Strore_TeHui_Cell.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/7.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： Strore_TeHui_Cell
 Created_Date： 20151107
 Created_People： GT
 Function_description： 自定义特惠专区cell
 ***************************************/

#import <UIKit/UIKit.h>
@protocol Strore_TeHui_CellDelegate<NSObject>
//立即抢购
-(void)gotoBuyWithItemTag:(NSInteger)tag;
-(void)addShoppingCarWithIndex:(NSIndexPath *)indexPath;
@end
@interface Strore_TeHui_Cell : UITableViewCell
@property(nonatomic, unsafe_unretained)id<Strore_TeHui_CellDelegate>delegate;
@property(nonatomic, strong)NSIndexPath *indexPath;
@property(nonatomic, strong)UIButton *panicbuying;
//更新数据
-(void)updatawithData:(NSDictionary*)dataDic withType:(NSInteger)type;
@end

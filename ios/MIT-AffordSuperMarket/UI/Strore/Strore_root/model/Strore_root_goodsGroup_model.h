//
//  Strore_root_goodsGroup_model.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/1/28.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Strore_root_goodsGroup_model : NSObject

@property (assign, nonatomic) BOOL isChecked;
@property (strong, nonatomic) NSString *groupsName;
@property (strong, nonatomic) NSString *groupsCode;
@property (strong,nonatomic) NSString *groupsCount;
@end

//
//  Strore_CollectionViewCell.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/5.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： Strore_CollectionViewCell
 Created_Date： 20151105
 Created_People： GT
 Function_description： 便利店CollectionCell
 ***************************************/

#import <UIKit/UIKit.h>

@protocol Strore_CollectionViewCellDelegate <NSObject>

- (void)selectedItemAtIndexPath:(NSIndexPath *)indexPath buttonPoint:(CGPoint )buttonPonint;
@end
@interface Strore_CollectionViewCell : UICollectionViewCell
- (void)retSubViewWithData:(NSDictionary*)dataDic;
@property(nonatomic, strong)NSIndexPath *indexPath;
@property(assign,nonatomic)id<Strore_CollectionViewCellDelegate>delegate;

@end

//
//  Service_HomeMaking_Detail.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/1/5.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
ClassName： Service_listController
Created_Date： 20151103
Created_People： JSQ
Function_description：家政服务详情
***************************************/


#import "BaseViewController.h"
@interface Service_HomeMaking_Detail : BaseViewController

@property(nonatomic,strong)NSString *topTitle;

@end

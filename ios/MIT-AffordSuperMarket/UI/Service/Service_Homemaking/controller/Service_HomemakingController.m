//
//  Service_HomemakingController.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/1/5.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "Service_HomemakingController.h"
//view
#import "Service_HomemakingCell.h"
//controller
#import "Service_HomeMaking_Detail.h"
#import "HYBLoopScrollView.h"
#import "Global.h"
#import "UIColor+Hex.h"

@interface Service_HomemakingController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic, strong)HYBLoopScrollView *loopScrollView;
@property(nonatomic,strong)UITableView *tableView;
@property(nonatomic,strong)NSMutableArray *dataSource;
@end

@implementation Service_HomemakingController

- (void)viewDidLoad {
    [super viewDidLoad];
    _dataSource=[[NSMutableArray alloc]init];
    self.view.backgroundColor=[UIColor colorWithHexString:@"#efefef"];
    self.title=@"家政";
    [self setUpSubView];
    [self SetUpdataSource];
    [self addBackNavItem];
}
#pragma mark-- 设置数据源
-(void)SetUpdataSource
{
    for (NSInteger i=0;i<2; i++) {
        NSMutableDictionary *dic =[[NSMutableDictionary alloc]init];
        if (i==0) {
            [dic setValue:@"svhousekeeping_icon_dailycleaning" forKey:@"imageName"];
            [dic setValue:@"日常保洁" forKey:@"title"];
            [dic setValue:@"适用于家具、家电、台面、地面、墙面、等区域的日常表面清洁。" forKey:@"detail"];
        }else{
            [dic setValue:@"svhousekeeping_icon_deepcleaning" forKey:@"imageName"];
            [dic setValue:@"深度保洁" forKey:@"title"];
            [dic setValue:@"适用于长时间未打扫房屋，将积累已久的尘垢、污渍、油污全部清扫干净。" forKey:@"detail"];
        }
        [_dataSource addObject:dic];
    }
    
    [_tableView reloadData];
}
#pragma mark --UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    Service_HomeMaking_Detail *homeMaking_DetailVC=[[Service_HomeMaking_Detail alloc]init];
    homeMaking_DetailVC.topTitle=indexPath.row==0?@"日常保洁":@"深度保洁";
    [self.navigationController pushViewController:homeMaking_DetailVC animated:YES];
//    Evaluate_service_Controller *VC=[[Evaluate_service_Controller alloc]init];
//    [self.navigationController pushViewController:VC animated:YES];
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 90;
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view =[[UIView alloc]initWithFrame:CGRectMake(0, 0, VIEW_WIDTH, 40)];
    view.backgroundColor=[UIColor whiteColor];
    UILabel *lable =[[UILabel alloc]initWithFrame:CGRectMake(15, 10, VIEW_WIDTH, 20)];
    lable.text=@"保洁服务";
    lable.font=[UIFont systemFontOfSize:14];
    lable.textColor=[UIColor colorWithHexString:@"#555555"];
    
    [view addSubview:lable];
    return view;
}
#pragma mark --UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dicData =[_dataSource objectAtIndex:indexPath.row];
    NSString *ID = @"tuwenxiangqing";
     Service_HomemakingCell*cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[Service_HomemakingCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
       
    }
    
    [cell setSuberViewDataWithDictionary:dicData];
    
    return cell;

}
#pragma mark --初始化控件
-(void)setUpSubView
{
    UIView *hedaView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, VIEW_WIDTH, VIEW_HEIGHT == 480 ? 121 : 161)];
    
    CGRect loopScrollViewFrame = CGRectMake(0, 0, VIEW_WIDTH, VIEW_HEIGHT == 480 ? 120 : 160);
    _loopScrollView = [HYBLoopScrollView loopScrollViewWithFrame:loopScrollViewFrame
                                                       imageUrls:nil];
    
    _loopScrollView.delegate = (id<HYBLoopScrollViewDelegate>)self;
    //设置pagecontroll样式
    _loopScrollView.pageControl.currentPageIndicatorTintColor = [UIColor colorWithHexString:@"#c52720"];
    _loopScrollView.imageUrls = @[@"productDetail_Default_loopImage"];
    _loopScrollView.pageControl.pageIndicatorTintColor = [UIColor colorWithHexString:@"#dededd"];
    _loopScrollView.timeInterval = 5;//设置间隔时间
    _loopScrollView.placeholder = [UIImage imageNamed:@""];
    _loopScrollView.alignment = kPageControlAlignRight;//设置pagecontroll对齐方式
    [hedaView addSubview:_loopScrollView];
    UILabel *labe=[[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_loopScrollView.frame), VIEW_WIDTH, 1)];
    labe.backgroundColor=[UIColor colorWithHexString:@"#efefef"];
    [hedaView addSubview:labe];
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, VIEW_WIDTH, VIEW_HEIGHT-49-64) style:UITableViewStylePlain];
    
   
    _tableView.backgroundColor=[UIColor clearColor];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    _tableView.delegate=self;
    _tableView.dataSource=self;
    _tableView.tableHeaderView = hedaView;
    
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, VIEW_WIDTH,1)];
    _tableView.tableFooterView = view;
    [self.view addSubview:_tableView];

}
-(void)backBtnAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

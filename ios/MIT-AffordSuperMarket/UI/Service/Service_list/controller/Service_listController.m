//
//  Service_listController.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/12/16.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "Service_listController.h"
#import "Service_DetailController.h"
#import "Service_listCell.h"
#import "Global.h"
#import "NSString+Conversion.h"
#import "ManagerGlobeUntil.h"
#import "ManagerHttpBase.h"
#import "AppDelegate.h"
#import "UIColor+Hex.h"
#import "RDVTabBarController.h"
#import "MTA.h"
#import "EmptyPageView.h"
@interface Service_listController ()<UITableViewDataSource,UITableViewDelegate,EmptyPageViewDelegate>
@property (nonatomic,strong)UITableView *tableView;
@property (nonatomic,strong)NSMutableArray *dataSource;
@property (nonatomic,strong)EmptyPageView *emptyPageView;

//当前页
@property (nonatomic)NSInteger currentPage;
@end
@implementation Service_listController
@synthesize currentPage = _currentPage;
- (void)viewDidLoad {
    [super viewDidLoad];
    _dataSource =[[NSMutableArray alloc]init];
    self.view.backgroundColor=[UIColor colorWithHexString:@"#efefef"];
    self.currentPage = 1;
    self.title=self.titles;
    [self addBackNavItem];
    [self setSuberView];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.tabBarController setTabBarHidden:YES animated:NO];
    //判断网络获取数据
    if ([ManagerGlobeUntil sharedManager].isNetworkReachability) {
        [self setDataSourceWithrequestPage:self.currentPage requestCount:kRequestLimit];
    }
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.tabBarController setTabBarHidden:NO animated:YES];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [MTA trackPageViewBegin:@"服务列表"];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [MTA trackPageViewEnd:@"服务列表"];
}
#pragma mark--获取数据
-(void)setDataSourceWithrequestPage:(NSInteger)requestPage requestCount:(NSInteger)requestCount
{
    //    SIGNATURE			设备识别码
    //    ID			店铺ID
    [[ManagerGlobeUntil sharedManager]showHUDWithMsg:@"加载中..." inView:self.view];
    ManagerHttpBase *manager=[ManagerHttpBase sharedManager];
    XiaoQuAndStoreInfoModel *mode = [[ManagerGlobeUntil sharedManager] getxiaoQuAndStoreInfo];
    NSMutableDictionary *parameter = [[NSMutableDictionary alloc]init];
    [parameter setObject:[NSString stringISNull:mode.storeID] forKey:@"STOREID"];
    [parameter setObject:self.code forKey:@"CATEGORYCODE"];
    [parameter setObject:[NSNumber numberWithInteger:requestPage] forKey:@"PAGENUM"];
    [parameter setObject:[NSNumber numberWithInteger:requestCount] forKey:@"PAGESIZE"];
    __weak typeof(self)weakSelf = self;
    [manager parameters:parameter customPOST:@"service/shops" success:^(AFHTTPRequestOperation *operation, id responseObject) {
         [[ManagerGlobeUntil sharedManager] hideHUDFromeView:weakSelf.view];
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dataDic = (NSDictionary*)responseObject;
            NSString *state = [NSString stringTransformObject:[dataDic objectForKey:@"STATE"]];//状态码
            
            if ([state isEqualToString:@"0"]) {//请求成功
                [_dataSource removeAllObjects];
                [_dataSource addObjectsFromArray:[[dataDic objectForKey:@"OBJECT"] objectForKey:@"list"]];
                [_tableView reloadData];
                if (_dataSource.count ==0) {
                    _tableView.hidden =YES;
                    _emptyPageView.hidden =NO;
                }else
                {
                    _tableView.hidden =NO;
                    _emptyPageView.hidden =YES;
                    
                }
            } else {
                [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:@"暂无数据" inView:weakSelf.view];
            }

        }
        
    } failure:^(bool isFailure) {
         [[ManagerGlobeUntil sharedManager] hideHUDFromeView:weakSelf.view];
        [_tableView reloadData];
    }];
    
    

}
#pragma mark--UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    static NSString *cellIdentifier = @"fuwucell";
    //2.没有则创建cell
    Service_listCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    
    if(!cell) {
        cell=[[Service_listCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        
        cell.accessoryType = UITableViewCellAccessoryNone;
        
        cell.backgroundColor = [UIColor whiteColor];
    }
    NSDictionary *dataDic = [self.dataSource objectAtIndex:indexPath.row];
    [cell setSuberViewDataWithDictionary:dataDic];
    //4.返回cell
    return cell;
}
#pragma mark--UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return HightScalar(135);
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 1;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
     [_tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary *dicData=[_dataSource objectAtIndex:indexPath.row];
    Service_DetailController *detailVc=[[Service_DetailController alloc]init];
    detailVc.shopID=[NSString stringTransformObject:[dicData objectForKey:@"ID"]];
    detailVc.shopName=[NSString stringTransformObject:[dicData objectForKey:@"NAME"]];
    detailVc.shopImage=[NSString stringTransformObject:[dicData objectForKey:@"THUMB"]];
    [self.navigationController pushViewController:detailVc animated:YES];
    
}
-(void)backBtnAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)buttonClick
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
#pragma mark--初始化控件
-(void)setSuberView
{
    _tableView =[[UITableView alloc]initWithFrame:CGRectMake(0, 0, VIEW_WIDTH, VIEW_HEIGHT-64) style:UITableViewStylePlain];
    _tableView.delegate=self;
    _tableView.dataSource=self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    _tableView.backgroundColor=[UIColor  clearColor];
    [self.view addSubview:_tableView];
    
    _emptyPageView =[[EmptyPageView alloc]initWithFrame:CGRectMake(0, 0, VIEW_WIDTH, VIEW_HEIGHT-64) EmptyPageViewType:kServiecListView];
    _emptyPageView.hidden =YES;
    _emptyPageView.delegate =self;
    [self.view addSubview:_emptyPageView];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end

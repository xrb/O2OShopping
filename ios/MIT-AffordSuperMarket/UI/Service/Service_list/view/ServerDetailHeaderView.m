//
//  ServerDetailHeaderView.m
//  ServerDetailDemo
//
//  Created by apple on 15/12/16.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "ServerDetailHeaderView.h"
#define VIEW_WIDTH [UIScreen mainScreen].bounds.size.width//屏幕宽度
#define VIEW_HEIGHT [UIScreen mainScreen].bounds.size.height//屏幕高度
#define kleftSpace 15
#define labelHeight 30
#define khSpace 8
//vendor
#import "HYBLoopScrollView.h"
#import "Global.h"
//untils
#import "UIColor+Hex.h"
#import "NSString+Conversion.h"
#import "NSString+TextSize.h"
@interface ServerDetailHeaderView ()
@property(nonatomic, strong)HYBLoopScrollView *loopScrollView;
@property(nonatomic, strong)UILabel *titleLabel;
@property(nonatomic, strong)UILabel *countLabel;
//@property(nonatomic, strong)UILabel *markCountLabel;
@property(nonatomic, strong)UILabel *currentPriceLabel;
@property(nonatomic, strong)UILabel *oldPriceLabel;
@end
@implementation ServerDetailHeaderView
- (instancetype)initWithFrame:(CGRect)frame placeImageName:(NSString*)placeImageName {
    self = [super initWithFrame:frame];
    if (self) {
        [self setupSubViewWithPlaceImageName:placeImageName];
    }
    return self;
}
#pragma mark -- 初始化视图
- (void)setupSubViewWithPlaceImageName:(NSString*)placeImageName {
    //设置轮播图
    CGRect loopScrollViewFrame = CGRectMake(0, 0, VIEW_WIDTH, HightScalar(210));
    _loopScrollView = [HYBLoopScrollView loopScrollViewWithFrame:loopScrollViewFrame
                                                       imageUrls:nil];
    //设置pagecontroll样式
    _loopScrollView.pageControl.currentPageIndicatorTintColor = [UIColor colorWithHexString:@"#c52720"];
    _loopScrollView.pageControl.pageIndicatorTintColor = [UIColor colorWithHexString:@"#dededd"];
    
    _loopScrollView.timeInterval = 3;//设置间隔时间
    _loopScrollView.alignment = kPageControlAlignRight;//设置pagecontroll对齐方式
    _loopScrollView.backgroundColor = [UIColor clearColor];
    [self addSubview:_loopScrollView];
    _loopScrollView.imageUrls = @[placeImageName,placeImageName,placeImageName];
    //标题
    UILabel *labe=[[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_loopScrollView.frame), VIEW_WIDTH, 1)];
    labe.backgroundColor=[UIColor colorWithHexString:@"#efefef"];
    [self addSubview:labe];
    _titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(kleftSpace
                                                          , CGRectGetMaxY(_loopScrollView.frame)+6
                                                          , VIEW_WIDTH - 2*kleftSpace
                                                           , labelHeight)];
    _titleLabel.font = [UIFont systemFontOfSize:FontSize(20)];
    _titleLabel.textColor = [UIColor colorWithHexString:@"#555555"];
    _titleLabel.textAlignment = NSTextAlignmentLeft;
    _titleLabel.text  = @"";
    [self addSubview:_titleLabel];
    //服务次数
    _countLabel = [[UILabel alloc]initWithFrame:CGRectZero];
    _countLabel.font = [UIFont systemFontOfSize:FontSize(16)];
    _countLabel.textColor = [UIColor colorWithHexString:@"#999999"];
    _countLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:_countLabel];
    //优惠后价格
    _currentPriceLabel = [[UILabel alloc]initWithFrame:CGRectZero];
    _currentPriceLabel.font = [UIFont systemFontOfSize:FontSize(18)];
    _currentPriceLabel.textColor = [UIColor colorWithHexString:@"#fc605e"];
    _currentPriceLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:_currentPriceLabel];
    //优惠前价格
    _oldPriceLabel = [[UILabel alloc]initWithFrame:CGRectZero];
    _oldPriceLabel.font = [UIFont systemFontOfSize:FontSize(13)];
    _oldPriceLabel.textColor = [UIColor colorWithHexString:@"#999999"];
    _oldPriceLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:_oldPriceLabel];
    
}

//更新总数据
- (void)updateWithDataDic:(NSDictionary*)dataDic {
    NSArray *imageUrls =[dataDic objectForKey:@"IMAGES"];
    if (imageUrls.count!=0) {
        NSMutableArray *loopImageUrls = [[NSMutableArray alloc]init];
        for (NSString *imageUrl in imageUrls) {
            [loopImageUrls addObject:[NSString appendImageUrlWithServerUrl:imageUrl]];
        }
        
        _loopScrollView.imageUrls = [loopImageUrls copy];
    }else{
        _loopScrollView.imageUrls = @[@"productDetail_Default_loopImage"];
    }
    self.titleLabel.text = [NSString stringTransformObject:[dataDic objectForKey:@"NAME"]];
    NSDictionary *dic = [[dataDic objectForKey:@"ITEMS"] firstObject];
    NSString *serviceCountStr = [NSString stringTransformObject:[dataDic objectForKey:@"TOTAL"]];
    NSString *markCountStr = @"0";
    NSString *currentPriceStr = [NSString stringTransformObject:[dic objectForKey:@"SELLPRICE"]];
    NSString *oldPriceStr = [NSString stringTransformObject:[dic objectForKey:@"MARKETPRICE"]];
    serviceCountStr = [NSString stringWithFormat:@"月服务次数%@",serviceCountStr];
    markCountStr = [NSString stringWithFormat:@"关注%@",markCountStr];
    currentPriceStr = [NSString stringWithFormat:@"￥%@",currentPriceStr];
    oldPriceStr = [NSString stringWithFormat:@"￥%@",oldPriceStr];
    CGSize serviceCountSize = [serviceCountStr sizeWithFont:[UIFont systemFontOfSize:FontSize(16)] maxSize:CGSizeMake(160, labelHeight)];
//    CGSize markCountSize = [markCountStr sizeWithFont:[UIFont systemFontOfSize:14] maxSize:CGSizeMake(160, labelHeight)];
    CGSize currentPriceSize = [currentPriceStr sizeWithFont:[UIFont systemFontOfSize:FontSize(18)] maxSize:CGSizeMake(160, labelHeight)];
    CGSize oldPriceSize = [oldPriceStr sizeWithFont:[UIFont systemFontOfSize:FontSize(13)] maxSize:CGSizeMake(160, labelHeight)];
    
    CGRect serviceCountFrame = self.countLabel.frame;
    serviceCountFrame.origin.x = CGRectGetMinX(_titleLabel.frame);
    serviceCountFrame.origin.y = CGRectGetMaxY(_titleLabel.frame);
    serviceCountFrame.size = CGSizeMake(serviceCountSize.width, labelHeight);
    self.countLabel.frame = serviceCountFrame;
    
    CGRect currentPriceFrame = self.currentPriceLabel.frame;
    currentPriceFrame.origin.x = CGRectGetMinX(_countLabel.frame);
    currentPriceFrame.origin.y = CGRectGetMaxY(_countLabel.frame);
    currentPriceFrame.size = CGSizeMake(currentPriceSize.width, labelHeight);
    self.currentPriceLabel.frame = currentPriceFrame;
    
    CGRect oldPriceFrame = self.oldPriceLabel.frame;
    oldPriceFrame.origin.x = CGRectGetMaxX(_currentPriceLabel.frame) + khSpace;
    oldPriceFrame.origin.y = CGRectGetMinY(_currentPriceLabel.frame);
    oldPriceFrame.size = CGSizeMake(oldPriceSize.width, labelHeight);
    self.oldPriceLabel.frame = oldPriceFrame;
    
    self.countLabel.text = serviceCountStr;
    self.currentPriceLabel.text = currentPriceStr;
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:oldPriceStr];
    [attrString addAttribute:NSStrikethroughStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlinePatternSolid | NSUnderlineStyleSingle] range:NSMakeRange(0, oldPriceStr.length)];
    self.oldPriceLabel.attributedText = attrString;

    
}
//更新价格信息
- (void)updatePriceWithDataDic:(NSDictionary*)dataDic {
    NSString *currentPriceStr = [NSString stringTransformObject:[dataDic objectForKey:@"SELLPRICE"]];
    NSString *oldPriceStr = [NSString stringTransformObject:[dataDic objectForKey:@"MARKETPRICE"]];
    currentPriceStr = [NSString stringWithFormat:@"￥%@",currentPriceStr];
    oldPriceStr = [NSString stringWithFormat:@"￥%@",oldPriceStr];
    CGSize currentPriceSize = [currentPriceStr sizeWithFont:[UIFont systemFontOfSize:18] maxSize:CGSizeMake(160, labelHeight)];
    CGSize oldPriceSize = [oldPriceStr sizeWithFont:[UIFont systemFontOfSize:14] maxSize:CGSizeMake(160, labelHeight)];
    
    CGRect currentPriceFrame = self.currentPriceLabel.frame;
    currentPriceFrame.size = CGSizeMake(currentPriceSize.width, labelHeight);
    self.currentPriceLabel.frame = currentPriceFrame;
    
    CGRect oldPriceFrame = self.oldPriceLabel.frame;
    oldPriceFrame.origin.x = CGRectGetMaxX(_currentPriceLabel.frame) + khSpace;
    oldPriceFrame.size = CGSizeMake(oldPriceSize.width, labelHeight);
    self.oldPriceLabel.frame = oldPriceFrame;
    
    self.currentPriceLabel.text = currentPriceStr;
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:oldPriceStr];
    [attrString addAttribute:NSStrikethroughStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlinePatternSolid | NSUnderlineStyleSingle] range:NSMakeRange(0, oldPriceStr.length)];
    self.oldPriceLabel.attributedText = attrString;
}

@end

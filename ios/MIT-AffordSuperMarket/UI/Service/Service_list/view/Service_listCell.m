//
//  Service_listCell.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/12/16.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#define KLeftSpace 15
#define KTopSpace 15
#define KSpace 10
#import "Service_listCell.h"
#import "Global.h"
#import "UIColor+Hex.h"
#import "UIImage+ColorToImage.h"
#import "NSString+Conversion.h"
#import "UIImageView+AFNetworking.h"
@interface Service_listCell()
//服务公司logo
@property (nonatomic,strong)UIImageView *companyLogo;
//公司名称
@property (nonatomic,strong)UILabel *companyName;
//公司介绍
@property (nonatomic,strong)UILabel *companydetail;
//公司现价
@property (nonatomic,strong)UILabel *price;
//公司原价
@property (nonatomic,strong)UILabel *oldprice;
//公司服务次数
@property (nonatomic,strong)UILabel *serviceNum;
@end
@implementation Service_listCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setSuberView];
    }
    return self;
}

#pragma mark--初始化控件
-(void)setSuberView
{
    //logo
    _companyLogo =[[UIImageView alloc]initWithFrame:CGRectMake(KLeftSpace, KTopSpace, HightScalar(104), HightScalar(104))];
    [_companyLogo.layer setBorderColor:[UIColor colorWithHexString:@"#dddddd"].CGColor];
    [_companyLogo.layer setBorderWidth:0.7];
    [self.contentView addSubview:_companyLogo];
    
    //名字
    _companyName=[[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.companyLogo.frame)+KSpace, CGRectGetMinY(self.companyLogo.frame), VIEW_WIDTH-CGRectGetWidth(self.companyLogo.bounds)-KSpace-KLeftSpace-KLeftSpace, HightScalar(30))];
    _companyName.font=[UIFont systemFontOfSize:FontSize(16)];
    _companyName.textColor=[UIColor colorWithHexString:@"#555555"];
    _companyName.text=@"乐邦家政";
    [self.contentView addSubview:_companyName];
    //服务次数
    _serviceNum=[[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMinX(self.companyName.frame), CGRectGetMaxY(self.companyName.frame),CGRectGetWidth(self.companyName.bounds), CGRectGetHeight(self.companyName.bounds))];
    _serviceNum.font=[UIFont systemFontOfSize:FontSize(14)];
    _serviceNum.textColor=[UIColor colorWithHexString:@"#999999"];
    _serviceNum.text=@"累计服务:80次";
    [self.contentView addSubview:_serviceNum];
    //商家描述
    _companydetail=[[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMinX(self.serviceNum.frame), CGRectGetMaxY(self.serviceNum.frame), CGRectGetWidth(self.serviceNum.bounds), CGRectGetHeight(self.companyName.bounds)+KSpace)];
    
    _companydetail.font=[UIFont systemFontOfSize:FontSize(14)];
    _companydetail.textColor =[UIColor colorWithHexString:@"#999999"];
    _companydetail.text=@"新业家政";
    _companydetail.numberOfLines = 0;
    [self.contentView addSubview:_companydetail];
}

-(void)setSuberViewDataWithDictionary:(NSDictionary *)dictionary
{
    if (dictionary.count!=0)
    {
        [self.companyLogo setImageWithURL:[NSURL URLWithString:[NSString appendImageUrlWithServerUrl:[NSString stringTransformObject:[dictionary objectForKey:@"THUMB"]]]]placeholderImage:[UIImage imageNamed:@"place_image"]];
    _companyName.text=[NSString stringTransformObject:[dictionary objectForKey:@"NAME"]];
    _companydetail.text=[NSString stringTransformObject:[dictionary objectForKey:@"COMMENT"]];
    NSMutableAttributedString *str=[[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"累计服务:%@次",[NSString stringTransformObject:[dictionary objectForKey:@"TOTAL"]]]];
        
    [str addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"#ff0000"] range:NSMakeRange(5, str.length-6)];
        
    self.serviceNum.attributedText=str;
    }
}
@end

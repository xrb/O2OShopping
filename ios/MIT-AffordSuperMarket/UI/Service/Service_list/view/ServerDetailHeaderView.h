//
//  ServerDetailHeaderView.h
//  ServerDetailDemo
//
//  Created by apple on 15/12/16.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： ServerDetailHeaderView
 Created_Date： 20151221
 Created_People： GT
 Function_description： 服务商家详列表头部视图
 ***************************************/

#import <UIKit/UIKit.h>

@interface ServerDetailHeaderView : UIView
@property(nonatomic, strong)NSArray *imageUrls;
- (instancetype)initWithFrame:(CGRect)frame placeImageName:(NSString*)placeImageName;
//更新总数据
- (void)updateWithDataDic:(NSDictionary*)dataDic;
//更新价格信息
- (void)updatePriceWithDataDic:(NSDictionary*)dataDic;
@end

//
//  ServerDetailSelectServerTypeCell.h
//  ServerDetailDemo
//
//  Created by apple on 15/12/16.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： ServerDetailHeaderView
 Created_Date： 20151221
 Created_People： GT
 Function_description： 服务商家详列表服务类型视图
 ***************************************/

#import <UIKit/UIKit.h>
@protocol ServerDetailSelectServerTypeCellDelegate<NSObject>
-(void)selectedItemWithTag:(NSInteger)tag itemData:(NSDictionary*)dataDic;
@end
@interface ServerDetailSelectServerTypeCell : UITableViewCell
@property(nonatomic, unsafe_unretained)id<ServerDetailSelectServerTypeCellDelegate>delegate;

- (void)updateDataWithDataArr:(NSArray*)dataArr;
- (CGFloat)cellFactHight;
@end

//
//  BaseViewController.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/10/30.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController
@property(nonatomic, strong)NSString *titleItemTitle;//针对便利店navigatioItemButton标题
- (void)addLeftNavItemWith:(UIImage*)img title:(NSString*)title;
- (void)addRightNavItemWith:(UIImage*)img;
- (void)addRightNavItemWithTitle:(NSString*)title;
- (void)rigthBtnAction:(id)sender;
- (void)leftBtnAction:(id)sender;
- (void)setNavgationTitleViewWithTitlt:(NSString*)title;
- (void)setNavgationTitleViewWithImageName:(NSString*)imageName;
- (void)setNavgationTitleViewWithButtonTitle:(NSString*)title buttonImageName:(NSString*)imageName;
- (void)addBackNavItem;
- (void)backBtnAction:(id)sender;
- (void)titleItemButtonAction:(id)sender;//针对便利店
//首页
- (void)setHomeNavgationTitleViewWithButtonTitle:(NSString*)title;
- (void)beatSearchEvent:(id)sender;
//网络状态
-(void)applicationDidBecomeActive;

@end

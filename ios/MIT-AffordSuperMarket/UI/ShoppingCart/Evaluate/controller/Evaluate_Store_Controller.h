//
//  Evaluate_Store_Controller.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/3/18.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： Evaluate_Store_Controller
 Created_Date： 20160318
 Created_People： GT
 Function_description： 对商店进行评价
 ***************************************/

#import "BaseViewController.h"
typedef NS_ENUM(NSInteger, EvaluateType) {
    kgoodsRating = 100,//商品评价
    kpeiSongRating = 101,//配送评价
    kserviceRating //服务评价
};
@interface Evaluate_Store_Controller : BaseViewController
@property(nonatomic, strong)NSString *orderNum;
@end

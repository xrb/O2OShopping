//
//  Evaluate_Store_Controller.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/3/18.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "Evaluate_Store_Controller.h"
//untis
#import "UIColor+Hex.h"
#import "Global.h"
#import "AppDelegate.h"
#import "NSString+TextSize.h"
#import "UIImage+ColorToImage.h"
#import "ManagerGlobeUntil.h"
#import "ManagerHttpBase.h"
#import "NSString+Conversion.h"
//vendor
#import "LHRatingView.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "RDVTabBarController.h"
#import "UIPlaceHolderTextView.h"
#import "MTA.h"
//controller
#import "MyGoodsOrderController.h"

#define kleftSpace 15
#define ktopSpace 15
#define khSpace 15
#define kvSpace 6
#define ksepLineHight 0.7
#define kbottomViewHight 56
@interface Evaluate_Store_Controller ()<ratingViewDelegate>
{
    NSInteger _goodsRateValue;
    NSInteger _peiSongRateValue;
    NSInteger _serviceRateValue;
}
@property(nonatomic, strong)TPKeyboardAvoidingScrollView *rootScrollView;
@property (nonatomic,strong)UIPlaceHolderTextView *ecaluateTextView;//评论区域
@property (nonatomic,strong)LHRatingView * goodsRatingView;//商品满意度
@property (nonatomic,strong)LHRatingView * peiSongRatingView;//配送速度满意度
@property (nonatomic,strong)LHRatingView * serviceRatingView;//服务质量满意度
@end

@implementation Evaluate_Store_Controller

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithHexString:@"#efefef"];
    self.title = @"评价";
    [self addBackNavItem];
    _goodsRateValue = 5;
    _peiSongRateValue = 5;
    _serviceRateValue = 5;
    [self setupSubView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.tabBarController setTabBarHidden:YES animated:NO];
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [MTA trackPageViewBegin:@"评价页面"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [appDelegate.tabBarController setTabBarHidden:NO animated:YES];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [MTA trackPageViewEnd:@"评价页面"];
}

#pragma mark -- button Action
- (void)submitRatingEvent {
    if (self.ecaluateTextView.text.length == 0) {
        NSString *errorMsg = @"您还未对本次购物进行评价！";
        [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:errorMsg inView:self.view];
    } else {
        [self loadRatingRequest];
    }
}

- (void)backBtnAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark -- 网络请求

- (void)loadRatingRequest {
    /*
     SIGNATURE			设备识别码
     TOKEN			    用户签名
     ORDERNUM			订单号
     EVAL1              商品满意度
     EVAL2              配送速度满意度
     EVAL3              服务质量满意度
     CONTENT            评价内容
     **/
    UserInfoModel *mode = [[ManagerGlobeUntil sharedManager] getUserInfo];
    ManagerHttpBase *manager=[ManagerHttpBase sharedManager];
    NSMutableDictionary *paramerters=[[NSMutableDictionary alloc]init];
    [paramerters setObject:[NSString stringTransformObject:mode.token] forKey:@"TOKEN"];
    [paramerters setObject:[NSString stringTransformObject:self.orderNum] forKey:@"ORDERNUM"];
    [paramerters setObject:[NSNumber numberWithInteger:_goodsRateValue] forKey:@"EVAL1"];
    [paramerters setObject:[NSNumber numberWithInteger:_peiSongRateValue] forKey:@"EVAL2"];
    [paramerters setObject:[NSNumber numberWithInteger:_serviceRateValue] forKey:@"EVAL3"];
    [paramerters setObject:self.ecaluateTextView.text forKey:@"CONTENT"];
    __weak typeof(self) weakSelf = self;
    [manager parameters:paramerters customPOST:@"order/eval" success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *dataDic = (NSDictionary*)responseObject;
        NSString *state = [NSString stringTransformObject:[dataDic objectForKey:@"STATE"]];//状态码
        NSString *errorMsg = [NSString stringTransformObject:[dataDic objectForKey:@"MSG"]];
        if ([state isEqualToString:@"0"]) {//请求成功
            [self rateSuccess];
        } else {
           [[ManagerGlobeUntil sharedManager]showTextHUDWithMsg:errorMsg inView:weakSelf.view];
        }
        
    } failure:^(bool isFailure) {
        
    }];

}

- (void)rateSuccess {
    
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"" message:@"评价成功，感谢您的评价，我们会继续加油~"
                                                      delegate:(id<UIAlertViewDelegate>)self
                                             cancelButtonTitle:@"确认" otherButtonTitles: nil];
    [alertView show];
    
}
#pragma mark - ratingViewDelegate 星星视图
- (void)ratingView:(LHRatingView *)view score:(CGFloat)score {
    switch (view.tag) {
        case kgoodsRating:
            _goodsRateValue = score;
            break;
        case kpeiSongRating:
            _peiSongRateValue = score;
            break;
        case kserviceRating:
            _serviceRateValue = score;
            break;
            
        default:
            break;
    }
}
#pragma mark -- UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    for (UIViewController*VC in self.navigationController.childViewControllers) {
        if ([VC isKindOfClass:[MyGoodsOrderController class]]) {
            ((MyGoodsOrderController *)VC).myGoodOrderType = kGoodWholeType;
            ((MyGoodsOrderController *)VC).selectedIndex = 0;
            ((MyGoodsOrderController *)VC).PopType =@"evaluateType";
            [self.navigationController popToViewController:VC animated:YES];
        }
    }
}
#pragma mark -- 初始化视图
- (void)setupSubView {
    //根视图容器
    _rootScrollView = [[TPKeyboardAvoidingScrollView alloc]initWithFrame:CGRectMake(0
                                                                                    , 0
                                                                                    , CGRectGetWidth(self.view.bounds)
                                                                                    , CGRectGetHeight(self.view.bounds))];
    _rootScrollView.scrollViewDelegate = (id<TPKeyboardAvoidingScrollViewDelegate>)self;
    _rootScrollView.showsHorizontalScrollIndicator = NO;
    _rootScrollView.showsVerticalScrollIndicator = NO;
    _rootScrollView.backgroundColor = [UIColor clearColor];
    _rootScrollView.contentSize = CGSizeMake(CGRectGetWidth(self.view.bounds)
                                             , CGRectGetHeight(self.view.bounds) - 64);
    [self.view addSubview:_rootScrollView];
    
    NSString *goodsRatingTitle = @"商品满意度";
    NSString *peiSongRatingTitle = @"配送速度满意度";
    NSString *serviceRatingTitle = @"服务质量满意度";
    CGSize titleSize = [serviceRatingTitle sizeWithFont:[UIFont systemFontOfSize:FontSize(16)] maxSize:CGSizeMake(160, 30)];
    //商品满意度
    UILabel *goodsRatingTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(kleftSpace
                                                                              , ktopSpace
                                                                              , titleSize.width
                                                                              , HightScalar(30))];
    goodsRatingTitleLabel.textColor = [UIColor colorWithHexString:@"#555555"];
    goodsRatingTitleLabel.font = [UIFont systemFontOfSize:FontSize(16)];
    goodsRatingTitleLabel.text = goodsRatingTitle;
    [self.rootScrollView addSubview:goodsRatingTitleLabel];
    
    _goodsRatingView = [[LHRatingView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(goodsRatingTitleLabel.frame) + khSpace
                                                                     , CGRectGetMinY(goodsRatingTitleLabel.frame)
                                                                     , VIEW_WIDTH - CGRectGetMaxX(goodsRatingTitleLabel.frame) - khSpace - kleftSpace
                                                                     , CGRectGetHeight(goodsRatingTitleLabel.bounds))
                                                 starType:@"1"];
    _goodsRatingView.delegate = self;
    _goodsRatingView.tag= kgoodsRating;
     _goodsRatingView.score = _goodsRateValue;
    _goodsRatingView.ratingType = INTEGER_TYPE;
    [self.rootScrollView addSubview:_goodsRatingView];
    
    //配送速度满意度
    UILabel *peiSongRatingTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMinX(goodsRatingTitleLabel.frame)
                                                                              , CGRectGetMaxY(goodsRatingTitleLabel.frame) + khSpace
                                                                              , CGRectGetWidth(goodsRatingTitleLabel.bounds)
                                                                              , CGRectGetHeight(goodsRatingTitleLabel.bounds))];
    peiSongRatingTitleLabel.textColor = [UIColor colorWithHexString:@"#555555"];
    peiSongRatingTitleLabel.font = [UIFont systemFontOfSize:FontSize(16)];
    peiSongRatingTitleLabel.text = peiSongRatingTitle;
    [self.rootScrollView addSubview:peiSongRatingTitleLabel];
    
    _peiSongRatingView = [[LHRatingView alloc]initWithFrame:CGRectMake(CGRectGetMinX(_goodsRatingView.frame)
                                                                     , CGRectGetMinY(peiSongRatingTitleLabel.frame)
                                                                     , CGRectGetWidth(_goodsRatingView.bounds)
                                                                     , CGRectGetHeight(_goodsRatingView.bounds))
                                                 starType:@"1"];
    _peiSongRatingView.delegate = self;
    _peiSongRatingView.tag= kpeiSongRating;
    _peiSongRatingView.score = _peiSongRateValue;
    _peiSongRatingView.ratingType = INTEGER_TYPE;
    [self.rootScrollView addSubview:_peiSongRatingView];
    
    //服务质量满意度
    UILabel *serviceRatingTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMinX(peiSongRatingTitleLabel.frame)
                                                                                , CGRectGetMaxY(peiSongRatingTitleLabel.frame) + khSpace
                                                                                , CGRectGetWidth(peiSongRatingTitleLabel.bounds)
                                                                                , CGRectGetHeight(peiSongRatingTitleLabel.bounds))];
    serviceRatingTitleLabel.textColor = [UIColor colorWithHexString:@"#555555"];
    serviceRatingTitleLabel.font = [UIFont systemFontOfSize:FontSize(16)];
    serviceRatingTitleLabel.text = serviceRatingTitle;
    [self.rootScrollView addSubview:serviceRatingTitleLabel];
    
    _serviceRatingView = [[LHRatingView alloc]initWithFrame:CGRectMake(CGRectGetMinX(_peiSongRatingView.frame)
                                                                       , CGRectGetMinY(serviceRatingTitleLabel.frame)
                                                                       , CGRectGetWidth(_peiSongRatingView.bounds)
                                                                       , CGRectGetHeight(_peiSongRatingView.bounds))
                                                   starType:@"1"];
    _serviceRatingView.delegate = self;
    _serviceRatingView.tag= kserviceRating;
    _serviceRatingView.score = _serviceRateValue;

    _serviceRatingView.ratingType = INTEGER_TYPE;
    [self.rootScrollView addSubview:_serviceRatingView];
    
    //分割线
    UIImageView *sepLineimageView = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMinX(serviceRatingTitleLabel.frame)
                                                                                , CGRectGetMaxY(serviceRatingTitleLabel.frame) + khSpace
                                                                                , VIEW_WIDTH - kleftSpace
                                                                                 , ksepLineHight)];
    sepLineimageView.image = [UIImage createImageWithColor:[UIColor colorWithHexString:@"#dddddd"] frame:CGRectMake(0, 0, 1, 1)];
    [self.rootScrollView addSubview:sepLineimageView];
    
    //评论区域
    _ecaluateTextView =[[UIPlaceHolderTextView alloc]initWithFrame:CGRectMake(CGRectGetMinX(sepLineimageView.frame)
                                                                              ,CGRectGetMaxY(sepLineimageView.frame) + khSpace
                                                                              , VIEW_WIDTH-2*kleftSpace
                                                                              , 160)];
    
    _ecaluateTextView.textColor=[UIColor colorWithHexString:@"#555555"];
    self.ecaluateTextView.delegate = (id<UITextViewDelegate>)self;
    self.ecaluateTextView.backgroundColor=[UIColor whiteColor];
    self.ecaluateTextView.returnKeyType=UIReturnKeyDefault;
    self.ecaluateTextView.keyboardType=UIKeyboardTypeDefault;
    [self.ecaluateTextView.layer setMasksToBounds:YES];
    self.ecaluateTextView.font = [UIFont systemFontOfSize:FontSize(16)];
    _ecaluateTextView.placeholder=@"您的评价对我们很重要，请输入您对本次购物的评价，长度为1-500字~";
    [self.rootScrollView addSubview: self.ecaluateTextView];
    
    //底部视图
    UIButton *bottomView = [UIButton buttonWithType:UIButtonTypeCustom];
    [bottomView setTitle:@"提交评价" forState:UIControlStateNormal];
    bottomView.frame = CGRectMake(0, CGRectGetMaxY(self.view.frame) - kbottomViewHight - 64
                                  , VIEW_WIDTH
                                  , kbottomViewHight);
    [bottomView setTitleColor:[UIColor colorWithHexString:@"#ffffff"] forState:UIControlStateNormal];
    [bottomView setTitleColor:[UIColor colorWithHexString:@""] forState:UIControlStateHighlighted];
    [bottomView setBackgroundImage:[UIImage createImageWithColor:[UIColor colorWithHexString:@"#db3b34"]
                                                           frame:CGRectMake(0, 0, 1, 1)]
                          forState:UIControlStateNormal];
    
    [bottomView setBackgroundImage:[UIImage createImageWithColor:[UIColor colorWithHexString:@"#b2201b"]
                                                           frame:CGRectMake(0, 0, 1, 1)]
                          forState:UIControlStateHighlighted];
    
    bottomView.titleLabel.font = [UIFont systemFontOfSize:FontSize(16)];
    [bottomView addTarget:self action:@selector(submitRatingEvent) forControlEvents:UIControlEventTouchUpInside];
    [self.rootScrollView addSubview:bottomView];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

@end

//
//  ShoppingCart_RootCell.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/11.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>


//添加代理，用于按钮加减的实现
@protocol ShopCarTableViewCellDelegate <NSObject>

- (void)selectedItemAtIndexPath:(NSIndexPath*)indexPath selected:(BOOL)isSelected;

- (void)selectedItemAtIndexPath:(NSIndexPath *)indexPath tag:(NSInteger )tag;
@end
@interface ShoppingCart_RootCell : UITableViewCell

@property(nonatomic, strong)NSIndexPath *indexPath;
//小计
@property (retain,nonatomic)NSString *subtotalMoneytext;

@property(assign,nonatomic)id<ShopCarTableViewCellDelegate>delegate;

-(void)settingDataWithdicData:(NSDictionary *)dicData;
@end

//
//  ShoppingCartSectionHeaderView.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/4/20.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： ShoppingCartSectionHeaderModel
 Created_Date： 20160420
 Created_People： GT
 Function_description： 购物车section头部视图
 ***************************************/

#import <UIKit/UIKit.h>
@class ShoppingCartSectionHeaderModel;
@protocol ShoppingCartSectionHeaderViewDelegate <NSObject>
//选中按钮点击
-(void)selectSectionHeaderWithGoodsType:(NSInteger)goodsType viewForHeaderInSection:(NSInteger)section isSelected:(BOOL)isSelected;
@end

@interface ShoppingCartSectionHeaderView : UITableViewHeaderFooterView
@property(nonatomic, assign)NSInteger section;
@property(nonatomic,unsafe_unretained)id<ShoppingCartSectionHeaderViewDelegate>delegate;
- (void)updateViewWithShoppingCartSectionHeaderModel:(ShoppingCartSectionHeaderModel*)model;
@end

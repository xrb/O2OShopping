//
//  ShoppingCartViewModel.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/4/20.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： ShoppingCartViewModel
 Created_Date： 20160420
 Created_People： GT
 Function_description： 购物车数据逻辑处理
 ***************************************/

#import <Foundation/Foundation.h>
#import "ShoppingCartBottomModel.h"
@interface ShoppingCartViewModel : NSObject
@property(nonatomic,strong)NSMutableArray *sectionHeaderDatasArr;//section头部信息
@property(nonatomic,strong)ShoppingCartBottomModel *bottomModel;//购物车底部
//获取购物车数据
- (void)requestShoppingCartDatas:(void (^)(NSArray * goodsData))shoppingCartDataBlock;
//选中所有商品
- (void)updateAllDataWithSelectedStatus:(BOOL)isSelected success:(void (^)(NSArray * goodsData))success;
//根据是否选中sectionHeader来改变section数据
- (void)updateSectionDataWithSelectSectionHeaderWithGoodsType:(NSInteger)goodsType viewForHeaderInSection:(NSInteger)section isSelected:(BOOL)isSelected success:(void (^)(NSArray * goodsData))success;
//根据选中cell来改变数据源
- (void)updateCellDataWithSelectedCellIndexPath:(NSIndexPath*)indexPath isSelected:(BOOL)isSelected success:(void (^)(NSArray * goodsData))success;
//商品价格改变
- (void)updateCellDataWithSelectedCellIndexPath:(NSIndexPath *)indexPath goodsCount:(NSInteger)goodsCount success:(void (^)(NSArray *goodsData))success;
//删除单个商品
- (void)deleteGoodsAtIndexPath:(NSIndexPath*)indexPath success:(void (^)(NSArray *goodsData))success;
//多选删除
- (void)deleteSelectedGoodsSuccess:(void (^)(NSArray *goodsData))success;
//添加关注
- (void)markGoodsSuccess:(void (^)(NSString *successMsg))success failure:(void(^)(NSString *failureMessage))failureMessage;
@end

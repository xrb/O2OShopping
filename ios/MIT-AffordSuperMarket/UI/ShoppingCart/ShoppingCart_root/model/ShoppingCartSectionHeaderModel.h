//
//  ShoppingCartSectionHeaderModel.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/4/20.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： ShoppingCartSectionHeaderModel
 Created_Date： 20160420
 Created_People： GT
 Function_description： 购物车section头部视图模型
 ***************************************/

#import <Foundation/Foundation.h>

@interface ShoppingCartSectionHeaderModel : NSObject
@property(nonatomic, assign)NSInteger storeType;//便利店类型 1直营 0便利店
@property(nonatomic, strong)NSString *title;//便利店名字
@property(nonatomic)BOOL isSelected;//是否被选中
@end

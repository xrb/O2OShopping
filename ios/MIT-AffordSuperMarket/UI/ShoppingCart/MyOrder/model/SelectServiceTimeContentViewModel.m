//
//  SelectServiceTimeContentViewModel.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/12/24.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "SelectServiceTimeContentViewModel.h"
//model
#import "SelectServiceTimeContentItemModel.h"
@interface SelectServiceTimeContentViewModel () {
    NSInteger _chaZhi;//当天差得时间段
}
@property(nonatomic, strong)NSMutableArray *originalDataArr;
@property(nonatomic, strong)NSMutableArray *filterDataSource;//过滤后的数据
@property(nonatomic, strong)NSMutableArray *rehandleDataSource;
@end
@implementation SelectServiceTimeContentViewModel

- (void)initDataWithArray:(NSArray*)dataArr {
    _originalDataArr = [[NSMutableArray alloc]initWithArray:dataArr];
    _filterDataSource = [[NSMutableArray alloc]init];
    _rehandleDataSource = [[NSMutableArray alloc]init];
    [self filterOriginalData];
}
- (NSArray*)getServiceTimeContentWithSelectedDateIndex:(NSInteger)index {
    
    return [self.rehandleDataSource objectAtIndex:index];
}


#pragma mark -- 对数据进行处理
//过滤原始数据，对数据进行提取
- (void)filterOriginalData {
    [self.filterDataSource removeAllObjects];
    for (NSDictionary *dataDic in self.originalDataArr) {
        [self.filterDataSource addObject:[dataDic objectForKey:@"HOURS"]];
    }
    //对当天数据进行处理
    [self.filterDataSource insertObject:[self changeCurrentDayTimeDataSource] atIndex:0];
    [self changeServiceTimeItemDataToModel];
}

//处理当日数据
- (NSArray*)changeCurrentDayTimeDataSource {
    NSMutableArray *todayTimes = [[NSMutableArray alloc]init];
    if (self.filterDataSource.count != 0) {
        //判断当日服务端返回时间是否和其他天返回时间段一样，如果不一样需要自己进行补全
        if ([[self.filterDataSource firstObject] count] != [[self.filterDataSource lastObject] count]) {
            _chaZhi = [[self.filterDataSource lastObject] count] - [[self.filterDataSource firstObject] count];
            NSArray *topArr = [[self.filterDataSource lastObject] subarrayWithRange:NSMakeRange(0, _chaZhi)];//获取当天缺少的时间段
            //组装今天时间段
            [todayTimes addObjectsFromArray:topArr];
            [todayTimes addObjectsFromArray:[self.filterDataSource firstObject]];
        }
    }
    [self.filterDataSource removeObjectAtIndex:0];
    return [todayTimes copy];
}

//把数据放到model里面
- (void)changeServiceTimeItemDataToModel {
    NSInteger i = 0;
    [self.rehandleDataSource removeAllObjects];
    for (NSArray *dataArr in self.filterDataSource) {
        NSMutableArray *array = [[NSMutableArray alloc]init];
        for (NSInteger j = 0 ; j < [dataArr count] ; j++) {
            NSDictionary *dataDic = [dataArr objectAtIndex:j];
            SelectServiceTimeContentItemModel *model = [[SelectServiceTimeContentItemModel alloc]init];
            model.enable = YES;
            if (i == 0 && j < _chaZhi) {
            model.enable = NO;
            }
            model.time = [dataDic objectForKey:@"TITLE"];
            model.date = [dataDic objectForKey:@"DATETIME"];
            [array addObject:model];
        }
        [self.rehandleDataSource addObject:array];
        i++;
    }
}

@end

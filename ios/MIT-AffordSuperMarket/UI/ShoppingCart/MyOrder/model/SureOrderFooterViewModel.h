//
//  SureOrderFooterViewModel.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/21.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SureOrderFooterViewModel : NSObject
@property(nonatomic, strong)NSString *youHuiPrice;
@property(nonatomic, strong)NSString *totalPrice;
@property(nonatomic, strong)NSString *factPrice;
@end

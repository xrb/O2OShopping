//
//  SureOrderCell.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 15/11/21.
//  Copyright © 2015年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "SureOrderCell.h"
#import "Global.h"
#define kleftSpace 15
#define kcellHeight HightScalar(47)
//uintils
#import "UIColor+Hex.h"
#import "Global.h"
@interface SureOrderCell ()
@property(nonatomic, strong)UILabel *contentLabel;
@property(nonatomic, strong)UILabel *countLabel;
@property(nonatomic, strong)UILabel *priceLabel;
@property (nonatomic,strong)UILabel *homeMake;
@end
@implementation SureOrderCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setUpSuberView];
    }
    return  self;
    
}

#pragma mark -- 初始化视图
- (void)setUpSuberView {
    if (!_homeMake) {
    //家政服务 标题名字
    _homeMake =[[UILabel alloc]initWithFrame:CGRectMake(15, 0, 60, kcellHeight)];
    _homeMake.textColor=[UIColor colorWithHexString:@"#555555"];
    _homeMake.font=[UIFont systemFontOfSize:FontSize(14.0)];
    _homeMake.hidden=YES;
    [self.contentView addSubview:self.homeMake];
    }
    if (!_priceLabel) {
        _priceLabel = [self setLabelWithTitle:@"￥116" fontSize:FontSize(15) textColor:@"#555555"];
        _priceLabel.frame = CGRectMake(VIEW_WIDTH - kleftSpace - 65
                                       , 0
                                       , 65
                                       , kcellHeight);
        _priceLabel.textAlignment = NSTextAlignmentRight;
        _priceLabel.minimumScaleFactor = 0.8;
        _priceLabel.adjustsFontSizeToFitWidth = YES;
        [self.contentView addSubview:_priceLabel];
    }
    
    if (!_countLabel) {
        _countLabel = [self setLabelWithTitle:@"X 4" fontSize:FontSize(15) textColor:@"#555555"];
        _countLabel.frame = CGRectMake(CGRectGetMinX(_priceLabel.frame) - 40
                                       , CGRectGetMinY(_priceLabel.frame)
                                       , 40
                                       , CGRectGetHeight(_priceLabel.bounds));
        _countLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:_countLabel];
    }
    if (!_contentLabel) {
        _contentLabel = [self setLabelWithTitle:@"红星蓝瓶八年陈酿二锅头53度500毫升" fontSize:FontSize(15) textColor:@"#555555"];
        _contentLabel.frame = CGRectMake(kleftSpace
                                         , CGRectGetMinY(_countLabel.frame)
                                         , CGRectGetMinX(_countLabel.frame) - kleftSpace
                                         , CGRectGetHeight(_countLabel.bounds));
        _contentLabel.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:_contentLabel];
    }
}
- (UILabel *)setLabelWithTitle:(NSString*)title fontSize:(NSInteger)fontSize textColor:(NSString*)textColor {
    UILabel *label = [[UILabel alloc]init];
    label.text = title;
    label.font = [UIFont systemFontOfSize:fontSize];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor colorWithHexString:textColor];
    return label;
}

#pragma mark -- 初始化数据
- (void)updateData:(NSDictionary*)dataDic  hoemMakeType:(NSString *)homeMakeType{
     NSString *fuHao = @"X ";
    if ([[dataDic allKeys] containsObject:@"SERVICESID"]) {//服务订单
        self.contentLabel.text = [dataDic objectForKey:@"TITLE"];
//        self.countLabel.text = [NSString stringWithFormat:@"%@%@",fuHao,@"1"];
        self.countLabel.hidden =YES;
        self.priceLabel.text =[NSString stringWithFormat:@"￥%.2f",[[dataDic objectForKey:@"SELLPRICE"] floatValue]];
        if (homeMakeType.length !=0) {
        _homeMake.hidden=NO;
        if ([homeMakeType isEqualToString:@"1"]) {
            _homeMake.text=@"日常保洁";
        }else{
            _homeMake.text=@"深度保洁";
        }
        self.contentLabel.frame= CGRectMake(VIEW_WIDTH/3, CGRectGetMinY(self.homeMake.frame), VIEW_WIDTH/2+50, kcellHeight);
            self.contentLabel.textColor =[UIColor colorWithHexString:@"#fd8f8e"];
            self.priceLabel.textColor =[UIColor colorWithHexString:@"#fd8f8e"];
        }
    } else {
        self.contentLabel.text = [dataDic objectForKey:@"NAME"];
        self.countLabel.text = [NSString stringWithFormat:@"%@%@",fuHao,[dataDic objectForKey:@"BUYNUM"]];
        self.priceLabel.text =[NSString stringWithFormat:@"￥%.2f",[[dataDic objectForKey:@"SELLPRICE"] floatValue]];
    }
    
}
- (void)updateGoodsData:(NSDictionary*)dataDic {
     NSString *fuHao = @"X ";
    self.contentLabel.text = [dataDic objectForKey:@"NAME"];
    self.countLabel.text = [NSString stringWithFormat:@"%@%@",fuHao,[dataDic objectForKey:@"BUYNUM"]];
    self.priceLabel.text =[NSString stringWithFormat:@"￥%.2f",[[dataDic objectForKey:@"SELLPRICE"] floatValue]];
}
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

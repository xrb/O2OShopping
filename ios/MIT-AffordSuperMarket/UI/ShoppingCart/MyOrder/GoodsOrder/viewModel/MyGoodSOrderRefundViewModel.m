//
//  MyGoodSOrderRefundViewModel.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/3/23.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "MyGoodSOrderRefundViewModel.h"
//model
#import "MyGoodSOrderRefundModel.h"
//unitls
#import "Global.h"
#import "ManagerGlobeUntil.h"
#import "ManagerHttpBase.h"
#import "NSString+Conversion.h"
//vendor
#import "MJExtension.h"
typedef void (^Finished) (BOOL isFinished);
@interface MyGoodSOrderRefundViewModel ()
@property(nonatomic,weak)Finished isFinished;
@end
@implementation MyGoodSOrderRefundViewModel
- (id)init {
    self = [super init];
    if (self) {
        _dataSource = [[NSMutableArray alloc]init];
    }
    return self;
}
- (void)parameters:(NSMutableDictionary *)parameters requestSuccess:(void (^)(BOOL isSuccess))success failure:(void(^)(NSString *failureInfo))failureInfo{
    __weak typeof(self) weakSelf = self;
    if ([[parameters objectForKey:@"PAGENUM"]isEqualToString:@"1"]) {
        [self.dataSource removeAllObjects];

    }
       ManagerHttpBase *manager=[ManagerHttpBase sharedManager];
    [manager parameters:parameters customPOST:@"order/refund" success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject isKindOfClass:[NSDictionary class]]) {//判读返回数据是否是字典
            NSDictionary *dataDic = (NSDictionary*)responseObject;
            NSString *msg = [dataDic objectForKey:@"MSG"];
            NSString *state = [NSString stringTransformObject:[dataDic objectForKey:@"STATE"]];//状态码
            
            if ([state isEqualToString:@"0"]) {//请求成功
                weakSelf.isFinished = success;
                self.stateMent =[NSString stringTransformObject:[[dataDic objectForKey:@"OBJECT"] objectForKey:@"STATEMENT"]];
                self.totalCount =[NSString stringTransformObject:[[dataDic objectForKey:@"OBJECT"] objectForKey:@"TOTALROW"]];
                [weakSelf handleDataSourceWithDatas:[[dataDic objectForKey:@"OBJECT"] objectForKey:@"LIST"]];
            } else {
                [weakSelf.dataSource removeAllObjects];
                failureInfo(msg);
            }
        }
    } failure:^(bool isFailure) {
        failureInfo(@"请求超时");
    }];
}
- (void)handleDataSourceWithDatas:(NSArray*)dataArr
{
    if (dataArr.count == 0) {
        self.isFinished(YES);
        return ;
    }
   
    [self.dataSource addObjectsFromArray:[MyGoodSOrderRefundModel mj_objectArrayWithKeyValuesArray:dataArr]];
    self.isFinished(YES);
}
@end

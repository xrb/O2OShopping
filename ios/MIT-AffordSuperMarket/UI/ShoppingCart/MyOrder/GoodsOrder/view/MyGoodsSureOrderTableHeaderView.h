//
//  MyGoodsSureOrderTableHeaderView.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/4/8.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
/***************************************
 ClassName： MyGoodsSureOrderTableHeaderView
 Created_Date： 20160408
 Created_People：GT
 Function_description： 商品提交订单table头部视图
 ***************************************/

#import <UIKit/UIKit.h>
typedef NS_ENUM(NSInteger, SelectedShoppingCartGoodsType) {
    konlySelectedStoreGoods = 0,//只选中便利店商品
    konlySelectedSelfGoods = 1 ,//只选中自营商品
    kselectedStoreGoodsAndSelfGoods//选中包含便利店和自营商品
};
typedef NS_ENUM(NSInteger, SelectPayTypeOrSendGoodsType) {
    kselectPayType = 0,
    kselectSendGoodsType
};
@protocol MyGoodsSureOrderTableHeaderViewDelegate<NSObject>
- (void)selectPayTypeOrSendGoodsTypeWithType:(SelectPayTypeOrSendGoodsType)type;
- (void)reSetMyGoodsReceivedAddress;
@end

@interface MyGoodsSureOrderTableHeaderView : UIView
@property(nonatomic, unsafe_unretained)id<MyGoodsSureOrderTableHeaderViewDelegate>delegate;
@property(nonatomic, strong)NSString *storeGoodsPayType;//便利店商品支付方式
@property(nonatomic, strong)NSString *storeGoodsSendProductType;//便利店商品收货方式
@property(nonatomic, strong)NSString *selfGoodsPayType;//自营商品支付方式
@property(nonatomic, strong)NSString *selfGoodsSendProductType;//自营商品收货方式
@property(nonatomic, strong)NSString *sexType;//性别类型
@property(nonatomic, strong)UITextField *userNameTextField;//用户名输入框
@property(nonatomic, strong)UITextField *userTelePhoneTextField;//用户联系电话输入框
@property(nonatomic, strong)UITextField *userAddressTextField;//用户详细地址
@property(nonatomic, strong)NSString *peiSongIntroduceMessage;//配送介绍信息
- (instancetype)initWithFrame:(CGRect)frame hadOrdersAddressStatus:(BOOL)isHadOrdersAddress selectedGoodsType:(SelectedShoppingCartGoodsType)selectedGoodsType;
- (void)updataWithName:(NSString*)name telNumber:(NSString*)telNumber address:(NSString*)address;
@end

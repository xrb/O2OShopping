//
//  MyGoodsOrderTrackingCell.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/3/15.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "MyGoodsOrderTrackingCell.h"
//untils
#import "Global.h"
#import "UIImage+ColorToImage.h"
#import "UIColor+Hex.h"
#import "NSString+TextSize.h"
#import "NSString+Conversion.h"
#define klogoImageWidth  HightScalar(23)
#define kleftSpace 16
#define klineImageHight 1
#define kupLineImageHight 10
#define ktopSpace 8
@interface MyGoodsOrderTrackingCell ()
{
    
}
@property(nonatomic, strong)UIImageView *upLineImageView;
@property(nonatomic, strong)UIImageView *logoImageView;
@property(nonatomic, strong)UIImageView *downLineImageView;
@property(nonatomic, strong)UILabel *contentLabel;
@property(nonatomic, strong)UILabel *dateLabel;
@property(nonatomic, strong)UIImageView *sepLineImageView;
@property(nonatomic, strong)UIImage *lineImage;
@end
@implementation MyGoodsOrderTrackingCell


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self =[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setUpSuberView];
    }
    return self;
}

#pragma mark -- 初始化数据
- (void)updateViewWithData:(MyGoodsOrderTrackingModel*)model {
    
    [self changeViewFrameWith:model];
}
//改变frame
- (void)changeViewFrameWith:(MyGoodsOrderTrackingModel*)model {
    UIImage *image = [UIImage imageNamed:@"tail_history_ever"];
    if (model.isFirst) {
        image = [UIImage imageNamed:@"tail_history_cur"];
        self.upLineImageView.hidden = YES;
    } else if (model.isLast) {
        self.downLineImageView.hidden = YES;
    } else {
        self.upLineImageView.hidden = NO;
        self.downLineImageView.hidden =  NO;
    }
    self.logoImageView.image = image;
    //上部指示线
    CGRect upLineImageFrame = self.upLineImageView.frame;
    upLineImageFrame.origin.y = 0;
    upLineImageFrame.origin.x = kleftSpace + image.size.width/2;
    upLineImageFrame.size.height = kupLineImageHight;
    self.upLineImageView.frame = upLineImageFrame;
    //logo视图
    CGRect logoImageFrame = self.logoImageView.frame;
    logoImageFrame.size.width = image.size.width;
    logoImageFrame.size.height = image.size.height;
    self.logoImageView.frame = logoImageFrame;
    //内容视图
    CGRect contentFrame = self.contentLabel.frame;
    CGFloat contentFactWidth = VIEW_WIDTH - CGRectGetMaxX(self.logoImageView.frame) - 2*kleftSpace;
    contentFrame.origin.x = CGRectGetMaxX(self.logoImageView.frame) + kleftSpace;
    contentFrame.origin.y = ktopSpace;
    contentFrame.size.width = contentFactWidth;
    CGSize contentSize = [model.handleContent sizeWithFont:[UIFont systemFontOfSize:FontSize(15)]
                                                   maxSize:CGSizeMake(MAXFLOAT, 120)];
    CGFloat factHight = 0;
    if (contentSize.width <= contentFactWidth) {
        factHight = FontSize(15) + 2*6;
    }else if (contentSize.width <= 2*contentFactWidth) {
        factHight = 2*FontSize(15) + 2*6;
    } else if (contentSize.width <= 3*contentFactWidth) {
        factHight = 3*FontSize(15) + 2*6;
    } else if (contentSize.width <= 4*contentFactWidth) {
        factHight = 4*FontSize(15) + 2*6;
    }
    contentFrame.size.height = factHight;
    self.contentLabel.frame = contentFrame;
    
    //日期视图
    CGRect dateFrame = self.dateLabel.frame;
    dateFrame.origin.x = CGRectGetMinX(self.contentLabel.frame);
    dateFrame.origin.y = CGRectGetMaxY(self.contentLabel.frame);
    dateFrame.size.width = CGRectGetWidth(self.contentLabel.frame);
    dateFrame.size.height = FontSize(13) + 6;
    self.dateLabel.frame = dateFrame;
    
    //分割线视图
    CGRect sepLineFrame = self.sepLineImageView.frame;
    sepLineFrame.origin.x = CGRectGetMinX(self.dateLabel.frame);
    sepLineFrame.origin.y = CGRectGetMaxY(self.dateLabel.frame) + 8 - klineImageHight;
    sepLineFrame.size.width = VIEW_WIDTH - CGRectGetMinX(self.dateLabel.frame);
    sepLineFrame.size.height = klineImageHight;
    self.sepLineImageView.frame = sepLineFrame;
    
    //下部指示线
    CGRect downLineImageFrame = self.downLineImageView.frame;
    downLineImageFrame.origin.y = CGRectGetMaxY(self.logoImageView.frame);
    downLineImageFrame.origin.x = CGRectGetMinX(self.upLineImageView.frame);
    downLineImageFrame.size.height = CGRectGetMaxY(self.sepLineImageView.frame) - CGRectGetMaxY(self.logoImageView.frame);
    self.downLineImageView.frame = downLineImageFrame;
    
    [self resetViewValueWith:model];
}
//赋值
- (void)resetViewValueWith:(MyGoodsOrderTrackingModel*)model {
    
    self.contentLabel.text = [NSString stringTransformObject:model.handleContent];
    self.dateLabel.text = [NSString stringTransformObject:model.handleDate];
}
#pragma mark -- cell实际高度
- (CGFloat)cellFactHight {
    return CGRectGetMaxY(self.sepLineImageView.frame);
}
#pragma mark -- 初始化视图
- (void)setUpSuberView {
    [self.contentView addSubview:self.upLineImageView];
    [self.contentView addSubview:self.logoImageView];
    [self.contentView addSubview:self.downLineImageView];
    [self.contentView addSubview:self.contentLabel];
    [self.contentView addSubview:self.dateLabel];
    [self.contentView addSubview:self.sepLineImageView];
}
- (UIImage*)lineImage {
    if (!_lineImage) {
//        UIImage *image = [UIImage createImageWithColor:[UIColor colorWithHexString:@"#666666"]
//                                                 frame:CGRectMake(0, 0, klineImageHight, klineImageHight)];
        UIImage *image = [UIImage imageNamed:@"news_line_center"];
        _lineImage = image;
    }
    return _lineImage;
}
//上边距线
- (UIImageView*)upLineImageView {
    if (!_upLineImageView) {
        CGRect frame = CGRectMake(kleftSpace + klogoImageWidth/2
                                  , 0
                                  , klineImageHight
                                  , kupLineImageHight);
        UIImageView *imageView = [[UIImageView alloc]initWithFrame:frame];
        imageView.image = self.lineImage;
        _upLineImageView = imageView;
    }
    return _upLineImageView;
}
//logo视图
- (UIImageView*)logoImageView {
    if (!_logoImageView) {
        
        CGRect frame = CGRectMake(kleftSpace
                                  , CGRectGetMaxY(self.upLineImageView.frame)
                                  , klogoImageWidth
                                  , klogoImageWidth);
        UIImageView *imageView = [[UIImageView alloc]initWithFrame:frame];
        _logoImageView = imageView;
    }
    return _logoImageView;
}
//下边距线
- (UIImageView*)downLineImageView {
    if (!_downLineImageView) {
        UIImageView *imageView = [[UIImageView alloc]initWithImage:self.lineImage];
        imageView.frame = CGRectMake(CGRectGetMinX(self.upLineImageView.frame)
                                     , CGRectGetMaxY(self.logoImageView.frame)
                                     , klineImageHight
                                     , 60);
        _downLineImageView = imageView;
    }
    return _downLineImageView;
}

//内容视图
- (UILabel*)contentLabel {
    if (!_contentLabel) {
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectZero];
        label.textColor = [UIColor colorWithHexString:@"#777777"];
        label.font = [UIFont systemFontOfSize:FontSize(15)];
        label.numberOfLines = 0;
        _contentLabel = label;
    }
    return _contentLabel;
}

//日期
- (UILabel*)dateLabel {
    if (!_dateLabel) {
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectZero];
        label.textColor = [UIColor colorWithHexString:@"#999999"];
        label.font = [UIFont systemFontOfSize:FontSize(13)];
        _dateLabel = label;
    }
    return _dateLabel;
}

//分割线
- (UIImageView*)sepLineImageView {
    if (!_sepLineImageView) {
        UIImageView *imageView = [[UIImageView alloc]initWithImage:self.lineImage];
        _sepLineImageView = imageView;
    }
    return _sepLineImageView;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

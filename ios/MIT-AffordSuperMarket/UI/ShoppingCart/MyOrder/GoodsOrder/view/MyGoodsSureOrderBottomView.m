//
//  MyGoodsSureOrderBottomView.m
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/4/8.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//

#import "MyGoodsSureOrderBottomView.h"
//unitls
#import "Global.h"
#import "NSString+TextSize.h"
#import "UIColor+Hex.h"
#import "UIImage+ColorToImage.h"
#define kleftSpace 15
#define ksubmitButtonWidth 100
@interface MyGoodsSureOrderBottomView () {
    NSString *_storeGoodsTotalPrice;
    NSString *_selfGoodsTotalPrice;
    NSString *_totalPrice;
}
@property(nonatomic, strong)UILabel *onlinePayLabel;//在线支付
@property(nonatomic, strong)UILabel *cashOnDeliveryLable;//货到付款
@end
@implementation MyGoodsSureOrderBottomView
- (id)initWithFrame:(CGRect)frame goodsType:(NSInteger)goodsType storeGoodsPrice:(NSString*)storeGoodsPrice selfGoodsPrice:(NSString*)selfGoodsPrice goodsTotalPrice:(NSString*)totalPrice {
    self = [super initWithFrame:frame];
    if (self) {
        _storeGoodsTotalPrice = storeGoodsPrice;
        _selfGoodsTotalPrice = selfGoodsPrice;
        _totalPrice = totalPrice;
        [self setupSubViewWithgoodsType:goodsType storeGoodsPrice:storeGoodsPrice selfGoodsPrice:selfGoodsPrice];
    }
    return self;
}

#pragma mark -- 初始化视图
- (void)setupSubViewWithgoodsType:(NSInteger)goodsType storeGoodsPrice:(NSString*)storeGoodsPrice selfGoodsPrice:(NSString*)selfGoodsPrice {

    //货到付款
    _cashOnDeliveryLable = [[UILabel alloc]initWithFrame:CGRectMake(kleftSpace
                                                                       , 0
                                                                       , VIEW_WIDTH/3*2
                                                                       ,29)];
    _cashOnDeliveryLable.font = [UIFont systemFontOfSize:FontSize(17)];
    _cashOnDeliveryLable.textColor = [UIColor whiteColor];
    _cashOnDeliveryLable.textAlignment = NSTextAlignmentLeft;
    _cashOnDeliveryLable.backgroundColor = [UIColor clearColor];
    _cashOnDeliveryLable.hidden = YES;
    [self addSubview:_cashOnDeliveryLable];
     //在线支付
    _onlinePayLabel = [[UILabel alloc]initWithFrame:CGRectMake(kleftSpace
                                                                       , 0
                                                                       , CGRectGetWidth(_cashOnDeliveryLable.bounds)
                                                                       , 58)];
    _onlinePayLabel.font = [UIFont systemFontOfSize:FontSize(17)];
    _onlinePayLabel.textColor = [UIColor whiteColor];
    _onlinePayLabel.textAlignment = NSTextAlignmentLeft;
    _onlinePayLabel.backgroundColor = [UIColor clearColor];
    _onlinePayLabel.text = [NSString stringWithFormat:@"在线支付：￥%@",_totalPrice];
    [self addSubview:_onlinePayLabel];
    
    
     //提交按钮
    UIImage *backGroundImage = [UIImage createImageWithColor:[UIColor colorWithHexString:@"#db3b34"]
                                                       frame:CGRectMake(0, 0, 1, 1)];
    UIButton *submitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    submitBtn.frame = CGRectMake(CGRectGetWidth(self.bounds) - HightScalar(ksubmitButtonWidth)
                                    , CGRectGetMinY(_onlinePayLabel.frame)
                                    , HightScalar(ksubmitButtonWidth)
                                    , CGRectGetHeight(_onlinePayLabel.bounds));
    [submitBtn setTitle:@"提交订单" forState:UIControlStateNormal];
    [submitBtn setBackgroundImage:backGroundImage forState:UIControlStateNormal];
    [submitBtn addTarget:self action:@selector(submitOrderEvent:) forControlEvents:UIControlEventTouchUpInside];
    submitBtn.titleLabel.font = [UIFont systemFontOfSize:FontSize(17)];
    [self addSubview:submitBtn];

}

- (void)updateViewWithGoodsType:(NSInteger)goodsType storeGoodsPayType:(NSString*)storeGoodsPayType selfGoodsPayType:(NSString*)selfGoodsPayType storeGoodsPrice:(NSString*)storeGoodsPrice selfGoodsPrice:(NSString*)selfGoodsPrice goodsTotalPrice:(NSString*)totalPrice offSetstoreGoodsPrice:(NSString*)offSetstoreGoodsPrice offSetselfGoodsPrice:(NSString*)offSetselfGoodsPrice offSetgoodsTotalPrice:(NSString*)offSettotalPrice switchState:(BOOL)switchState{
    //选中商品类型:0只包含便利店商品 1只包含自营商品 2包含便利店和自营商品
    if (goodsType == 0 ) {//便利店商品价格
        if ([storeGoodsPayType isEqualToString:@"1"]) {//在线支付
        if (switchState ==YES) {
            self.onlinePayLabel.text = [NSString stringWithFormat:@"在线支付：￥%@",offSetstoreGoodsPrice];
            }else{
            self.onlinePayLabel.text = [NSString stringWithFormat:@"在线支付：￥%@",storeGoodsPrice];
            }
        } else if ([storeGoodsPayType isEqualToString:@"2"]) {//货到付款
            self.onlinePayLabel.text = [NSString stringWithFormat:@"货到付款：￥%@",storeGoodsPrice];
        }
    } else if (goodsType == 1) {//自营商品价格
        if (switchState ==YES) {
            self.onlinePayLabel.text = [NSString stringWithFormat:@"在线支付：￥%@",offSetselfGoodsPrice];
        }else{
            self.onlinePayLabel.text = [NSString stringWithFormat:@"在线支付：￥%@",selfGoodsPrice];
        }
    } else if (goodsType == 2) {//便利店和自营商品价格
        CGRect onlinePayLabelFrame = self.onlinePayLabel.frame;
        CGRect cashOnDeliveryLableFrame = self.cashOnDeliveryLable.frame;
        if ([storeGoodsPayType isEqualToString:@"1"]) {//在线支付
            self.cashOnDeliveryLable.hidden = YES;
            onlinePayLabelFrame.origin.y = 0;
            onlinePayLabelFrame.size.height = 58;
            self.onlinePayLabel.frame = onlinePayLabelFrame;
            if (switchState ==YES) {
                self.onlinePayLabel.text = [NSString stringWithFormat:@"在线支付：￥%@",offSettotalPrice];
            }else{
                self.onlinePayLabel.text = [NSString stringWithFormat:@"在线支付：￥%@",totalPrice];
            }
        } else if ([storeGoodsPayType isEqualToString:@"2"]) {//货到付款
            self.cashOnDeliveryLable.hidden =  NO;
            cashOnDeliveryLableFrame.origin.y = 5;
            cashOnDeliveryLableFrame.size.height = 24;
            self.cashOnDeliveryLable.frame = cashOnDeliveryLableFrame;
            onlinePayLabelFrame.origin.y = CGRectGetMaxY(self.cashOnDeliveryLable.frame);
            onlinePayLabelFrame.size.height = CGRectGetHeight(self.cashOnDeliveryLable.bounds);
            self.onlinePayLabel.frame = onlinePayLabelFrame;
            if (switchState ==YES) {
                self.cashOnDeliveryLable.text = [NSString stringWithFormat:@"货到付款：￥%@",storeGoodsPrice];
                self.onlinePayLabel.text = [NSString stringWithFormat:@"在线支付：￥%@",offSetselfGoodsPrice];
            }else{
                self.cashOnDeliveryLable.text = [NSString stringWithFormat:@"货到付款：￥%@",storeGoodsPrice];
                self.onlinePayLabel.text = [NSString stringWithFormat:@"在线支付：￥%@",selfGoodsPrice];
            }
           
        }
    }
}
#pragma mark -- button Action
- (void)submitOrderEvent:(id)sender {
    if (_delegate && [_delegate respondsToSelector:@selector(submitOrder)]) {
        [_delegate submitOrder];
    }
}

@end

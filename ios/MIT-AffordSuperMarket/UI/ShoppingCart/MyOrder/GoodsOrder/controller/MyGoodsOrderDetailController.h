//
//  MyGoodsOrderDetailController.h
//  MIT-AffordSuperMarket
//
//  Created by apple on 16/3/14.
//  Copyright © 2016年 河南大实惠电子商务有限公司. All rights reserved.
//
/***************************************
 ClassName： MyGoodsOrderDetailController
 Created_Date： 20160314
 Created_People：JSQ
 Function_description： 商品订单详情
 ***************************************/

#import "BaseViewController.h"

@interface MyGoodsOrderDetailController : BaseViewController
@property(nonatomic, strong)NSString *orderNum;
@end

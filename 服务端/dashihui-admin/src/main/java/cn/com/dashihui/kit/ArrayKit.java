package cn.com.dashihui.kit;

import java.util.List;
import java.util.Map;

@SuppressWarnings("rawtypes")
public class ArrayKit {
	public static boolean isEmpty(List list){
		return list==null||list.size()==0;
	}
	
	public static boolean isEmpty(Map map){
		return map==null||map.size()==0;
	}
}

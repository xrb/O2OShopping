package cn.com.dashihui.web.controller;

import java.util.HashMap;
import java.util.Map;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresPermissions;

import com.jfinal.kit.JsonKit;
import com.jfinal.kit.StrKit;

import cn.com.dashihui.web.base.BaseController;
import cn.com.dashihui.web.dao.WxArticleCategory;
import cn.com.dashihui.web.service.WxArticleCategoryService;

@RequiresAuthentication
public class WxArticleCategoryController extends BaseController{
	private WxArticleCategoryService service = new WxArticleCategoryService();

	@RequiresPermissions(value={"wx:dashihui01:article:category:index","wx:iotocy:article:category:index"},logical=Logical.OR)
    public void index(){
		if(SecurityUtils.getSubject().isPermitted("wx:dashihui01:article:category:index")){
			setAttr("mp", 1);
		}else if(SecurityUtils.getSubject().isPermitted("wx:iotocy:article:category:index")){
			setAttr("mp", 2);
		}
        render("index.jsp");
    }
	
	public void list(){
		int mp = getParaToInt("mp");
		renderSuccess(service.findAllWithCount(mp));
	}

	public void all(){
		int mp = getParaToInt("mp");
		renderText(JsonKit.toJson(service.findAll(mp)));
	}

	public void toAdd(){
		setAttr("mp", getPara("mp"));
		render("add.jsp");
	}
	
	/**
	 * 添加
	 * @return -1：异常，0：成功，1：分类名称为空，2：分类名称过长
	 */
	public void doAdd(){
		//分类名
		String name = getPara("name");
		//归属公众号
		String mp = getPara("mp");
		//排序
		int orderNo = getParaToInt("orderNo",0);
		if(StrKit.isBlank(name)){
			renderResult(1);
			return;
		}else if(name.length()>50){
			renderResult(2);
			return;
		}else{
			//保存
			WxArticleCategory category = new WxArticleCategory()
				.set("mp", mp)
				.set("name", name)
				.set("orderNo", orderNo);
			if(service.addCategory(category)){
				renderSuccess(service.findById(category.getInt("id")));
				return;
			}
		}
		renderFailed();
	}

	public void toEdit(){
		int id = getParaToInt(0,0);
		if(id!=0){
			setAttr("object", service.findById(id));
		}
		render("edit.jsp");
	}
	
	/**
	 * 更新
	 * @return -1：异常，0：成功，1：分类名称为空，2：分类名称过长
	 */
	public void doEdit(){
		//ID
		String categoryid = getPara("categoryid");
		//分类名
		String name = getPara("name");
		//排序
		int orderNo = getParaToInt("orderNo",0);
		if(StrKit.isBlank(categoryid)){
			renderFailed();
			return;
		}else if(StrKit.isBlank(name)){
			renderResult(1);
			return;
		}else if(name.length()>50){
			renderResult(2);
			return;
		}else{
			//更新
			WxArticleCategory category = new WxArticleCategory()
				.set("id", categoryid)
				.set("name", name)
				.set("orderNo", orderNo);
			if(service.editCategory(category)){
				renderSuccess(service.findById(Integer.valueOf(categoryid)));
				return;
			}
		}
		renderFailed();
	}
	
	/**
	 * 删除
	 * @return -1：删除失败，0：删除成功，1：分类下有文章
	 */
	public void doDelete(){
		int id = getParaToInt(0,0);
		if(id!=0){
			if(service.hasArticle(id)){
				renderResult(1);
			}else if(service.delCategory(id)){
				renderSuccess();
				return;
			}
		}
		renderFailed();
	}
	
	/**
	 * 分类排序
	 * wing
	 * 2015-10-26
	 */
	 public void doSort(){
    	Map<String,String[]> params = getParaMap();
    	if(params!=null&&params.size()!=0&&params.containsKey("sortKey")){
    		String sortKey = params.get("sortKey")[0];
    		Map<String,String> sortMap = new HashMap<String,String>();
    		for(String key : params.keySet()){
    			if(!key.equals("sortKey")){
    				String id = key.replace(sortKey, "");
    				String no = params.get(key)[0];
    				if(StrKit.isBlank(no)||no.length()>3){
    					no = "0";
    				}
    				sortMap.put(id, no);
    			}
    		}
    		service.sortCategory(sortMap);
    		renderSuccess();
    		return;
    	}
    	renderFailed();
    }
}

package cn.com.dashihui.web.dao;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.plugin.activerecord.Model;

public class Resource extends Model<Resource>{
	private static final long serialVersionUID = 1L;
	private static Resource me = new Resource();
	public static Resource me(){
		return me;
	}
	
	private boolean hasChildren = false;

	public List<Resource> getChildren() {
		return get("children");
	}

	public void setChildren(List<Resource> children) {
		put("children", children);
		hasChildren = true;
	}
	
	public boolean hasChildren(){
		return hasChildren;
	}
	
	public void addChild(Resource child){
		List<Resource> children = getChildren();
		if(children==null){
			children = new ArrayList<Resource>();
		}
		children.add(child);
		setChildren(children);
		hasChildren = true;
	}
}

package cn.com.dashihui.web.service;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

import cn.com.dashihui.web.dao.Version;

public class VersionService{
	/**
	 *分页查找版本
	 *2015-10-26
	 */
	public Page<Record> findByPage(int pageNum, int pageSize,int kind, String type){
		String sql="FROM t_sys_version A WHERE 1=1";
		List<Object> listParams = new ArrayList<Object>();
		if(kind != 0) {
			sql += " AND A.kind = ?";
			listParams.add(kind);
		}
		if(!StrKit.isBlank(type)) {
			sql += " AND A.type = ?";
			listParams.add(type);
		}
		sql += " ORDER BY A.releaseDate DESC";
		Page<Record> page = null;
		if(listParams.size() <= 0) {
			page = Db.paginate(pageNum, pageSize, "SELECT A.*", sql);
		} else {
			page = Db.paginate(pageNum, pageSize, "SELECT A.*", sql, listParams.toArray());
		}
		return page;
	}
	/**
	 *根据id查找版本
	 *2015-10-26
	 */
	public Record findById(int id){
		return Db.findById("t_sys_version", id);
	}
	/**
	 *增加版本
	 *2015-10-26
	 */
	public boolean add(Version version){
		return version.save();
	}
	/**
	 *修改版本
	 *2015-10-26
	 */
	public boolean update(Version object){
		return object.update();
	}
	/**
	 *删除版本
	 *2015-10-26
	 */
	public boolean delete(int id){
		return Db.deleteById("t_sys_version", id);
	}
}



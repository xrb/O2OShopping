package cn.com.dashihui.web.service;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

import cn.com.dashihui.web.dao.Feedback;

public class FeedbackService {
	
	public Page<Record> findByPage(int pageNum, int pageSize){
		StringBuffer sqlExcept = new StringBuffer("FROM t_sys_feedback sf LEFT JOIN t_bus_user bu ON sf.userid=bu.id");
		sqlExcept.append(" ORDER BY sf.createDate DESC");
		return Db.paginate(pageNum, pageSize, "SELECT sf.*,bu.username,bu.nickName ", sqlExcept.toString());
	}
	
	public boolean delFeedback(int id){
		return Feedback.me().deleteById(id);
	}
}

package cn.com.dashihui.web.controller;

import org.apache.shiro.authz.annotation.RequiresAuthentication;

import com.jfinal.kit.PropKit;
import com.jfinal.kit.StrKit;
import com.jfinal.log.Logger;

import cn.com.dashihui.web.base.BaseController;
import cn.com.dashihui.web.dao.Role;
import cn.com.dashihui.web.service.ResourceService;
import cn.com.dashihui.web.service.RoleService;

@RequiresAuthentication
public class RoleController extends BaseController{
	private static final Logger logger = Logger.getLogger(RoleController.class);
	private RoleService roleService = new RoleService();
	private ResourceService resourceService = new ResourceService();
    
	//@RequiresPermissions("role:index")
    public void index(){
        render("index.jsp");
    }
    
	public void page(){
		int pageNum = getParaToInt(0, 1);
		int pageSize = getParaToInt("pageSize", PropKit.getInt("constants.pageSize"));
		renderResult(0,roleService.findByPage(pageNum, pageSize));
	}
	
	public void toAdd(){
		render("add.jsp");
	}
	
	/**
	 * 添加
	 * @return -1：异常，0：成功，1：角色名称为空，2：角色名称过长，3：角色别名为空，4：角色别名过长，5：角色备注过长
	 */
	public void doAdd(){
		//角色名，如：admin、guest
		String name = getPara("name");
		//角色别名，如：管理员、游客
		String alias = getPara("alias");
		//备注
		String remark = getPara("remark");
		if(StrKit.isBlank(name)){
			renderResult(1);
			return;
		}else if(name.length()>50){
			renderResult(2);
			return;
		}else if(StrKit.isBlank(alias)){
			renderResult(3);
			return;
		}else if(alias.length()>50){
			renderResult(4);
			return;
		}else if(!StrKit.isBlank(remark)&&remark.length()>300){
			renderResult(5);
			return;
		}else{
			//保存
			if(roleService.addRole(new Role()
				.set("name", name)
				.set("alias", alias)
				.set("remark", remark))){
				renderSuccess();
				return;
			}
		}
		renderFailed();
	}
	
	public void toEdit(){
		int id = getParaToInt(0,0);
		if(id!=0){
			setAttr("object", roleService.findById(id));
		}
		render("edit.jsp");
	}
	
	/**
	 * 更新
	 * @return -1：异常，0：成功，1：角色名称为空，2：角色名称过长，3：角色别名为空，4：角色别名过长，5：角色备注过长
	 */
	public void doEdit(){
		//角色ID
		String roleid = getPara("roleid");
		//角色名，如：admin、guest
		String name = getPara("name");
		//角色别名，如：管理员、游客
		String alias = getPara("alias");
		//备注
		String remark = getPara("remark");
		if(StrKit.isBlank(roleid)){
			renderResult(6);
			return;
		}else if(StrKit.isBlank(name)){
			renderResult(1);
			return;
		}else if(name.length()>50){
			renderResult(2);
			return;
		}else if(StrKit.isBlank(alias)){
			renderResult(3);
			return;
		}else if(alias.length()>50){
			renderResult(4);
			return;
		}else if(!StrKit.isBlank(remark)&&remark.length()>300){
			renderResult(5);
			return;
		}else{
			//更新
			if(roleService.editRole(new Role()
				.set("id", roleid)
				.set("name", name)
				.set("alias", alias)
				.set("remark", remark))){
				renderSuccess();
				return;
			}
		}
		renderFailed();
	}
	
	/**
	 * 删除
	 * @return -1：删除失败，0：删除成功
	 */
	public void doDelete(){
		int id = getParaToInt(0,0);
		if(id!=0&&roleService.delRole(id)){
			logger.info("删除角色【"+id+"】，并删除相应角色用户关联信息，以及角色权限关联信息");
			renderSuccess();
			return;
		}
		renderFailed();
	}
	
	public void toAccredit(){
		int roleid = getParaToInt(0,0);
		if(roleid!=0){
			setAttr("roleid", getPara(0));
	    	setAttr("resourceList", resourceService.findResourcesByRole(roleid));
		}
		render("accredit.jsp");
	}
	
	/**
	 * 角色权限设置
	 * @return -1：失败，0：成功，1：没有选择权限
	 */
	public void doAccredit(){
		int roleid = getParaToInt("roleid",0);
		String[] resources = getParaValues("resourceid");
		if(roleid==0){
			renderFailed();
			return;
		}else if(roleService.saveRoleResource(roleid, resources)){
			//重新查询登录用户可操作的菜单
    		this.setSessionAttr("navMenus", resourceService.findMenuResourcesByUser(getCurrentUser().getInt("id")));
			renderSuccess();
			return;
		}
		renderFailed();
	}
}
package cn.com.dashihui.web.controller;

import java.util.HashMap;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresAuthentication;

import com.jfinal.kit.StrKit;
import com.jfinal.log.Logger;

import cn.com.dashihui.web.base.BaseController;
import cn.com.dashihui.web.dao.Category;
import cn.com.dashihui.web.service.CategoryService;
@RequiresAuthentication
public class CategoryController extends BaseController{
	private static final Logger logger = Logger.getLogger(CategoryController.class);
	private CategoryService categoryService = new CategoryService();
	/**
	 * 分类主页
	 * wing
	 * 2015-10-26
	 */
	public void index(){
		setAttr("categoryList", categoryService.findAllCategory());
		render("category.jsp");
	}
	/**
	 * 转到分类增加页面
	 * wing
	 * 2015-10-26
	 */
	public void toAdd(){
		int parentId = getParaToInt(0,0);
		if(parentId!=0){
			setAttr("parent", categoryService.findById(parentId));
		}
		keepPara("type");
		render("categoryadd.jsp");
	}
	/**
	 * 保存分类数据
	 * wing
	 * 2015-10-26
	 */
	public void doAdd(){
		String categoryName = getPara("categoryName");//名称
		String categoryNum = getPara("categoryNum");//编号
		int categoryType= getParaToInt("categoryType",1);//级别
		int categoryFatherId= getParaToInt("categoryFatherId",0);//父节点
		if(StrKit.isBlank(categoryName)){
			renderResult(1);
			return;
		}
		if(StrKit.isBlank(categoryNum)){
			renderResult(2);
			return;
		}
		//保存
		Category category = new Category()
			.set("categoryType", categoryType)
			.set("categoryName", categoryName)
			.set("categoryNum", categoryNum)
			.set("categoryFatherId", categoryFatherId);
		if(categoryService.addCategory(category)){
			renderSuccess(categoryService.findById(category.getInt("categoryId")));
			return;
		}else{
			renderFailed();
			logger.debug("分类保存失败");
			return;
		}
	}
	/**
	 * 转到分类修改页面
	 * wing
	 * 2015-10-26
	 */
	public void toEdit(){
		int id = getParaToInt(0,0);
		if(id!=0){
			Category category = categoryService.findById(id);
			int parentid = category.getInt("categoryFatherId");
			if(parentid!=0){
				setAttr("parent", categoryService.findById(parentid));
			}
			setAttr("object", category);
		}
		render("categoryedit.jsp");
	}
	/**
	 * 更新分类 
	 * wing
	 * 2015-10-26
	 */
	public void doEdit(){
		String categoryId= getPara("categoryId");
		String categoryName = getPara("categoryName");
		String categoryNum = getPara("categoryNum");
		if(StrKit.isBlank(categoryId)){
			renderResult(1);
			return;
		}
		if(StrKit.isBlank(categoryName)){
			renderResult(2);
			return;
		}
		if(StrKit.isBlank(categoryNum)){
			renderResult(3);
			return;
		}
		boolean isSuc=categoryService.editCategory(new Category().set("categoryId", categoryId).set("categoryName", categoryName).set("categoryNum", categoryNum));
		if(isSuc){
			renderSuccess(categoryService.findById(Integer.valueOf(categoryId)));
			return;
		}
		renderFailed();
	}
	/**
	 * 删除 1:删除父节点失败2：删除子节点失败
	 * wing
	 * 2015-10-26
	 */
	public void doDelete(){
		int id = getParaToInt(0,0);
		if(id!=0){
			categoryService.delBathByFatherId(id);
			renderSuccess();
		}
		renderFailed();
	}
	/**
	 * 分类排序
	 * wing
	 * 2015-10-26
	 */
	 public void doSort(){
    	Map<String,String[]> params = getParaMap();
    	if(params!=null&&params.size()!=0&&params.containsKey("sortKey")){
    		String sortKey = params.get("sortKey")[0];
    		Map<String,String> sortMap = new HashMap<String,String>();
    		for(String key : params.keySet()){
    			if(!key.equals("sortKey")){
    				String id = key.replace(sortKey, "");
    				String no = params.get(key)[0];
    				if(StrKit.isBlank(no)||no.length()>3){
    					no = "0";
    				}
    				sortMap.put(id, no);
    			}
    		}
    		categoryService.sortCategory(sortMap);
    		renderSuccess();
    		return;
    	}
    	renderFailed();
    }
}

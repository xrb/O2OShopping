package cn.com.dashihui.shiro;

import java.util.List;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authc.credential.DefaultPasswordService;
import org.apache.shiro.authc.credential.PasswordService;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.SimplePrincipalCollection;

import com.jfinal.plugin.activerecord.Record;

import cn.com.dashihui.kit.ArrayKit;
import cn.com.dashihui.web.dao.Admin;
import cn.com.dashihui.web.service.AdminService;
import cn.com.dashihui.web.service.ResourceService;

public class ShiroRealm extends AuthorizingRealm{
	
	/**
     * 认证回调函数,登录时调用.
     */
	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
		UsernamePasswordToken authcToken = (UsernamePasswordToken) token;
        String username = authcToken.getUsername();
        Admin user = new AdminService().findByUsername(username);
        if (null == user) {
            throw new AuthenticationException("用户不存在");
        }
        String password = String.valueOf(authcToken.getPassword());
        /**
         * 加密密码匹配方法
         * 注：注册时需要使用
         * PasswordService svc = new DefaultPasswordService();
		 * String encrypted = svc.encryptPassword(password);
		 * 来对密码加密并存储
         */
        PasswordService svc = new DefaultPasswordService();
        if(svc.passwordsMatch(password, user.getStr("password"))){
        	return new SimpleAuthenticationInfo(user, password, getName());
        }else{
        	throw new AuthenticationException("密码不匹配");
        }
	}
	
	/**
     * 授权查询回调函数, 进行鉴权但缓存中无用户的授权信息时调用.
     */
	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
		Admin user = (Admin) principals.fromRealm(getName()).iterator().next();
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        if( null == user){
        	return info;
        }
        //加载角色信息
        List<Record> roleList = new AdminService().findRolesByUser(user.getInt("id"));
        if( null == roleList){
        	return info;
        }
        if(!ArrayKit.isEmpty(roleList)){
        	for(Record record : roleList){
                //角色代码
                info.addRole(record.getStr("name"));
            }
        }
        //加载权限信息
        List<Record> resourceList = new ResourceService().findResourcesByUser(user.getInt("id"));
        if( null == resourceList){
        	return info;
        }
        if(!ArrayKit.isEmpty(resourceList)){
        	for(Record record : resourceList){
                //资源代码就是权限值，类似user：list
                info.addStringPermission(record.getStr("code"));
            }
        }
        return info;
	}
	
	/**
     * 更新用户授权信息缓存.
     */
    public void clearCachedAuthorizationInfo(String principal) {
        SimplePrincipalCollection principals = new SimplePrincipalCollection(principal, getName());
        clearCachedAuthorizationInfo(principals);
    }
 
    /**
     * 清除所有用户授权信息缓存.
     */
    public void clearAllCachedAuthorizationInfo() {
        Cache<Object, AuthorizationInfo> cache = getAuthorizationCache();
        if (cache != null) {
            for (Object key : cache.keys()) {
                cache.remove(key);
            }
        }
    }
}

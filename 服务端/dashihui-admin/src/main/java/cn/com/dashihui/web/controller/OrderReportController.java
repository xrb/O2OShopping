package cn.com.dashihui.web.controller;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.plugin.activerecord.Record;

import cn.com.dashihui.kit.MapKit;
import cn.com.dashihui.web.base.BaseController;
import cn.com.dashihui.web.service.OrderReportService;

public class OrderReportController extends BaseController{
	private OrderReportService service = new OrderReportService();
	
	/**
	 * 跳转至自营商品订单统计页面
	 */
	public void index(){
		render("index.jsp");
	}
    
	/**
	 * 统计店铺商品订单各订单状态数<br/>
	 * 首页店铺商品订单状态统计用
	 */
	public void orderStateCount(){
		renderSuccess(service.queryOrderStatesCount());
	}
	
	/**
	 * 查询近一周内日店铺商品订单量及日成交订单量<br/>
	 * 首页图表用
	 */
	public void orderStateChart(){
		List<String> labels = new ArrayList<String>();
		List<String> datas1 = new ArrayList<String>();
		//所有订单
		List<Record> orderList1 = service.queryOrderOfSevenDay();
		for(Record item : orderList1){
			labels.add(item.getStr("date"));
			datas1.add(String.valueOf(item.getLong("total")));
		}
		List<String> datas2 = new ArrayList<String>();
		//成功的订单
		List<Record> orderList2 = service.queryFinishOrderOfSevenDay();
		for(Record item : orderList2){
			datas2.add(String.valueOf(item.getLong("total")));
		}
		//返回数据
		renderSuccess(MapKit.newInstance().put("labels", labels).put("datas1", datas1).put("datas2", datas2).getAttrs());
	}
}

package cn.com.dashihui.web.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

import cn.com.dashihui.web.dao.Community;
import cn.com.dashihui.web.dao.Store;

public class CommunityService {
	/**
	 * 分页查找社区信息
	 */
	public Page<Record> findByPage(int pageNum,int pageSize,int province,int city,int area,String title){
		StringBuffer sBuffer = new StringBuffer(" FROM (SELECT * FROM t_dict_community T WHERE 1=1");
		List<Object> params = new ArrayList<Object>();
		if(area!=0){
			sBuffer.append(" AND T.province=? AND T.city=? AND T.area=?");
			params.add(province);params.add(city);params.add(area);
		}else if(city!=0){
			sBuffer.append(" AND T.province=? AND T.city=?");
			params.add(province);params.add(city);
		}else if(province!=0){
			sBuffer.append(" AND T.province=?");
			params.add(province);
		}
		if(StrKit.notBlank(title)){
			sBuffer.append(" AND T.title LIKE ?");
			params.add("%"+title+"%");
		}
		sBuffer.append(" ) A");
		sBuffer.append(" LEFT JOIN t_dict_store B ON A.storeid=B.id");
		sBuffer.append(" ORDER BY A.createDate DESC");
		return Db.paginate(pageNum, pageSize, "SELECT A.id,A.province,A.city,A.area, A.title,A.address,A.createDate,B.title as storeTitle", sBuffer.toString(), params.toArray());
	}
	
	public boolean sortCommunity(Map<String,String> sortMap){
		int batchSize = sortMap.size();
		List<String> sqlList = new ArrayList<String>();
		for(String id : sortMap.keySet()){
			sqlList.add("UPDATE t_dict_community SET orderNo="+sortMap.get(id)+" WHERE id="+id);
		}
		int[] result = Db.batch(sqlList,batchSize);
		return result.length>0;
	}
	
	public boolean addCommunity(Community newObject){
		return newObject.save();
	}
	
	public boolean delCommunity(int id){
		return Community.me().deleteById(id);
	}
	
	public boolean editCommunity(Community object){
		return object.update();
	}
	
	public boolean setStore(int storeid,int communityid){
		return Db.update("UPDATE t_dict_community SET storeid=? WHERE id=?", storeid, communityid)==1;
	}
	
	public Community findById(int id){
		return Community.me().findFirst("SELECT comm.*,p.name provinceName,c.name cityName,a.name areaName,s.title storeTitle FROM t_dict_community comm LEFT JOIN t_dict_city p ON comm.province=p.id LEFT JOIN t_dict_city c ON c.id=comm.city LEFT JOIN t_dict_city a ON a.id=comm.area LEFT JOIN t_dict_store s ON comm.storeid=s.id WHERE comm.id=?",id);
	}
	
	public Community findByCode(String code){
		return Community.me().findFirst("SELECT * FROM t_dict_community WHERE code=?",code);
	}
	
	public Community findByStore(int storeid){
		return Community.me().findFirst("SELECT * FROM t_dict_community WHERE storeid=?",storeid);
	}
	
	public List<Store> findStoreByCity(int province,int city,int area){
		return Store.me().find("SELECT * FROM t_dict_store WHERE province=? AND city=? AND area=?",province,city,area);
	}
}

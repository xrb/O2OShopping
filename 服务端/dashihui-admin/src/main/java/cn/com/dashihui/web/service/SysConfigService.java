package cn.com.dashihui.web.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

import cn.com.dashihui.web.dao.SysConfig;

public class SysConfigService {
	
	public boolean sortConfig(Map<String,String> sortMap){
		int batchSize = sortMap.size();
		List<String> sqlList = new ArrayList<String>();
		for(String id : sortMap.keySet()){
			sqlList.add("UPDATE t_sys_config SET orderNo="+sortMap.get(id)+" WHERE id="+id);
		}
		int[] result = Db.batch(sqlList,batchSize);
		return result.length>0;
	}

	public Page<Record> findByPage(int pageNum, int pageSize){
		StringBuffer sqlExcept = new StringBuffer("FROM t_sys_config");
		sqlExcept.append(" ORDER BY orderNo");
		return Db.paginate(pageNum, pageSize, "SELECT * ", sqlExcept.toString());
	}
	
	public boolean addConfig(SysConfig newObject){
		return newObject.save();
	}
	
	public boolean delConfig(int id){
		return SysConfig.me().deleteById(id);
	}
	
	public boolean editConfig(SysConfig object){
		return object.update();
	}
	
	public SysConfig findById(int id){
		return SysConfig.me().findFirst("SELECT * FROM t_sys_config WHERE id=?",id);
	}
	
	public SysConfig findByCode(String code){
		return SysConfig.me().findFirst("SELECT * FROM t_sys_config WHERE code=?",code);
	}
	
	public List<SysConfig> findAllConfig(){
		return SysConfig.me().find("SELECT * FROM t_sys_config c ORDER BY c.orderNo");
	}
}

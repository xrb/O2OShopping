<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript">
<!--
$(function(){
	//初始化表单验证
	$("#addForm").validate({
		rules:{
			passwordConfirm: {
				equalTo: "#passwordOld"
			},
			phonenumber: {
				isMobile:true
			}
		},
		messages:{
			username: {required: "请输入用户名"},
			password: {required: "请输入密码"},
			passwordConfirm: {required: "请再次输入密码",equalTo: "两次密码输入不相同"},
			trueName: {required: "请输入真实姓名"},
			phonenumber: {required: "请输入手机号码",isMobile: "请输入正确的手机号码"}
		},
		submitHandler:function(form){
			$(form).ajaxSubmit({
				success:function(data){
					switch(data.flag){
					case 3:
						Kit.alert("用户名已经存在");return;
					case 9:
						Kit.alert("请选择归属店铺");return;
					case -1:
						Kit.alert("系统异常，请重试");return;
					case 0:
						dataPaginator.loadPage(1);
						addDialog.close();
					}
				}
			});
		}
	});
});
//-->
</script>
<form id="addForm" action="${BASE_PATH}/auth/admin/doAdd" method="post" class="form-horizontal">
	<div class="form-group">
	    <label class="col-lg-2 control-label">用户名</label>
	    <div class="col-lg-9">
        	<input type="text" name="username" value="" class="form-control" placeholder="请输入用户名" required maxlength="20">
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">密码</label>
	    <div class="col-lg-9">
        	<input type="password" id="passwordOld" name="password" value="" class="form-control" placeholder="请输入密码" required maxlength="50">
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">确认密码</label>
	    <div class="col-lg-9">
        	<input type="password" id="passwordConfirm" name="passwordConfirm" value="" class="form-control" placeholder="请再次输入密码" required maxlength="50">
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">真实姓名</label>
	    <div class="col-lg-9">
        	<input type="text" name="trueName" value="" class="form-control" placeholder="请输入真实姓名" required maxlength="20">
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">手机号码</label>
	    <div class="col-lg-9">
        	<input type="text" name="phonenumber" value="" class="form-control" placeholder="请输入手机号码" required maxlength="11">
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">状态</label>
	    <div class="col-lg-9">
	    	<select name="enabled" class="selectpicker form-control">
				<option value="1">可用</option>
				<option value="0">禁用</option>
	        </select>
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">备注</label>
	    <div class="col-lg-9">
        	<textarea name="remark" class="form-control" maxlength="20" rows="3"></textarea>
	    </div>
	</div>
	<div class="row">
		<div class="col-lg-6"><button class="btn btn-primary col-sm-3 pull-right" type="submit" autocomplete="off">提交</button></div>
		<div class="col-lg-6"><button class="btn btn-default col-sm-3 pull-left" type="button" onclick="javascript:addDialog.close();" autocomplete="off">取消</button></div>
	</div>
</form>
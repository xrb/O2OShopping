<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript">

$(function(){
	//初始化表单验证
	$("#addForm").validate({
		messages:{
			categoryNum: {required: "请输入分类编号"},
			categoryName: {required: "请输入分类名称"}
		},
		submitHandler:function(form){
			$(form).ajaxSubmit({
				success:function(data){
					switch(data.flag){
					case 1:
						Kit.alert("名称不能为空");return;
					case 2:
						Kit.alert("编码不能为空");return;
					case -1:
						Kit.alert("系统异常，请重试");return;
					case 0:
						onCategoryAdded($("input[name='categoryFatherId']").val(),data.object);
						addDialog.close();
					}
				}
			});
		}
	});
});

</script>
<form id="addForm" action="${BASE_PATH}/dict/category/doAdd" method="post" class="form-horizontal">
	<div class="form-group">
	    <label class="col-lg-2 control-label">分类编码</label>
	    <div class="col-lg-9">
        	<input type="text" name="categoryNum" value="" class="form-control" placeholder="请输入分类编码" required maxlength="9">
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">分类名称</label>
	    <div class="col-lg-9">
        	<input type="text" name="categoryName" value="" class="form-control" placeholder="请输入分类名称" required maxlength="50">
	    </div>
	</div>
	
	<c:if test="${parent!=null}">
		<div class="form-group">
		    <label class="col-lg-2 control-label">父分类</label>
		    <div class="col-lg-9">
		    	<input type="text" class="form-control" value="${parent.categoryName}" disabled="disabled"/>
		    </div>
			<input type="hidden" name="categoryFatherId" value="${parent.categoryId}"/>
		</div>
	</c:if>
	<c:if test="${parent==null}"><input type="hidden" name="categoryFatherId" value="0"/></c:if>
	<div class="form-group">
	    <label class="col-lg-2 control-label">类型</label>
	    <div class="col-lg-9">
	    	<c:choose>
		    	<c:when test="${type==1}"><input type="text" class="form-control" value="一级分类" disabled="disabled"/></c:when>
		    	<c:when test="${type==2}"><input type="text" class="form-control" value="二级分类" disabled="disabled"/></c:when>
		    	<c:when test="${type==3}"><input type="text" class="form-control" value="三级分类" disabled="disabled"/></c:when>
		    	<c:when test="${type==4}"><input type="text" class="form-control" value="四级分类" disabled="disabled"/></c:when>
	    	</c:choose>
	    </div>
	</div>
	<input type="hidden" name="categoryType" value="${type}"/>
	<div class="row">
		<div class="col-lg-6">
			<button class="btn btn-primary col-sm-3 pull-right" type="submit" autocomplete="off">提交</button>
		</div>
		<div class="col-lg-6">
			<button class="btn btn-default col-sm-3 pull-left" type="button" onclick="javascript:addDialog.close();" autocomplete="off">取消</button>
		</div>
	</div>
</form>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<script type="text/javascript">
$(function(){
	//初始化表单验证
	$("#setStoreForm").validate({
		messages:{
			storeid: {required: "请选择要关联的便利店"}
		},
		errorPlacement: function(error, element) {
			//将错误信息添加至指定元素中
			error.appendTo("#setStoreErrorPlace");
		},
		submitHandler:function(form){
			$(form).ajaxSubmit({
				success:function(data){
					switch(data.flag){
					case -1:
						Kit.alert("系统异常，请重试");return;
					case 0:
						dataPaginator.loadPage(1);
						setStoreDialog.close();
					}
				}
			});
		}
	});
});
</script>
<form id="setStoreForm" action="${BASE_PATH}/dict/community/doSetStore" method="post" class="form-horizontal">
	<div class="table-responsive">
		<table class="table table-hover">
			<thead>
				<tr>
					<th>便利店名称</th>
				</tr>
			</thead>
			<tbody>
				<c:if test="${storeList!=null&&fn:length(storeList)!=0}">
				<c:forEach items="${storeList}" var="store" varStatus="status">
				<tr>
					<td><input type="radio" name="storeid" value="${store.id}" <c:if test="${store.checked}">checked</c:if> required/> ${store.title}</td>
				</tr>
				</c:forEach>
				</c:if>
			</tbody>
		</table>
		<p id="setStoreErrorPlace" class="has-error intent1"></p>
	</div>
	<div class="row">
		<div class="col-lg-6"><button class="btn btn-primary col-sm-3 pull-right" type="submit" autocomplete="off" <c:if test="${storeList==null||fn:length(storeList)==0}">disabled</c:if>>提交</button></div>
		<div class="col-lg-6"><button class="btn btn-default col-sm-3 pull-left" type="button" onclick="javascript:setStoreDialog.close();" autocomplete="off">取消</button></div>
	</div>
	<input type="hidden" name="communityid" value="${communityid}">
</form>
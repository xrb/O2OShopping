<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript">
$(function(){
	//初始化表单验证
	$("#addForm").validate({
		rules:{
			passwordConfirm: {
				equalTo: "#passwordOld"
			},
			tel: {isTelOrMobile: true},
			mail: {email: true}
		},
		messages:{
			username: {required: "请输入用户名"},
			password: {required: "请输入密码"},
			passwordConfirm: {required: "请再次输入密码",equalTo: "两次密码输入不相同"},
			title: {required: "请输入店铺名称"},
			tel: {isTelOrMobile: "请输入座机或手机号码"},
			mail: {email: "请输入正确的邮箱地址"},
			address:{required: "请输入店铺具体地址"}
		},
		submitHandler:function(form){
			$(form).ajaxSubmit({
				success:function(data){
					switch(data.flag){
						case 3:
							Kit.alert("用户名已经存在");return;
						case 12:
							Kit.alert("请输入座机或手机号码");return;
						case 13:
							Kit.alert("请输入正确的邮箱地址");return;
						case -1:
							Kit.alert("系统异常，请重试");return;
						case 0:
							dataPaginator.loadPage(1);
							addDialog.close();
					}
				}
			});
		}
	});
});
</script>
<form id="addForm" action="${BASE_PATH}/dict/store/doAdd" method="post" class="form-horizontal" enctype="multipart/form-data">
	<div class="form-group">
	    <label class="col-lg-2 control-label">用户名</label>
	    <div class="col-lg-9">
        	<input type="text" name="username" value="" class="form-control" placeholder="请输入用户名" required maxlength="20">
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">密码</label>
	    <div class="col-lg-9">
        	<input type="password" id="passwordOld" name="password" value="" class="form-control" placeholder="请输入密码" required maxlength="50">
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">确认密码</label>
	    <div class="col-lg-9">
        	<input type="password" id="passwordConfirm" name="passwordConfirm" value="" class="form-control" placeholder="请再次输入密码" required maxlength="50">
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">店铺名称</label>
	    <div class="col-lg-9">
        	<input type="text" name="title" value="" class="form-control" placeholder="请输入店铺名称" required maxlength="20">
	    </div>
	</div>
	<div class="form-group">
		<label class="col-lg-2 control-label">门头照片</label>
		<div class="col-lg-9">
			<input type="file" id="thumb" name="thumb" accept="image/jpg" title="选择文件">
		</div>
		<div class="col-lg-offset-2 p-t-5 col-lg-9"><span class="text-success">建议640px*300px,大小60K以内，支持JPEG、PNG格式</span></div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">门店类型</label>
	    <div class="col-lg-9">
        	<ul class="iCheckList">
	    		<li><input type="radio" class="iCheck" name="type" value="1">&nbsp;总部</li>
	    		<li><input type="radio" class="iCheck" name="type" value="2">&nbsp;总店</li>
	    		<li><input type="radio" class="iCheck" name="type" value="3" checked="checked">&nbsp;分店</li>
	    		<li><input type="radio" class="iCheck" name="type" value="4">&nbsp;配送</li>
	    	</ul>
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">门店属性</label>
	    <div class="col-lg-9">
        	 <ul class="iCheckList">
	    		<li><input type="radio" class="iCheck" name="attribute" value="1" checked="checked">&nbsp;加盟店</li>
	    		<li><input type="radio" class="iCheck" name="attribute" value="2" >&nbsp;直营店</li>
		    </ul>
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">地址</label>
	    <div class="col-lg-9">
    		<select id="province" name="province" class="selectpicker pull-left" data-width="30%" data-url="${BASE_PATH}/api/province" data-isfirst="true" data-next="#city" data-key="id:name" ></select>
			<select id="city" name="city"  class="selectpicker pull-left p-l-5" data-width="35%" data-url="${BASE_PATH}/api/city/{value}" data-next="#area" data-key="id:name" ></select>
			<select id="area" name="area"   class="selectpicker pull-left p-l-5" data-width="35%" data-url="${BASE_PATH}/api/area/{value}"  data-key="id:name" ></select>
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label"></label>
	    <div class="col-lg-9">
       	    <input type="text" name="address" value="" class="form-control" placeholder="请输入详细地址" required maxlength="100">
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">店长</label>
	    <div class="col-lg-9">
        	<input type="text" name="manager" value="" class="form-control" placeholder="请输入店长名称" required maxlength="20">
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">电话</label>
	    <div class="col-lg-9">
        	<input type="text" name="tel" value="" class="form-control" placeholder="请输入电话" maxlength="13">
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">营业从</label>
	    <div class="col-lg-9">
	        <input class="form-control datetimepicker" name="beginTime" size="16" type="text" data-format="hh:ii" readonly="readonly" value="" placeholder="请点击选择营业开始时间">
    	</div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">营业至</label>
       	<div class="col-lg-9">
	        <input class="form-control datetimepicker" name="endTime" size="16" type="text" data-format="hh:ii" readonly="readonly" value="" placeholder="请点击选择营业结束时间">
    	</div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">邮箱</label>
	    <div class="col-lg-9">
        	<input type="text" name="mail" value="" class="form-control" placeholder="请输入邮箱" maxlength="50">
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">配送说明</label>
	    <div class="col-lg-9">
	    	<textarea  name="deliverydes" class="form-control" placeholder="请输入配送说明" maxlength="200"></textarea>
	    </div>
	</div>
	<div class="row">
		<div class="col-lg-6"><button class="btn btn-primary col-sm-3 pull-right" type="submit" autocomplete="off">提交</button></div>
		<div class="col-lg-6"><button class="btn btn-default col-sm-3 pull-left" type="button" onclick="javascript:addDialog.close();" autocomplete="off">取消</button></div>
	</div>
</form>
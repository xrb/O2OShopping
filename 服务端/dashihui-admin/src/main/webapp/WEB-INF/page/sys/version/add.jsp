<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript">
$(function(){
	//初始化表单验证
	$("#addForm").validate({
		rules: {
			versionPart1:{digits:true},
			versionPart2:{digits:true},
			versionPart3:{digits:true},
			downurl:{
	            required:true,       
	            url:false,
	            maxlength:200
	        }  
		},
		messages:{
			type: {required: "请输入类别"},
			versionPart1: {required: ""},
			versionPart2: {required: ""},
			versionPart3: {required: ""},
			downurl: {required: "请输入下载地址",url: "请输入合法的下载地址"}
		},
		submitHandler:function(form){
			$(form).ajaxSubmit({
				success:function(data){
					switch(data.flag){
						case -1:
							Kit.alert("系统异常，请重试");
							return;
						case 0:
							dataPaginator.loadPage(1);
							addDialog.close();
					}
				}
			});
		}
	});
});
</script>
<form id="addForm" action="${BASE_PATH}/sys/version/doAdd" method="post" class="form-horizontal">
	<div class="form-group">
	    <label class="col-lg-2 control-label">平台</label>
	    <div class="col-lg-9">
	    	<select name="type" class="selectpicker form-control">
				<option value="A">安卓</option>
				<option value="B">iPhone</option>
	        </select>
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">版本号</label>
	    <div class="col-lg-9">
	    	<div class="input-group">
	        	<input type="text" name="versionPart1" class="form-control" required maxlength="3" size="3" data-error-place="#versionError">
	        	<span class="input-group-btn"></span>
	        	<input type="text" name="versionPart2" class="form-control" required maxlength="3" size="3" data-error-place="#versionError">
	        	<span class="input-group-btn"></span>
	        	<input type="text" name="versionPart3" class="form-control" required maxlength="5" size="5" data-error-place="#versionError">
	    	</div>
        	<div id="versionError"></div>
	    </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">下载地址</label>
        <div class="col-lg-9 controls textarea">
            <textarea name="downurl" class="form-control" placeholder="请输入下载地址" maxlength="200"></textarea>
        </div>
	</div>
	<div class="form-group">
        <label class="col-lg-2 control-label"></label>
        <div class="col-lg-9 controls">
        	<input name="updateFlag" type="checkbox" value="1"> 强制升级
        </div>
    </div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">升级内容</label>
        <div class="col-lg-9 controls textarea">
        	<textarea name='updatelog' class="form-control" placeholder="请输入升级内容" maxlength="200"></textarea>
        </div>
	</div>
	<div class="form-group">
	    <label class="col-lg-2 control-label">发布时间</label>
       	<div class="col-lg-9" >
	    	<input class="form-control datetimepicker" id="releaseDate" placeholder="请选择发布时间" name="releaseDate" type="text" data-format="yyyy-mm-dd hh:ii" readonly="readonly">
	    	<span class="help-block m-b-none label label-info">*APP对外公布的发布时间，根据该值排序，取日期最近的版本为最新版本</span>
    	</div>
	</div>
	<div class="row">
		<div class="col-lg-6"><button class="btn btn-primary col-sm-3 pull-right" type="submit" autocomplete="off">提交</button></div>
		<div class="col-lg-6"><button class="btn btn-default col-sm-3 pull-left" type="button" onclick="javascript:addDialog.close();" autocomplete="off">取消</button></div>
	</div>
</form>
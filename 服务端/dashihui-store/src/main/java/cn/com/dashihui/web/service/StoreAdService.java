package cn.com.dashihui.web.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.jfinal.plugin.activerecord.Db;

import cn.com.dashihui.web.dao.Category;
import cn.com.dashihui.web.dao.StoreAd;
import cn.com.dashihui.web.dao.Goods;

public class StoreAdService {
	
	public boolean sortAd(Map<String,String> sortMap){
		int batchSize = sortMap.size();
		List<String> sqlList = new ArrayList<String>();
		for(String id : sortMap.keySet()){
			sqlList.add("UPDATE t_bus_store_ad SET orderNo="+sortMap.get(id)+" WHERE id="+id);
		}
		int[] result = Db.batch(sqlList,batchSize);
		return result.length>0;
	}
	
	public List<StoreAd> findAll(int storeid){
		return StoreAd.me().find("SELECT sa.*,sg.name goodsName FROM t_bus_store_ad sa LEFT JOIN t_bus_goods sg ON sa.goodsid=sg.id WHERE sa.storeid=? ORDER BY sa.orderNo,sa.createDate DESC",storeid);
	}
	
	public boolean addAd(StoreAd newObject){
		return newObject.save();
	}
	
	public boolean delAd(int id){
		return StoreAd.me().deleteById(id);
	}
	
	public boolean editAd(StoreAd object){
		return object.update();
	}
	
	public StoreAd findById(int id){
		return StoreAd.me().findFirst("SELECT sa.*,sg.name goodsName FROM t_bus_store_ad sa LEFT JOIN t_bus_goods sg ON sa.goodsid=sg.id WHERE sa.id=?",id);
	}
	
	public List<Category> findAllCategoryWithGoods(int storeid){
		List<Goods> goodsList = Goods.me().find("SELECT sg.*,g.categoryonid,g.categorytwid,g.categorythid,g.categoryfoid FROM t_bus_goods sg INNER JOIN t_bus_goods g ON sg.goodsid=g.id WHERE sg.storeid=? ORDER BY sg.createDate",storeid);
		List<Category> categoryList = Category.me().find("SELECT c.categoryId id,c.categoryName name,c.categoryFatherId parentid,c.categoryType type FROM t_dict_category c ORDER BY c.categoryFatherId,c.categoryNo");
		//将查询出来的分类列表，整理成嵌套形式
		if(categoryList!=null){
			List<Category> categoryType1List = new ArrayList<Category>(), categoryType2List = new ArrayList<Category>(), categoryType3List = new ArrayList<Category>(), categoryType4List = new ArrayList<Category>();
			for(Category c : categoryList){
				if(c.getInt("type")==1){
					categoryType1List.add(c);
				}else if(c.getInt("type")==2){
					categoryType2List.add(c);
				}else if(c.getInt("type")==3){
					categoryType3List.add(c);
				}else if(c.getInt("type")==4){
					//当是县/区时，从所有社区记录中匹配其下属数据
					if(goodsList!=null){
						for(Goods goods : goodsList){
							if(c.getInt("id").intValue()==goods.getInt("categoryfoid").intValue()){
								c.addGoods(goods);
							}
						}
					}
					categoryType4List.add(c);
				}
			}
			if(categoryType4List.size()!=0){
				for(Category r4 : categoryType4List){
					for(Category r3 : categoryType3List){
						if(r4.getInt("parentid").intValue()==r3.getInt("id").intValue()){
							r3.addChild(r4);
							break;
						}
					}
				}
			}
			if(categoryType3List.size()!=0){
				for(Category r3 : categoryType3List){
					for(Category r2 : categoryType2List){
						if(r3.getInt("parentid").intValue()==r2.getInt("id").intValue()){
							r2.addChild(r3);
							break;
						}
					}
				}
			}
			if(categoryType2List.size()!=0){
				for(Category r2 : categoryType2List){
					for(Category r1 : categoryType1List){
						if(r2.getInt("parentid").intValue()==r1.getInt("id").intValue()){
							r1.addChild(r2);
							break;
						}
					}
				}
			}
			return categoryType1List;
		}
		return null;
	}
}

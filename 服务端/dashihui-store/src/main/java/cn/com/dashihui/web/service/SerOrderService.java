package cn.com.dashihui.web.service;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

import cn.com.dashihui.web.common.SerOrderCode;
import cn.com.dashihui.web.dao.SerOrder;
import cn.com.dashihui.web.dao.SerShop;

/**
 * 新版服务订单管理<br/>
 * 查询的是t_bus_ser_order表
 */
public class SerOrderService {

	/**
	 * 分页查找订单信息
	 * @param orderNum 订单编号
	 * @param beginDate 下单时间
	 * @param endDate 成交时间
	 * @param state 订单状态，0：全部， 1：待付款， 2：待接单， 3：待派单， 4：待商家接单， 5：待用户确认完成， 6：已确认， 7：无效
	 * @param address 买家地址
	 * @param tel 买家电话
	 * @param payType 支付方式
	 */
	public Page<Record> findByPage(int pageNum,int pageSize,int storeid,String orderNum,String beginDate,String endDate,int state,String address,String tel,int payType){
		StringBuffer sBuffer = new StringBuffer();
		List<Object> params = new ArrayList<Object>();
		sBuffer.append("FROM t_bus_ser_order A WHERE A.storeid=?");
		params.add(storeid);
		if(!StrKit.isBlank(orderNum)){
			sBuffer.append(" AND A.orderNum=?");
			params.add(orderNum);
		}
		if(!StrKit.isBlank(beginDate)&&!StrKit.isBlank(endDate)){
			sBuffer.append(" AND A.startDate BETWEEN ? AND ?");
			params.add(beginDate);
			params.add(endDate);
		}else if(!StrKit.isBlank(beginDate)&&StrKit.isBlank(endDate)){
			sBuffer.append(" AND DATE_FORMAT(A.startDate,'%Y-%m-%d')>=?");
			params.add(beginDate);
		}else if(StrKit.isBlank(beginDate)&&!StrKit.isBlank(endDate)){
			sBuffer.append(" AND DATE_FORMAT(A.startDate,'%Y-%m-%d')<=?");
			params.add(endDate);
		}
		switch (state) {
			case 1://待付款（“在线支付”、“未支付”、“正常”）
				sBuffer.append(" AND payType=? AND payState=? AND orderState=?");
				params.add(SerOrderCode.OrderPayType.ON_LINE);
				params.add(SerOrderCode.OrderPayState.NO_PAY);
				params.add(SerOrderCode.OrderState.NORMAL);
				break;
			case 2://待接单（1：“在线支付”、“已支付”、“店铺未接单”、“正常”，2：“服务后付款”、“店铺未接单”、“正常”）
				sBuffer.append(" AND ((payType=? AND payState=?) OR payType=?) AND deliverState=? AND orderState=?");
				params.add(SerOrderCode.OrderPayType.ON_LINE);
				params.add(SerOrderCode.OrderPayState.HAD_PAY);
				params.add(SerOrderCode.OrderPayType.AFTER_SERVICE);
				params.add(SerOrderCode.OrderDispatchState.STORE_NO_ACCEPT);
				params.add(SerOrderCode.OrderState.NORMAL);
				break;
			case 3://待派单/已拒单（1：“在线支付”、“已支付”、“店铺已接单”、“正常”或“拒单”，2：“服务后付款”、“店铺已接单”、“正常”或“拒单”）
				sBuffer.append(" AND ((payType=? AND payState=?) OR payType=?) AND deliverState=? AND (orderState=? OR orderState=?)");
				params.add(SerOrderCode.OrderPayType.ON_LINE);
				params.add(SerOrderCode.OrderPayState.HAD_PAY);
				params.add(SerOrderCode.OrderPayType.AFTER_SERVICE);
				params.add(SerOrderCode.OrderDispatchState.STORE_HAD_ACCEPT);
				params.add(SerOrderCode.OrderState.NORMAL);
				params.add(SerOrderCode.OrderState.REFUSE);
				break;
			case 4://待商家接单（1：“在线支付”、“已支付”、“店铺已派单”、“正常”，2：“服务后付款”、“店铺已派单”、“正常”）
				sBuffer.append(" AND ((payType=? AND payState=?) OR payType=?) AND deliverState=? AND orderState=?");
				params.add(SerOrderCode.OrderPayType.ON_LINE);
				params.add(SerOrderCode.OrderPayState.HAD_PAY);
				params.add(SerOrderCode.OrderPayType.AFTER_SERVICE);
				params.add(SerOrderCode.OrderDispatchState.STORE_HAD_DISPATCH);
				params.add(SerOrderCode.OrderState.NORMAL);
				break;
			case 5://待用户确认完成（1：“在线支付”、“已支付”、“店铺已接单”、“正常”，2：“服务后付款”、“店铺已接单”、“正常”）
				sBuffer.append(" AND ((payType=? AND payState=?) OR payType=?) AND deliverState=? AND orderState=?");
				params.add(SerOrderCode.OrderPayType.ON_LINE);
				params.add(SerOrderCode.OrderPayState.HAD_PAY);
				params.add(SerOrderCode.OrderPayType.AFTER_SERVICE);
				params.add(SerOrderCode.OrderDispatchState.PROSER_HAD_ACCEPT);
				params.add(SerOrderCode.OrderState.NORMAL);
				break;
			case 6://已确认（1：“在线支付”、“已支付”、“店铺已接单”、“已确认”，2：“服务后付款”、“店铺已接单”、“已确认”）
				sBuffer.append(" AND ((payType=? AND payState=?) OR payType=?) AND deliverState=? AND orderState=?");
				params.add(SerOrderCode.OrderPayType.ON_LINE);
				params.add(SerOrderCode.OrderPayState.HAD_PAY);
				params.add(SerOrderCode.OrderPayType.AFTER_SERVICE);
				params.add(SerOrderCode.OrderDispatchState.PROSER_HAD_ACCEPT);
				params.add(SerOrderCode.OrderState.FINISH);
				break;
			case 7://无效（“无效”）
				sBuffer.append(" AND orderState=?");
				params.add(SerOrderCode.OrderState.WASTE);
				break;
			default:
				//默认不查询取消、过期、删除的订单
				sBuffer.append(" AND orderState!=? AND orderState!=? AND orderState!=?");
				params.add(SerOrderCode.OrderState.CANCEL);
				params.add(SerOrderCode.OrderState.EXPIRE);
				params.add(SerOrderCode.OrderState.DELETE);
				break;
		}
		if(!StrKit.isBlank(address)){
			sBuffer.append(" AND address LIKE ?");
			params.add("%"+address+"%");
		}
		if(!StrKit.isBlank(tel)){
			sBuffer.append(" AND tel=?");
			params.add(tel);
		}
		if(payType!=0){
			sBuffer.append(" AND A.payType=?");
			params.add(payType);
		}
		sBuffer.append(" ORDER BY createDate DESC");
		//查询出符合条件的订单列表
		return Db.paginate(pageNum,pageSize,"SELECT * ",sBuffer.toString(),params.toArray());
	}

	/**
	 * 根据订单号查询出订单
	 */
	public SerOrder getOrderByOrderNum(String orderNum){
		String sql = "SELECT * FROM t_bus_ser_order WHERE orderNum=?";
		return SerOrder.me().findFirst(sql,orderNum);
	}

	/**
	 * 根据订单号查询订单操作日志
	 */
	public List<Record> getLogListByOrderNum(String orderNum){
		String sql = "SELECT * FROM t_bus_ser_order_log WHERE orderNum=? ORDER BY createDate";
		return Db.find(sql, orderNum);
	}

	/**
	 * 更新
	 */
	public boolean update(SerOrder order){
		return order.update();
	}
	
	/**
	 * 记录操作日志
	 */
	public void log(String orderNum, String user, String action, String content){
		Db.update("INSERT INTO t_bus_ser_order_log(orderNum,user,action,content) VALUES(?,?,?,?)",orderNum,user,action,content);
	}
	
	/**
	 * 获取当前店铺下的所有服务商家，供派单时选择
	 */
	public List<SerShop> getShopList(int storeid,String orderNum){
		return SerShop.me().find(" SELECT A.*,(CASE WHEN B.proSerid IS NOT NULL THEN 1 ELSE 0 END) checked FROM t_bus_ser_shop A LEFT JOIN "
								+ " (SELECT proSerid FROM t_bus_ser_order WHERE orderNum=? AND storeid=?) B "
								+ " ON A.id=B.proSerid WHERE A.storeid=? and A.enabled=1",orderNum,storeid,storeid);
	}
	
	/**
	 * 获取指定服务商家所登录的所有客户端的clientid<br/>
	 * 同一个商家帐号可以同时登录多个终端，所以会有多个clientid
	 */
	public List<String> getClientId(int shopid){
		return Db.query("SELECT clientid FROM t_bus_ser_shop_push WHERE shopid=?",shopid);
	}
}

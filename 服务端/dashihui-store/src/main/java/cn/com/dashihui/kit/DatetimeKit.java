package cn.com.dashihui.kit;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.jfinal.kit.StrKit;

public class DatetimeKit {
	/**
	 * 日期格式化
	 */
	public static String getFormatDate(String format){
		if(StrKit.isBlank(format)){
			format="yyyy-MM-dd HH:mm:ss";
		}
		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat(format);
		return dateFormat.format(date);
	}
	/**
	 * 日期格式化
	 */
	public static String getFormatDate(Date date,String format){
		if(StrKit.isBlank(format)){
			format="yyyy-MM-dd HH:mm:ss";
		}
		SimpleDateFormat dateFormat = new SimpleDateFormat(format);
		return dateFormat.format(date);
	}
	/**
	* 字符串转换成日期
	* @param str 格式为yyyy-MM-dd HH:mm:ss   
	* @return date
	*/
	public static Date StrToDate(String str) {
	  
	   SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	   Date date = null;
		try {
			date = format.parse(str);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	   return date;
	}
	/**
	 * 获得间隔后的格式化后的时间
	 * @param intervalTime 时间间隔 单位秒
	 * @param format 时间格式 （yyyy-MM-dd HH:mm:ss）
	 */
	public static String getIntervalFormatDate(int intervalTime,String format){
		if(StrKit.isBlank(format)){
			format="yyyy-MM-dd HH:mm:ss";
		}
		long currentTime = System.currentTimeMillis() + intervalTime;
		Date date = new Date(currentTime);
		DateFormat df = new SimpleDateFormat(format);
		String nowTime=df.format(date);
		return nowTime;
	}
	/**
	 * 比较与当前时间大小
	 * @param data 
	 * @return 1 传入时间大于当前时间 ，-1 传入时间小于当前时间，0：传入时间等于当前时间
	 */
	public static int compareDate(Date data){
        if (data.getTime() > new Date().getTime()) {
            return 1;
        } else if (data.getTime() < new Date().getTime()) {
            return -1;
        } else {//相等
            return 0;
        }
	}
	
	/**
	 * 获取时间戳
	 * @return
	 */
	public static String getTimestamp() {
		return Long.toString(new Date().getTime() / 1000);
	}
}

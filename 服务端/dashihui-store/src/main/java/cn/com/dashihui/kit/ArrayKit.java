package cn.com.dashihui.kit;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("rawtypes")
public class ArrayKit<T> {
	
	private List<T> list = new ArrayList<T>();
	
	private ArrayKit() {
	}
	
	public static ArrayKit newInstance(){
		return new ArrayKit();
	}
	
	public ArrayKit add(T item){
		this.list.add(item);
		return this;
	}
	
	public List<T> getList(){
		return this.list;
	}
}

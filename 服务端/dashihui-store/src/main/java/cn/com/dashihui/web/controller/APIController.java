package cn.com.dashihui.web.controller;

import com.jfinal.kit.JsonKit;
import com.jfinal.kit.PropKit;
import com.jfinal.kit.StrKit;

import cn.com.dashihui.web.base.BaseController;
import cn.com.dashihui.web.dao.Store;
import cn.com.dashihui.web.service.APIService;

public class APIController extends BaseController{
	private APIService service = new APIService();
	
	public void brand(){
		renderText(JsonKit.toJson(service.getAllBrand()));
	}
	
	public void province(){
		renderText(JsonKit.toJson(service.getAllProvince()));
	}
	
	public void city(){
		int parentid = getParaToInt(0);
		renderText(JsonKit.toJson(service.getAllCityByProvince(parentid)));
	}
	
	public void area(){
		int parentid = getParaToInt(0);
		renderText(JsonKit.toJson(service.getAllAreaByCity(parentid)));
	}
	
	public void category(){
		String parentidStr = getPara(0);
		int parentid = StrKit.isBlank(parentidStr)?0:Integer.valueOf(parentidStr);
		renderText(JsonKit.toJson(service.getCategoryByParent(parentid)));
	}
	
	public void storePage(){
		int p = getParaToInt("p");
		int c = getParaToInt("c");
		int a = getParaToInt("a");
		String keyword = getPara("k");
		int pageNum = getParaToInt(0, 1);
		int pageSize = getParaToInt("pageSize", PropKit.getInt("constants.pageSize"));
		renderSuccess(service.getStorePage(p, c, a, keyword, pageNum, pageSize));
	}
	
	public void goodsPage(){
		int storeid = getStoreid();
		int c1 = getParaToInt("c1");
		int c2 = getParaToInt("c2");
		int c3 = getParaToInt("c3");
		int c4 = getParaToInt("c4");
		String keyword = getPara("k");
		int pageNum = getParaToInt(0, 1);
		int pageSize = getParaToInt("pageSize", PropKit.getInt("constants.pageSize"));
		renderSuccess(service.getGoodsPage(storeid, c1, c2, c3, c4, keyword, pageNum, pageSize));
	}
	
	public void sellerPage(){
		int storeid = getStoreid();
		String phone = getPara("p");
		String keyword = getPara("k");
		int pageNum = getParaToInt(0, 1);
		int pageSize = getParaToInt("pageSize", PropKit.getInt("constants.pageSize"));
		renderSuccess(service.getSellerPage(storeid, phone, keyword, pageNum, pageSize));
	}
	
	public void build(){
		Store store = getCurrentUser();
		if(store!=null){
			renderText(JsonKit.toJson(service.getAllBuildByStore(store.getInt("id"))));
		}
	}
	
	public void unit(){
		renderText(JsonKit.toJson(service.getAllUnitByParent(getParaToInt(0))));
	}
	
	public void room(){
		renderText(JsonKit.toJson(service.getAllRoomByParent(getParaToInt(0))));
	}
	
	public void goodsTag(){
		renderText(JsonKit.toJson(service.getAllGoodsTag(getStoreid())));
	}
}

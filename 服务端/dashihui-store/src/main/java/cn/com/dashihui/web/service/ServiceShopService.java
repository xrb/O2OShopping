package cn.com.dashihui.web.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

import cn.com.dashihui.web.dao.ServiceCategory;
import cn.com.dashihui.web.dao.ServiceShop;
import cn.com.dashihui.web.dao.ServiceShopImages;
import cn.com.dashihui.web.dao.ServiceShopItem;

public class ServiceShopService {
	
	/**
	 * 查找出所有服务分类
	 */
	public List<ServiceCategory> findCategoryList(int storeid,int shopid){
		return ServiceCategory.me().find("SELECT A.*,(CASE WHEN B.categorycode IS NULL THEN 0 ELSE 1 END) checked FROM t_bus_service_category A"
				+ " LEFT JOIN t_bus_service_category_rel B ON A.code = B.categorycode AND B.shopid=?"
				+ " WHERE A.enabled=1 AND A.storeid=?"
				+ " ORDER BY orderNo ASC",shopid,storeid);
	}
	
	/**
	 * 翻页查找出所有的商家根据商铺 (名称和上下架状态)
	 */
	public Page<Record> findByPage(int pageNum, int pageSize, int storeid,String name, int state){
		StringBuffer sBuffer = new StringBuffer(" From t_bus_service_shop A WHERE 1=1");
		List<Object> params = new ArrayList<Object>();
		if(StrKit.notBlank(name)){
			sBuffer.append(" AND A.name LIKE ?");
			params.add("%"+name+"%");
		}
		if(state!=0){
			sBuffer.append(" AND A.state=?");
			params.add(state);
		}
		sBuffer.append(" ORDER BY A.createDate DESC");
		return Db.paginate(pageNum, pageSize, "SELECT A.*", sBuffer.toString(), params.toArray());
	}
	
	/**
	 * 根据商家ID查找出商铺的详情
	 */
	public ServiceShop findDetailById(int id){
		return ServiceShop.me().findFirst("SELECT id,`describe` FROM t_bus_service_shop WHERE id=?",id);
	}
	
	/**
	 * 保存商家
	 */
	public boolean addShop(ServiceShop newObject){
		return newObject.save();
	}
	
	/**
	 * 修改商家
	 */
	public boolean editShop(ServiceShop object){
		return object.update();
	}
	
	/**
	 * 删除商家
	 */
	public boolean delShop(int id){
		return ServiceShop.me().deleteById(id);
	}
	
	/**
	 * 删除商家与服务分类之间的关联
	 */
	public void delCaShopRel(int shopid){
		 Db.update("DELETE FROM t_bus_service_category_rel WHERE shopid=?", shopid);
	}
	
	/**
	 * 根据商家ID查询商铺以及商铺归属的店铺
	 */
	public ServiceShop findById(int id){
		return ServiceShop.me().findFirst("SELECT A.*,B.title as storeName,B.province,B.city,B.area FROM t_bus_service_shop A INNER JOIN t_dict_store B ON A.storeid = B.id  WHERE A.id=?",id);
	}
	
	/**
	 * 添加商户服务分类
	 */
	public void addCaToShop(String[] categorycode,int shopid){
		List<String> sqlList = new ArrayList<String>();
		sqlList.add("DELETE FROM t_bus_service_category_rel WHERE shopid="+shopid);
		for(int i=0;i<categorycode.length;i++){
			sqlList.add("INSERT INTO t_bus_service_category_rel(categorycode,shopid) values('"+categorycode[i]+"',"+shopid+")");
		}
		Db.batch(sqlList, sqlList.size());
	}
	
	/**
	 * 通过商家ID查找出商铺归属的服务分类(比如一个商铺归属于丽人和家政)
	 */
	public List<Record> findRelByshopId(int shopid){
		return Db.find("SELECT * FROM t_bus_service_category_rel WHERE shopid=?", shopid);
	}
	
	/**
	 * 图片排序
	 */
	public boolean sortImages(Map<String,String> sortMap){
		int batchSize = sortMap.size();
		List<String> sqlList = new ArrayList<String>();
		for(String id : sortMap.keySet()){
			sqlList.add("UPDATE t_bus_service_shop_images SET orderNo="+sortMap.get(id)+" WHERE id="+id);
		}
		int[] result = Db.batch(sqlList,batchSize);
		return result.length>0;
	}
	
	/**
	 * 根据商家ID查找出所有的图片
	 */
	public List<ServiceShopImages> findAllImages(int shopid){
		return ServiceShopImages.me().find("SELECT A.* FROM t_bus_service_shop_images A WHERE A.shopid=? ORDER BY A.orderNo",shopid);
	}
	
	/**
	 * 根据ID查找出图片
	 */
	public ServiceShopImages findImagesById(int id){
		return ServiceShopImages.me().findFirst("SELECT A.* FROM t_bus_service_shop_images A WHERE A.id=?",id);
	}
	
	/**
	 * 添加图片
	 */
	public boolean addImage(ServiceShopImages newObject){
		return newObject.save();
	}
	
	/**
	 * 删除图片
	 */
	public boolean delImage(int id){
		return ServiceShopImages.me().deleteById(id);
	}
	
	/**
	 * 根据商家ID查找出所有规格
	 */
	public List<ServiceShopItem> findAllItem(int shopid){
		return ServiceShopItem.me().find("SELECT A.* FROM t_bus_service_shop_item A WHERE A.shopid=? ORDER BY A.createDate DESC",shopid);
	}
	
	/**
	 * 根据ID查找出规格
	 */
	public ServiceShopItem findItemById(int id){
		return ServiceShopItem.me().findFirst("SELECT A.* FROM t_bus_service_shop_item A WHERE A.id=? ORDER BY A.createDate DESC",id);
	}
	
	/**
	 * 添加规格
	 */
	public boolean addItem(ServiceShopItem newObject){
		return newObject.save();
	}
	
	/**
	 * 修改规格
	 */
	public boolean updateItem(ServiceShopItem newObject){
		return newObject.update();
	}
	
	/**
	 * 删除规格
	 */
	public boolean delItem(int id){
		return ServiceShopItem.me().deleteById(id);
	}
	/**
	 * 根据shopid删除规格
	 */
	public void delItemByShopid(int shopid){
		Db.update("DELETE FROM t_bus_service_shop_item WHERE shopid=?",shopid);
	}
}

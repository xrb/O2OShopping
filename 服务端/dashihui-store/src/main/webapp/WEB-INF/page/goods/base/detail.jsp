<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<ul class="nav nav-tabs" role="tablist">
	<li role="presentation" class="active"><a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">商品信息</a></li>
	<li role="presentation"><a href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab">图片集</a></li>
</ul>
<div class="tab-content">
	<div role="tabpanel" class="tab-pane active container-fluid" id="tab1" style="padding-top:15px;min-height:650px;">
		<div class="row">
			<div class="col-lg-2 text-right">
			    <label>小图：</label>
			</div>
			<div class="col-lg-10">
			    <img src="${FTP_PATH}${object.thumb}" width="50%"/>
			</div>
		</div>
		<hr class="hr-line"/>
		<div class="row">
			<div class="col-lg-2 text-right">
			    <label>标题：</label>
			</div>
			<div class="col-lg-10">${object.name}</div>
		</div>
		<hr class="hr-line"/>
		<div class="row">
			<div class="col-lg-2 text-right">
			    <label>分类：</label>
			</div>
			<div class="col-lg-10">${object.categoryonName}-${object.categorytwName}-${object.categorythName}-${object.categoryfoName}</div>
		</div>
		<hr class="hr-line"/>
		<div class="row">
			<div class="col-lg-2 text-right">
			    <label>品牌：</label>
			</div>
			<div class="col-lg-10">${object.brandName}</div>
		</div>
		<hr class="hr-line"/>
		<div class="row">
			<div class="col-lg-2 text-right">
			    <label>规格：</label>
			</div>
			<div class="col-lg-10">${object.spec}</div>
		</div>
		<hr class="hr-line"/>
		<div class="row">
			<div class="col-lg-2 text-right">
			    <label>描述：</label>
			</div>
			<div class="col-lg-10">${object.shortInfo}</div>
		</div>
		<hr class="hr-line"/>
		<div class="row">
			<div class="col-lg-2 text-right">
			    <label>市场价：</label>
			</div>
			<div class="col-lg-10">${object.marketPrice} 元</div>
		</div>
		<hr class="hr-line"/>
		<div class="row">
			<div class="col-lg-2 text-right">
			    <label>销售价：</label>
			</div>
			<div class="col-lg-10">${object.sellPrice} 元</div>
		</div>
		<hr class="hr-line"/>
	</div>
	<div role="tabpanel" class="tab-pane container-fluid" id="tab2" style="padding-top:15px;min-height:650px;">
		<c:if test="${object.imageList!=null&&fn:length(object.imageList)!=0}">
		<div class="row">
			<c:forEach items="${object.imageList}" var="image">
			<div class="col-lg-4 col-md-4 col-sm-4">
				<div class="thumbnail">
					<a href="#"><img src="${FTP_PATH}${image.thumb}"></a>
				</div>
			</div>
			</c:forEach>
		</div>
		</c:if>
	</div>
</div>
<div class="row">
	<div class="col-lg-6"><button class="btn btn-primary col-sm-3 pull-right" type="button" onclick="javascript:doCopy(${object.id},2)" autocomplete="off">复制</button></div>
	<div class="col-lg-6"><button class="btn btn-default col-sm-3 pull-left"  type="button" onclick="javascript:detailDialog.close();" autocomplete="off">关闭</button></div>
</div>
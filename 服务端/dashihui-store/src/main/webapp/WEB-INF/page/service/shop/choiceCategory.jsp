<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<script type="text/javascript">
$("button[name='caButton']").click(function(){
	var shopid = $("input[name='cashopid']").val();
	var categorycode = [];
	var categoryCheck = $("input[name^='category'][type='checkbox']:checked");
	for(var i=0;i<categoryCheck.length;i++){
		var category = categoryCheck[i];
		categorycode.push($(category).val());
	}
	$.post("${BASE_PATH}/service/shop/addCaToShop",{'categorycode':categorycode.join(","),'shopid':shopid},function(result){
		if(result.flag == 0){
			choiceCategoryDialog.close();
		}else{
			kit.alert("保存失败");
		}
	});
});
</script>
<form class="form-horizontal">
<input type="hidden" name="cashopid" value="${shopid}">
   <div class="table-responsive" style="min-height:300px;">
		<table  class="table table-hover">
			<thead>
				<tr>
					<th>分类名称</th>
				</tr>
			</thead>
			<tbody>
			<c:if test="${cList!=null&&fn:length(cList)!=0}">
				<c:forEach items="${cList}" var="category">
					<tr data-id="${category.id}" data-node-id="${category.id}">
						<td><input name="category${category.code}" type="checkbox" class="iCheck" value="${category.code}" <c:if test="${category.checked==1}">checked</c:if>> ${category.name}</td>
					</tr>
	               </c:forEach>
               </c:if>
			</tbody>
		</table>
	</div>
	<div class="row">
		<div class="col-lg-6"><button class="btn btn-primary col-sm-3 pull-right" name="caButton" type="button" autocomplete="off">提交</button></div>
		<div class="col-lg-6"><button class="btn btn-default col-sm-3 pull-left" type="button" onclick="javascript:choiceCategoryDialog.close();" autocomplete="off">取消</button></div>
	</div>
</form>
	
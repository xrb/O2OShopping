package cn.com.dashihui.api.controller;

import com.jfinal.kit.PropKit;
import com.jfinal.kit.StrKit;

import cn.com.dashihui.api.base.BaseController;
import cn.com.dashihui.api.common.CacheHolder;
import cn.com.dashihui.api.common.CacheKey;
import cn.com.dashihui.api.common.ResultMap;
import cn.com.dashihui.api.dao.SerShopPush;
import cn.com.dashihui.api.dao.SerVersion;
import cn.com.dashihui.api.service.SerCommonService;
import cn.com.dashihui.api.service.SerShopService;
import cn.com.dashihui.kit.CommonKit;
import cn.com.dashihui.kit.SMSKit;

public class SerCommonController extends BaseController {
	
	private SerCommonService commonService = new SerCommonService();
	private SerShopService shopService = new SerShopService();
	
	
	/**
     * 检查版本更新，判断是否有比入参最新的版本信息
     * @param TYPE 平台，A.Android B.iPhone 
     * @param VERSION 当前客户端版本号
     * @return 
     */
    public void checkVersion(){
    	String typeStr = getPara("TYPE");
    	String versionStr = getPara("VERSION");
    	if(StrKit.isBlank(typeStr)){
    		renderFailed("参数TYPE不能为空");
    		return;
    	}else if(!typeStr.equals("A")&&!typeStr.equals("B")){
    		renderFailed("参数TYPE不正确");
    		return;
    	}else if(StrKit.isBlank(versionStr)){
    		renderFailed("参数VERSION不能为空");
    		return;
    	}else if(!versionStr.matches("[1-9]{1,3}\\.\\d{1,3}\\.\\d{1,5}")){
    		renderFailed("参数VERSION格式不正确");
    		return;
    	}
    	String[] versionArr = versionStr.split("\\.");
    	int iVersionPart1 = Integer.valueOf(versionArr[0]), iVersionPart2 = Integer.valueOf(versionArr[1]), iVersionPart3 = Integer.valueOf(versionArr[2]);
    	//查询指定平台客户端的最新版本信息
    	SerVersion lastVersion = commonService.findLastVersion(typeStr);
    	if(lastVersion!=null){
    		int lVersionPart1 = lastVersion.getInt("versionPart1"), lVersionPart2 = lastVersion.getInt("versionPart2"), lVersionPart3 = lastVersion.getInt("versionPart3");
    		//将数据库中的最新版本信息的版本号三个部分，与入参的版本号三个部分进行比较，判断数据库中的最新版本信息是否比入参版本号要大
    		if(lVersionPart1>iVersionPart1
    				||(lVersionPart1==iVersionPart1&&lVersionPart2>iVersionPart2)
    				||(lVersionPart1==iVersionPart1&&lVersionPart2==iVersionPart2&&lVersionPart3>iVersionPart3)){
    			renderSuccess(ResultMap.newInstance()
    					.put("UPDATEFLG",lastVersion.getInt("updateFlag")==1?"C":"B")
    					.put("CURVER", CommonKit.join(".",lVersionPart1,lVersionPart2,lVersionPart3))
    					.put("DOWNURL", lastVersion.get("downurl"))
    					.put("UPDATELOG",lastVersion.get("updatelog"))
    					.put("RELEASEDATE", lastVersion.getDate("releaseDate")));
    			return;
    		}
    	}
		renderSuccess(ResultMap.newInstance().put("UPDATEFLG","A"));
    }
	
	/**
	 * 找回密码时发送手机验证码
     * @param SIGNATURE 设备识别码
	 * @param PHONE 手机号码
	 */
    public void sendResetPwdCode(){
    	String phone = getPara("PHONE");
    	if(StrKit.isBlank(phone)){
    		renderFailed("参数PHONE不能为空");
    		return;
    	}else if(shopService.getShopByPhone(phone)==null){
    		renderFailed("手机号码未注册");
    		return;
    	}
    	String randomNum = CommonKit.randomNum();
    	
		if(SMSKit.sendFindPwdCode(phone, randomNum)){
			CacheHolder.cache(CacheHolder.key(CacheKey.CACHE_RESETPWD_SMS_VALID_CODE_KEY,phone), randomNum, CacheHolder.TIME_FIVE_MINUTE);
			if(PropKit.getBoolean("debug.sms")){
				//短信接口调试模式时，将短信验证码返回给客户端
				renderSuccess(ResultMap.newInstance().put("CODE",randomNum));
				return;
			}else{
				renderSuccess();
				return;
			}
		}
		renderFailed("验证码发送失败，请重新获取");
    }
	
	/**
	 * 注册设备信息
	 */
	public void registerDevice(){
		String shopid = getPara("SHOPID");
		String clientid = getPara("CLIENTID");
		String deviceid = getPara("DEVICEID");
		if(StrKit.isBlank(shopid)){
    		renderFailed("参数SHOPID不能为空");
    		return;
    	}else if(StrKit.isBlank(clientid)){
    		renderFailed("参数CLIENTID不能为空");
    		return;
    	}else if(StrKit.isBlank(deviceid)){
    		renderFailed("参数DEVICEID不能为空");
    		return;
    	}
		int shopidInt = Integer.valueOf(shopid).intValue();
		SerShopPush shopPush = commonService.findShopPush(shopidInt, clientid, deviceid);
		if(shopPush == null){
			if(commonService.addShopPush(new SerShopPush().set("shopid", shopidInt).set("clientid", clientid)
					                                   .set("deviceid", deviceid))){
				renderSuccess();
				return;
			}else{
				renderFailed("注册失败");
	    		return;
			}
		}else{
			renderSuccess();
			return;
		}
	}
	
	/**
	 * 清除手机的注册信息
	 */
	public void clearDevice(){
		String shopid = getPara("SHOPID");
		String clientid = getPara("CLIENTID");
		String deviceid = getPara("DEVICEID");
		if(StrKit.isBlank(shopid)){
    		renderFailed("参数SHOPID不能为空");
    		return;
    	}else if(StrKit.isBlank(clientid)){
    		renderFailed("参数CLIENTID不能为空");
    		return;
    	}else if(StrKit.isBlank(deviceid)){
    		renderFailed("参数DEVICEID不能为空");
    		return;
    	}
    	if(commonService.delShopPush(Integer.valueOf(shopid).intValue(), clientid, deviceid)){
    		renderSuccess();
    		return;
    	}else{
    		renderFailed("删除注册信息出错");
    		return;
    	}
    	
	}
}

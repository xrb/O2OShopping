<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="mtag" uri="mytag"%>
<!DOCTYPE html>
<html>
<head>
	<title>大实惠云商服务平台 社区o2o平台</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="Keywords" content="河南大实惠,大实惠云商,社区O2O,O2O创业，智慧社区,社区便利店,便利店加盟,连锁便利店,河南便利店" />
	<meta name="Description" content="河南大实惠电子商务有限公司是一家突破型电商平台，通过社区O2O模式，为小区业主提供更便捷、更安全、更舒适的生活服务体验，大实惠云商用户通过大实惠云商APP其他社区业主沟通和分享、发起交易、组织社区活动等，全面成为社区居民的智慧生活管家！" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<script type="text/javascript">window.BASE_PATH = '${BASE_PATH}';</script>
	<link rel="stylesheet" type="text/css" href="${BASE_PATH }/static/css/css.css">
	<link rel="stylesheet" type="text/css" href="${BASE_PATH }/static/css/others.css">
	<link rel="stylesheet" type="text/css" href="${BASE_PATH }/static/css/home_lunbo.css">
	<link rel="alternate icon" type="images/png" href="${BASE_PATH }/static/img/bsh_logo1.png">
	<script type="text/javascript" src="${BASE_PATH }/static/js/common.js"></script>
	<script type="text/javascript" src="${BASE_PATH }/static/js/jquery.min.js"></script>
	<script type="text/javascript" src="${BASE_PATH}/static/plugins/layer/layer.js"></script>
	<script type="text/javascript" src="${BASE_PATH }/static/js/index.js"></script>
	<style>
		.sev_bottom li a{
			width:90%;
		}
		.sev_bottom li p{
		
			padding: 15% 32% 22% 0;
			 width: 40%;
		}
		.sev_bottom li a{
			 right: -0.75%;
		}
		@media screen and (max-width:1440px) {
			.right dl {
			    width: 23%;
			}
			.bevent-list .time {
				top: 27%;
			}
			.home_pro_download li {
				margin-left: 0;
			}
			.right img {
				width: 80%;
			}
			.about .bigbox .right {
				right: 8%;
			}
			/*.sev_top_left, .sev_top_right{width:45.6%;float:none;}
						.sev_top{padding-bottom:5%;}*/

			.bevent-list .ico {
	  			right: 73.5%;
	  		}
			.bevent-list .time em {
			    margin-top: 7%;
			    margin-left: -11%;
			}
			.bevent-list li {
				width: 68.9%;
			}
			.download_1 img {
			    width: 100px;
			    height: 100px;}
			.download_1 .btn{
				height: 2.6rem;
			    line-height: 2.6rem;
			    font-size: 1.3rem;
			    }
			 .download_1 .btn{
				 left:40%;
			 }
			 .box11 {
			    margin-left: 25%;
			}
			.box2 {
			    margin-right: 26%;
			}
			.home_pro_download .box2 li {
			    margin-left: 120px;
			}
		
			.sev_bottom li p {
			    padding: 20% 12% 22% 0;
			    width: 55%;
		    }
			.sev_bottom li a{
				right: -2.8%;
			}
			.sev_bottom li a{
			padding-top: 4.1%;
			
			}
			div.indexmaindiv{
			    padding-left: 10%;
			}
			.home_pro_download{
				background-size: 100% 100%;
			}
		}
			
			.aboutus p{font-size: 220%;}
			.us .p1{font-size: 180%;}
			.home_sev .us .p1{font-size: 150%;}
			.big-event .p4 {
				background-color: #E73F18;
				height: 34px;
				width: 116px;
				border-radius: 17px;
				color: #fff;
				font-size: 16px;
				text-align: center;
				line-height: 34px;
				margin-left: -140%;
				margin-top: 20%;
			}
			
			.coop_pat {
				background: #e73f18 !important;
			}
			
			.sev_top_left_img {
				position: relative;
				animation: mymove 3s;
				-webkit-animation: mymove 3s; /*Safari and Chrome*/
				animation-direction: alternate;
			}
			
			@keyframes mymove { 
				0% {left: 0px;}
				50%{left:200px;}
				100%{left:0px;}
			}
			@-webkit-keyframes mymove /*Safari and Chrome*/ { 
				0% {left: 0px;}
				50%{left:200px;}
				100%{left:0px;}
			}
			.beian:link,.beian:visited,.beian:hover{
				color:#fff;
			}
	</style>
</head>
<body>
	<center>
		<header class="header">
			<div class="header_left">
				<a><img src="${BASE_PATH }/static/img/bsh_logo.png" /></a>
			</div>
			<div class="header_right">
				<ul>
					<li class="nav-active"><a class="button wobble-horizontal">网站首页</a></li>
					<li><a href="${BASE_PATH }/cooperate" class="button wobble-horizontal">合作加盟</a></li>
					<li><a href="${BASE_PATH }/about" class="button wobble-horizontal">关于我们</a></li>
					<li><a href="${BASE_PATH }/notice" class="button wobble-horizontal">新闻动态</a></li>
					<li><a href="${BASE_PATH }/contact" class="button wobble-horizontal">联系我们</a></li>
				</ul>
			</div>
		</header>
		<!--轮播图开始-->
		<section id="rt-showcase-surround">
			<div id="rt-showcase" class="slider-container rt-overlay-dark">
				<div class="rt-container slider-container">
					<div class="rt-grid-12 rt-alpha rt-omega">
						<!--[if IE]><link rel="stylesheet" href="css/ie.css"><![endif]-->
						<!--[if lte IE 9]><script type="text/javascript" src="js/ie.js"></script><![endif]-->
						<div class="csslider1 autoplay">
							<input name="cs_anchor1" autocomplete="off" id="cs_slide1_0"
								type="radio" class="cs_anchor slide"> <input
								name="cs_anchor1" autocomplete="off" id="cs_slide1_1"
								type="radio" class="cs_anchor slide"> <input
								name="cs_anchor1" autocomplete="off" id="cs_slide1_2"
								type="radio" class="cs_anchor slide"> <input
								name="cs_anchor1" autocomplete="off" id="cs_play1" type="radio"
								class="cs_anchor" checked> <input name="cs_anchor1"
								autocomplete="off" id="cs_pause1" type="radio" class="cs_anchor">
							<ul>
								<div
									style="width: 100%; visibility: hidden; font-size: 0px; line-height: 0;">
									<img src="${BASE_PATH }/static/img/ad1.png" style="width: 100%;">
								</div>
								<li class="num0 img"><img src="${BASE_PATH }/static/img/ad1.png" alt="Clouds"
									title="Clouds" /></li>
								<li class="num1 img"><img src="${BASE_PATH }/static/img/ad2.png"
									alt="Typewriter" title="Typewriter" /></li>
								<li class="num2 img"><img src="${BASE_PATH }/static/img/ad3.png"
									alt="Bicycle" title="Bicycle" /></li>
							</ul>
							<div class="cs_description">
								<label class="num0">
		                            <span class="cs_title">
		                            	<span class="cs_wrapper">
		                            		<div class="download_1">
						            			<img src="${BASE_PATH }/static/img/erweima.png">
									            <a href="http://www.91dashihui.com/app/download" 
									            	class="btn btn-1 download-btn"  data-trigger="download" 
									            	style="background:#e73f18;border:1px solid #fff;">Android版</a>
									            <a href="http://www.91dashihui.com/app/download" 
									            	class="btn btn-2 download-btn"  data-trigger="download">iPhone版</a>
											</div>
										</span>
									</span>
		                        </label>
		                        <label class="num1">
		                            <span class="cs_title"><span class="cs_wrapper"></span></span>
		                        </label>
		                        <label class="num2">
		                            <span class="cs_title"><span class="cs_wrapper"></span></span>
		                            
		                        </label>
							</div>
							<div class="cs_arrowprev">
								<label class="num0" for="cs_slide1_0"></label> <label
									class="num1" for="cs_slide1_1"></label> <label class="num2"
									for="cs_slide1_2"></label>
							</div>
							<div class="cs_arrownext">
								<label class="num0" for="cs_slide1_0"></label> <label
									class="num1" for="cs_slide1_1"></label> <label class="num2"
									for="cs_slide1_2"></label>
							</div>
							<div class="cs_bullets">
								<label class="num0" for="cs_slide1_0"> <span
									class="cs_point"></span> <span class="cs_thumb"><img
										src="${BASE_PATH }/static/img/ad1_sm.png" alt="Clouds" title="Clouds" /></span>
								</label> <label class="num1" for="cs_slide1_1"> <span
									class="cs_point"></span> <span class="cs_thumb"><img
										src="${BASE_PATH }/static/img/ad2_sm.png" alt="Typewriter" title="Typewriter" /></span>
								</label> <label class="num2" for="cs_slide1_2"> <span
									class="cs_point"></span> <span class="cs_thumb"><img
										src="${BASE_PATH }/static/img/ad3_sm.png" alt="Bicycle" title="Bicycle" /></span>
								</label>
							</div>
						</div>
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</section>
		<!--轮播图结束-->

		<!--公司介绍-->
		<div class="about">
			<div class="us">
				<span class="aboutus"><p>ABOUT US</p></span> <span class="hong"></span>
				<p class="p1">为什么选择我们</p>
			</div>
			<div class="bigbox">
				<div class="left">
					<p class="p2">
						大实惠云商自成立以来发展迅猛，短短半年时间已在河南省内签约地市加盟商 <font class="font_red">7</font>
						家，社区连锁便利店<font class="font_red"> 200 </font>家，并已建设<font
							class="font_red">1万平</font>的大型<font class="font_red">省级</font>物流配送中心。
					</p>
					<p class="p3">
						作为省内知名的电子商务服务企业，大实惠云商不仅可以为便利店经营者提供<font class="font_red">经营管理</font>方案，还能为平台入驻商户提供后台支持、物流配送、经营策划方案等
						一系列服务，此外，还可以为物业公司提供信息<font class="font_red">管理平台</font>。大实惠云商汇聚了一
						流的 IT 技术团队，在国内率先开发出集微商城、云服务、社交、公益等为一体的 <font class="font_red">新型社区生活服务</font>系统，这一切都离不开先进技术的支持。
					</p>
					<a href="${BASE_PATH }/about" class="button wobble-horizontal"
						style="margin: 1% 0 0 18%;">
						<p class="p4">
							查看更多<img src="${BASE_PATH }/static/img/arrow.png">
						</p>
					</a>
				</div>
				<div class="right">
					<a href="http://v.qq.com/boke/page/w/0/9/w0185k6yu39.html"
						target="_blank" class="button wobble-horizontal">
						<img src="${BASE_PATH }/static/img/aboutus.png"/>
					</a>
				</div>
			</div>
		</div>

		<!--产品下载-->
		<div class="home_pro_download">
			<div class="box11">
				<ul>
					<li><a><img
							onmousemove="this.src=&#39;${BASE_PATH }/static/img/pro_2.png&#39;"
							onmouseout="this.src=&#39;${BASE_PATH }/static/img/gf1.png&#39;"
							src="${BASE_PATH }/static/img/gf1.png"></a></li>
					<li><a><img
							onmousemove="this.src=&#39;${BASE_PATH }/static/img/pro_3.png&#39;"
							onmouseout="this.src=&#39;${BASE_PATH }/static/img/dz1.png&#39;"
							src="${BASE_PATH }/static/img/dz1.png"></a></li>
					<li><a><img
							onmousemove="this.src=&#39;${BASE_PATH }/static/img/pro_4.png&#39;"
							onmouseout="this.src=&#39;${BASE_PATH }/static/img/cy1.png&#39;"
							src="${BASE_PATH }/static/img/cy1.png"></a></li>
				</ul>
			</div>
			<div class="box2">
				<ul>
					<li><a><img
							onmousemove="this.src=&#39;${BASE_PATH }/static/img/pro_5.png&#39;"
							onmouseout="this.src=&#39;${BASE_PATH }/static/img/xrz.png&#39;"
							src="${BASE_PATH }/static/img/xrz.png"></a></li>
					<li><a><img
							onmousemove="this.src=&#39;${BASE_PATH }/static/img/pro_11.png&#39;"
							onmouseout="this.src=&#39;${BASE_PATH }/static/img/zfb.png&#39;"
							src="${BASE_PATH }/static/img/zfb.png"></a></li>
				</ul>
			</div>
		</div>

		<!--我们的服务-->
		<div class="home_sev">
			<div class="us">
				<span class="aboutus"><p>我们的服务</p></span> <span class="hong"></span>
				<p class="p1">明明可以靠价格取胜 偏偏靠服务</p>
			</div>
			<div class="sev">
				<div class="sev_top">
					<div class="sev_top_left">
						<div class="sev_top_left_img">
							<img src="${BASE_PATH }/static/img/left_text.png">
						</div>
					</div>
					<div class="sev_top_right">
						<div class="sev_top_right_img">
							<img src="${BASE_PATH }/static/img/right_text.png">
						</div>
					</div>
				</div>
				<div class="sev_bottom">
					<ul>
						<li><a><p>炒菜没盐，没空下楼买，大实惠免费送上门，顺便帮您垃圾提下楼。</p></a></li>
						<li><a><p>在家看孩没空洗衣洗发买日用品，大实惠免费送上门，还能帮您看会孩，
									轻松省下保姆钱。</p></a></li>
						<li><a><p>家政、洗衣、维修、美食、休闲娱乐不好找，呼叫大实惠，师傅瞬间找上门。</p></a></li>
					</ul>
				</div>
			</div>
		</div>

		<!--合作伙伴-->
		<div class="coop_pat">
			<div class="us">
				<span class="aboutus"><p>合作伙伴</p></span> <span class="hong"></span>
				<p class="p1">官方合作渠道 保证正品</p>
			</div>
			<div class="indexmaindiv" id="indexmaindiv">
				<div class="indexmaindiv1 clearfix">
					<div class="stylesgoleft" id="goleft"></div>
					<div class="maindiv1 " id="maindiv1">
						<ul>
							<li>
								<div class="playerdetail">
									<div class="detailimg">
										<img src="${BASE_PATH }/static/img/cooperate/1.png" />
									</div>
									<div class="detailimg">
										<img src="${BASE_PATH }/static/img/cooperate/2.png" />
									</div>
								</div>
							</li>
							<li>
								<div class="playerdetail">
									<div class="detailimg">
										<img src="${BASE_PATH }/static/img/cooperate/3.png" />
									</div>
									<div class="detailimg">
										<img src="${BASE_PATH }/static/img/cooperate/4.png" />
									</div>
								</div>
							</li>
							<li>
								<div class="playerdetail">
									<div class="detailimg">
										<img src="${BASE_PATH }/static/img/cooperate/5.png" />
									</div>
									<div class="detailimg">
										<img src="${BASE_PATH }/static/img/cooperate/6.png" />
									</div>
								</div>
							</li>
							<li>
								<div class="playerdetail">
									<div class="detailimg">
										<img src="${BASE_PATH }/static/img/cooperate/7.png" />
									</div>
									<div class="detailimg">
										<img src="${BASE_PATH }/static/img/cooperate/8.png" />
									</div>
								</div>
							</li>
							<li>
								<div class="playerdetail">
									<div class="detailimg">
										<img src="${BASE_PATH }/static/img/cooperate/9.png" />
									</div>
									<div class="detailimg">
										<img src="${BASE_PATH }/static/img/cooperate/10.png" />
									</div>
								</div>
							</li>
							<li>
								<div class="playerdetail">
									<div class="detailimg">
										<img src="${BASE_PATH }/static/img/cooperate/11.png" />
									</div>
									<div class="detailimg">
										<img src="${BASE_PATH }/static/img/cooperate/12.png" />
									</div>
								</div>
							</li>
							<li>
								<div class="playerdetail">
									<div class="detailimg">
										<img src="${BASE_PATH }/static/img/cooperate/13.png" />
									</div>
									<div class="detailimg">
										<img src="${BASE_PATH }/static/img/cooperate/14.png" />
									</div>
								</div>
							</li>
							<li>
								<div class="playerdetail">
									<div class="detailimg">
										<img src="${BASE_PATH }/static/img/cooperate/15.png" />
									</div>
									<div class="detailimg">
										<img src="${BASE_PATH }/static/img/cooperate/16.png" />
									</div>
								</div>
							</li>
							<li>
								<div class="playerdetail">
									<div class="detailimg">
										<img src="${BASE_PATH }/static/img/cooperate/17.png" />
									</div>
									<div class="detailimg">
										<img src="${BASE_PATH }/static/img/cooperate/18.png" />
									</div>
								</div>
							</li>
							<li>
								<div class="playerdetail">
									<div class="detailimg">
										<img src="${BASE_PATH }/static/img/cooperate/19.png" />
									</div>
									<div class="detailimg">
										<img src="${BASE_PATH }/static/img/cooperate/20.png" />
									</div>
								</div>
							</li>
						</ul>
					</div>
					<div class="stylesgoright" id="goright"></div>
				</div>
			</div>
		</div>
		<!--新闻动态-->
		<div class="home_news">
			<div class="us">
				<span class="aboutus"><p>新闻动态</p></span> <span class="hong"></span>
				<p class="p1">智慧社区o2o服务平台</p>
			</div>
			<div class="big-event index-all">
				<div class="box">
					<div class="bevent-list">
						<ul>
							<c:forEach var="noticeList" items="${noticeList1 }" varStatus="s">
								<a href="${BASE_PATH}/notice/detail/${noticeList.id}" target="_blank">
									<li class="hover">
										<span class="time">
											<em><fmt:formatDate value="${noticeList.createDate }" pattern="yyyy年MM月dd日"></fmt:formatDate></em>
											<i class="ico">&nbsp;</i>
										</span>
										<h3>${noticeList.title }</h3>
										<p><mtag:substring clearHTML="true" string="${noticeList.context }" limit="100"/></p>
									</li>
								</a>
							</c:forEach>
<!-- 								<li class="hover"><span class="time"><em>2016年05月1日</em>
									<i class="ico">&nbsp;</i></span>
									<h3>社区O2O，传统超市顺利转型的良药</h3>
									<p>一边是去年前三季度主要零售企业(含社区超市、大型商超)在国内共计关闭门超过130家，创关店数量新高。另一边却是超市在蓬勃发展着……</p>
								</li> -->
						</ul>
					</div>
					<!--end bevent-list -->
					<a class="button wobble-horizontal" href="${BASE_PATH }/notice/0"><p class="p4">
							查看更多<img src="${BASE_PATH }/static/img/arrow.png">
						</p></a>
				</div>
				<!--end box -->
			</div>
			<!--end big-event -->
		</div>
		<!--在线留言-->
		<footer>
			<h1>在线留言</h1>
			<h3>请您填写以下相关信息</h3>
			<div class="wbox  business margin100 clrfix">
				<div class="left">
					<form
						action="http://www.91dashihui.com/cooperate;jsessionid=546C1F14B3C702877DA1F8B841AEFF93#">
						<dl>
							<dt>
								<input type="text" id="name" name="name" maxlength="8" placeholder="请输入您的姓名" class="text"/>
							</dt>
							<dd>
								<input type="text" id="msisdn" name="msisdn" maxlength="11" placeholder="请输入您的手机号" class="text"/>
							</dd>
							<dt>
								<input type="text" id="mail" name="mail" maxlength="30" placeholder="请输入您的邮箱" class="text"/>
							</dt>
							<dd>
								<input type="text" id="qq" maxlength="10" name="qq" placeholder="请输入您的QQ" class="text" maxlength="10"/>
							</dd>
							<dd class="height178">
								<textarea id="context" name="context" maxlength="200" placeholder="请输入您的留言" class="text long height178"></textarea>
							</dd>
							<dd>
								<input type="button" id="save" class="submit" value="提交" onclick="javascript:doJoin();">
							</dd>
						</dl>
					</form>
				</div>
				<div class="right">
					<font>河南大实惠电子商务有限公司<br />联系电话：400-0079661/0371-86563519<br />电子邮箱：dashihui2015@163.com<br />公司地址：郑东新区绿地之窗尚峰座5层<br />
						<dl>
							<dt>
								<img src="${BASE_PATH }/static/img/wscm.png" />
							</dt>
							<dd>微商城订阅号</dd>
						</dl>
						<dl>
							<dt>
								<img src="${BASE_PATH }/static/img/a.png" />
							</dt>
							<dd>客户端二维码</dd>
						</dl>
					</font>
				</div>
			</div>
			<p class="big_foot_span" style="bottom:0;">
	  		    <span>Copyright©2015-2015.All Rights Reserved</span>
				河南大实惠电子商务有限公司版权所有&nbsp;&nbsp;<a href="http://www.miitbeian.gov.cn/publish/query/indexFirst.action" target="_blank" class="beian">豫ICP备15031706号-1</a> 
			</p>
		</footer>
	</center>
	<script type="text/javascript"> var _bdhmProtocol = (("https:" == document.location.protocol) ? " https://" : " http://"); document.write(unescape("%3Cscript src='" + _bdhmProtocol + "hm.baidu.com/h.js%3F60f951c38f2066e065bb57e48654c4ac' type='text/javascript'%3E%3C/script%3E")) </script>
</body>
</html>
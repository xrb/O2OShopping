package cn.com.dashihui.web.dao;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.plugin.activerecord.Model;

public class Notice extends Model<Notice>{
	private static final long serialVersionUID = 1L;
	private static Notice me = new Notice();
	public static Notice me(){
		return me;
	}
	
	private boolean hasChildren = false;

	public List<Notice> getChildren() {
		return get("children");
	}

	public void setChildren(List<Notice> children) {
		put("children", children);
		hasChildren = true;
	}
	
	public boolean hasChildren(){
		return hasChildren;
	}
	
	public void addChild(Notice child){
		List<Notice> children = getChildren();
		if(children==null){
			children = new ArrayList<Notice>();
		}
		children.add(child);
		setChildren(children);
		hasChildren = true;
	}
}

package cn.com.dashihui.web.controller;

import com.jfinal.plugin.activerecord.Record;

import cn.com.dashihui.web.base.BaseController;
import cn.com.dashihui.web.service.AppService;

/**
 * 客户端下载处理类
 */
public class AppController extends BaseController{
	private AppService service = new AppService();
    
    public void download(){
    	//查询安卓最新版本地址
    	Record record = service.findAndroidNewest();
    	if(record!=null){
    		setAttr("androidDownurl", record.getStr("downurl"));
    	}
    	render("/WEB-INF/page/download.jsp");
    }
}

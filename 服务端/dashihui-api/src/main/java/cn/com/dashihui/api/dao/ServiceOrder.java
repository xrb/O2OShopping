package cn.com.dashihui.api.dao;

import com.jfinal.plugin.activerecord.Model;

/**
 * 服务订单对象
 */
public class ServiceOrder  extends Model<ServiceOrder>{
	private static final long serialVersionUID = 1L;
	private static ServiceOrder me = new ServiceOrder();
	public static ServiceOrder me(){
		return me;
	}
}

package cn.com.dashihui.pay.wx.request;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import com.jfinal.kit.PropKit;

import cn.com.dashihui.kit.CommonKit;
import cn.com.dashihui.kit.DatetimeKit;
import cn.com.dashihui.pay.wx.kit.Signature;

public class PayOrderReqData {
	
	private String appid;
	private String partnerid;
	private String prepayid;
	//private String package;
	private String noncestr;
	private String timestamp;
	private String sign;
	
	public PayOrderReqData(String prepayid){
		 //微信分配的公众号ID（开通公众号之后可以获取到）
        setAppid(PropKit.get("weixinpay.appid"));

        //微信支付分配的商户号ID（开通公众号的微信支付功能之后可以获取到）
        setPartnerid(PropKit.get("weixinpay.mch_id"));
        
        setPrepayid(prepayid);
        
        //随机字符串，不长于32 位
        setNoncestr(CommonKit.getRandomStringByLength(32));
        
        setTimestamp(DatetimeKit.getTimestamp());
        
        //根据API给的签名规则进行签名
        String sign = Signature.getSign(toMap());
        setSign(sign);//把签名数据设置到Sign这个属性中
	}
	
	public String getAppid() {
		return appid;
	}
	public void setAppid(String appid) {
		this.appid = appid;
	}
	public String getPartnerid() {
		return partnerid;
	}
	public void setPartnerid(String partnerid) {
		this.partnerid = partnerid;
	}
	public String getPrepayid() {
		return prepayid;
	}
	public void setPrepayid(String prepayid) {
		this.prepayid = prepayid;
	}
	public String getNoncestr() {
		return noncestr;
	}
	public void setNoncestr(String noncestr) {
		this.noncestr = noncestr;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public String getSign() {
		return sign;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
   public Map<String,Object> toMap(){
        Map<String,Object> map = new HashMap<String, Object>();
        Field[] fields = this.getClass().getDeclaredFields();
        for (Field field : fields) {
            Object obj;
            try {
                obj = field.get(this);
                if(obj!=null){
                    map.put(field.getName(), obj);
                }
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        map.put("package", "Sign=WXPay");  
        return map;
	}
}

package cn.com.dashihui.pay.wx;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;

import com.jfinal.kit.PropKit;

import cn.com.dashihui.pay.wx.kit.HttpsRequest;
import cn.com.dashihui.pay.wx.kit.UtilKit;
import cn.com.dashihui.pay.wx.request.PreOrderReqData;
import cn.com.dashihui.pay.wx.request.QueryOrderReqData;
import cn.com.dashihui.pay.wx.response.PreOrderResData;
import cn.com.dashihui.pay.wx.response.QueryOrderResData;

public class WXPay {
	
	/**
	 * 向微信发起预支付订单请求，并返回微信端生成的订单号码
	 * @param orderNum 商户端订单号
	 * @param amount 本笔订单总金额，单位为分，需要换算
	 * @param remoteip 请求端IP，此为APP客户端IP
	 * @param startDate 订单生效时间
	 * @param expireDate 订单失效时间
	 * @return prepayid 微信端生成的订单号
	 */
	public static String prepay(String orderNum, String amount, String remoteip, String notifyUrl, String startDate, String expireDate){
		return prepay("大实惠云商超市", orderNum, amount, remoteip, notifyUrl, startDate, expireDate);
	}
	
	/**
	 * 向微信发起预支付订单请求，并返回微信端生成的订单号码
	 * @param body 提示性文字，用户在微信支付页面所看到的文字
	 * @param orderNum 商户端订单号
	 * @param amount 本笔订单总金额，单位为分，需要换算
	 * @param remoteip 请求端IP，此为APP客户端IP
	 * @param startDate 订单生效时间
	 * @param expireDate 订单失效时间
	 * @return prepayid 微信端生成的订单号
	 */
	public static String prepay(String body, String orderNum, String amount, String remoteip, String notifyUrl, String startDate, String expireDate){
		try {
			HttpsRequest request = new HttpsRequest();
			String responseStr = request.post(PropKit.get("weixinpay.unifiedorder"), new PreOrderReqData(body,orderNum,amount,remoteip,notifyUrl,startDate,expireDate));
			PreOrderResData preOrderResData = (PreOrderResData)UtilKit.getObjectFromXML(responseStr, PreOrderResData.class);
			return preOrderResData.getPrepay_id();
		} catch (UnrecoverableKeyException e) {
			e.printStackTrace();
		} catch (KeyManagementException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (KeyStoreException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 向微信发起订单支付结果查询请求
	 * @param wxorderid 微信订单号
	 * @return true：支付成功，false：支付失败
	 */
	public static boolean queryPayState(String wxorderid){
		try {
			HttpsRequest request = new HttpsRequest();
			String responseStr = request.post(PropKit.get("weixinpay.orderquery"), new QueryOrderReqData(wxorderid));
			QueryOrderResData queryOrderResData = (QueryOrderResData) UtilKit.getObjectFromXML(responseStr, QueryOrderResData.class);
			return queryOrderResData!=null&&queryOrderResData.getReturn_code().equals("SUCCESS")&&queryOrderResData.getResult_code().equals("SUCCESS");
		} catch (UnrecoverableKeyException e) {
			e.printStackTrace();
		} catch (KeyManagementException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (KeyStoreException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}
}

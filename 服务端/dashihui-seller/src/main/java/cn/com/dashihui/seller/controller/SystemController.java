package cn.com.dashihui.seller.controller;

import com.jfinal.aop.Clear;
import com.jfinal.kit.StrKit;

import cn.com.dashihui.seller.common.Constants;
import cn.com.dashihui.seller.dao.Seller;
import cn.com.dashihui.seller.interceptor.AuthLoginInterceptor;
import cn.com.dashihui.seller.kit.CommonKit;
import cn.com.dashihui.seller.kit.ValidateKit;
import cn.com.dashihui.seller.service.UserService;

public class SystemController extends BaseController{
	private UserService userService = new UserService();
	
	public void index(){
		redirect("/order/index");
	}

	/**
	 * 跳转首页
	 */
	public void detail(){
		render("detail.jsp");
	}
	
	/**
	 * 跳转至登录页面
	 */
	@Clear(AuthLoginInterceptor.class)
	public void login(){
		render("login.jsp");
	}
    
    /**
	 * 用户登录
	 * @param PHONE 手机号码
	 * @param PASSWORD 密码
	 */
	@Clear(AuthLoginInterceptor.class)
    public void doLogin(){
    	String msisdn = getPara("phone");
    	String password = getPara("password");
    	//验证各参数
    	if(StrKit.isBlank(msisdn)){
    		renderResult(1,"参数PHONE不能为空");
    		return;
    	}else if(!ValidateKit.Mobile(msisdn)){
    		renderResult(2,"参数PHONE格式不正确");
    		return;
    	}else if(StrKit.isBlank(password)){
    		renderResult(3,"参数PASSWORD不能为空");
    		return;
    	}
    	//查询指定手机号用户
    	Seller user = userService.findByUsername(msisdn);
    	if(user==null||!CommonKit.passwordsMatch(password, user.getStr("password"))){
    		renderResult(4,"用户名或登录密码错误");
    		return;
    	}
		//保存session以登录
		setSessionAttr(Constants.USER, user);
		renderSuccess();
    }
}

package cn.com.dashihui.wx.interceptor;

import com.jfinal.aop.Invocation;

import cn.com.dashihui.wx.controller.BaseController;

/**
 * 拦截用户请求，判断当前是否选择或定位过店铺
 */
public class AuthLocationInterceptor extends AjaxInterceptor{

	@Override
	public void onAjax(Invocation inv) {
		inv.invoke();
	}

	@Override
	public void onAjaxNope(Invocation inv) {
		BaseController c = (BaseController)inv.getController();
		//判断用户是否选择或定位过店铺
		if(c.getCurrentStore()==null){
			//未选择或定位，跳转至定位页面
			c.redirect("/location");
			return;
		}
		inv.invoke();
	}
}

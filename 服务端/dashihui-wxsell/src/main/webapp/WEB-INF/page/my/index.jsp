<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>我的</title>
		<!-- amazeui -->
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/ui/css/amazeui.min.css" />
		<!-- app -->
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/app/css/ui.css" />
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/app/css/m_my_index.css" />
	</head>
	<body class="app-navbar-body">
		<div class="app-top am-vertical-align">
			<c:choose>
			<c:when test="${USER!=null}">
			<c:choose>
			<c:when test="${USER.avator!=null&&fn:length(USER.avator)!=0}">
			<img src="${USER.avator}" width="20%">
			</c:when>
			<c:otherwise>
			<img src="${BASE_PATH}/static/app/img/avator_level${USER.level}.png" width="20%">
			</c:otherwise>
			</c:choose>
			<div class="app-m-l-10 am-vertical-align-middle">
				<c:choose>
				<c:when test="${USER.level==1}">
				<div>${USER.nickName}<a href="${BASE_PATH}/my/rule" class="app-level">普通会员</a></div>
				</c:when>
				<c:when test="${USER.level==2}">
				<div>${USER.nickName}<a href="${BASE_PATH}/my/rule" class="app-level">白金会员</a></div>
				</c:when>
				</c:choose>
				<div>${USER.username}</div>
			</div>
			</c:when>
			<c:otherwise>
			<img src="${BASE_PATH}/static/app/img/avator_default.png" width="20%">
			<div class="app-m-l-10 am-vertical-align-middle">
				<div>登录后可享受更多优惠</div>
				<div><a href="${BASE_PATH}/login">登录</a><span class="app-m-l-10">/</span><a href="${BASE_PATH}/regist" class="app-m-l-10">注册</a></div>
			</div>
			</c:otherwise>
			</c:choose>
		</div>
		<div class="app-tab">
			<div class="app-tab-item am-fl" style="width:50%;">
				<a href="${BASE_PATH}/my/collection" class="rdriver">
					<span id="stateCollected" class="am-navbar-label">0</span><br/>
					<span class="am-navbar-label">关注商品</span>
				</a>
			</div>
			<div class="app-tab-item am-fl" style="width:50%;">
				<a href="${BASE_PATH}/my/history">
					<span id="stateHistory" class="am-navbar-label">0</span><br/>
					<span class="am-navbar-label">浏览历史</span>
				</a>
			</div>
			<div class="am-cf"></div>
		</div>
		
		<div class="app-part">
			<div class="app-part-header">
				<span class="app-part-header-text">我的订单</span>
				<a href="${BASE_PATH}/order/list?state=1" class="am-fr">查看全部订单 <i class="am-icon-angle-right"></i></a>
			</div>
			<div class="app-part-main">
				<ul class="am-avg-sm-4">
					<li>
						<a href="${BASE_PATH}/order/list?state=2" class="rdriver">
							<img src="${BASE_PATH}/static/app/img/my_state_01.png" height="25" class="am-center">
							<span class="am-center am-text-center">待付款</span>
							<span id="stateNoPay" class="am-badge am-round" style="display:none;">0</span>
						</a>
					</li>
					<li>
						<a href="${BASE_PATH}/order/list?state=3" class="rdriver">
							<img src="${BASE_PATH}/static/app/img/my_state_02.png" height="25" class="am-center">
							<span class="am-center am-text-center">待收货</span>
							<span id="stateNoTake" class="am-badge am-round" style="display:none;">0</span>
						</a>
					</li>
					<li>
						<a href="${BASE_PATH}/order/list?state=4"class="rdriver">
							<img src="${BASE_PATH}/static/app/img/my_state_03.png" height="25" class="am-center">
							<span class="am-center am-text-center">待评价</span>
							<span id="stateNoEval" class="am-badge am-round" style="display:none;">0</span>
						</a>
					</li>
					<li>
						<a href="${BASE_PATH}/order/refund">
							<img src="${BASE_PATH}/static/app/img/my_refund.png" height="25" class="am-center">
							<span class="am-center am-text-center">退款</span>
							<span id="stateNoEval" class="am-badge am-round" style="display:none;">0</span>
						</a>
					</li>
				</ul>
			</div>
		</div>
		
		<div class="app-menu app-m-t-10">
			<div class="app-menu-item">
				<a href="${BASE_PATH}/my/myWallet">
					<i class="am-icon-credit-card"></i>
					<label>我的钱包</label>
					<i class="am-icon-angle-right am-fr"></i>
					<span class="am-cf"></span>
				</a>
			</div>
			<hr/>
			<div class="app-menu-item app-m-t-10">
				<a href="${BASE_PATH}/my/store">
					<i class="am-icon-location-arrow"></i>
					<label>我的门店</label>
					<i class="am-icon-angle-right am-fr"></i>
					<span class="am-cf"></span>
				</a>
			</div>
			<hr/>
			<div class="app-menu-item app-m-t-10">
				<a href="${BASE_PATH}/my/address?from=0">
					<i class="am-icon-truck"></i>
					<label>收货地址</label>
					<i class="am-icon-angle-right am-fr"></i>
					<span class="am-cf"></span>
				</a>
			</div>
			<hr/>
			<div class="app-menu-item app-m-t-10">
				<a href="${BASE_PATH}/fbIndex">
					<i class="am-icon-pencil"></i>
					<label>反馈与建议</label>
					<i class="am-icon-angle-right am-fr"></i>
					<span class="am-cf"></span>
				</a>
			</div>
		</div>
		
		<c:if test="${USER!=null}">
		<div style="width:100%;padding:5px;">
			<a href="javascript:doUnbind();" class="am-btn am-btn-warning app-m-t-10" style="width:100%;">解除绑定 </a>
		</div>
		</c:if>
		
		<div class="am-navbar am-cf am-no-layout app-navbar">
			<ul class="am-navbar-nav am-cf am-avg-sm-3">
				<li>
					<a href="${BASE_PATH}/index">
						<img src="${BASE_PATH}/static/app/img/menu_bot02.png"/>
						<span class="am-navbar-label">便利店</span>
					</a>
				</li>
				<li class="app-navbar-cart">
					<a href="${BASE_PATH}/cart">
						<img src="${BASE_PATH}/static/app/img/menu_bot03.png"/>
						<span class="am-navbar-label">购物车</span>
					</a>
					<span id="cartNum" class="am-badge am-round" style="display:none;"></span>
				</li>
				<li>
					<a href="javascript:void(0);" class="app-active">
						<img src="${BASE_PATH}/static/app/img/menu_bot25.png"/>
						<span class="am-navbar-label">我的</span>
					</a>
				</li>
			</ul>
		</div>
		
		<!-- include -->
		<%@include file="../include.jsp"%>
		<c:if test="${USER!=null}">
		<!-- lib -->
		<script type="text/javascript" src="${BASE_PATH}/static/lib/jquery/jquery-1.11.2.min.js"></script>
		<!-- amazeui -->
		<script type="text/javascript" src="${BASE_PATH}/static/ui/js/amazeui.min.js"></script>
		<!-- lib -->
		<script type="text/javascript" src="${BASE_PATH}/static/lib/layer/layer.js"></script>
		<!-- app -->
		<script type="text/javascript" src="${BASE_PATH}/static/app/js/kit.js"></script>
		<script type="text/javascript" src="${BASE_PATH}/static/app/js/history.js"></script>
		<script type="text/javascript">
			$(function(){
				$("#stateHistory").text(History.get().length);
				Kit.ajax.get("${BASE_PATH}/my/state",function(result){
					if(result.flag==0){
						var state = result.object;
						$("#stateCollected").text(state.collected);
						if(state.noPay>0){
							$("#stateNoPay").text(state.noPay).show();
						}
						if(state.noTake>0){
							$("#stateNoTake").text(state.noTake).show();
						}
						if(state.noEval>0){
							$("#stateNoEval").text(state.noEval).show();
						}
					}
				});
				//查询购物车状态
				Kit.ajax.post("${BASE_PATH}/cart/state",{},function(result){
					if(result.object.CART_COUNTER!=0)
						$("#cartNum").text(result.object.CART_COUNTER).show();
				});
			});
			function doUnbind(){
				Kit.ui.confirm("取消绑定后需要重新登录",function(){
					Kit.ajax.get("${BASE_PATH}/my/unbind",function(){
						Kit.ui.toast("解绑成功！");
						setTimeout(function(){
							Kit.render.refresh();
						}, 1000);
					});
				});
			}
		</script>
		</c:if>
	</body>
</html>
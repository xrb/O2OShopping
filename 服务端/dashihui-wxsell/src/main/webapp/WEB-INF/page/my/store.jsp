<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>${STORE.title}</title>
		<!-- amazeui -->
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/ui/css/amazeui.min.css" />
		<!-- app -->
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/app/css/ui.css" />
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/app/css/m_my_store.css" />
	</head>
	<body>
		<c:choose>
		<c:when test="${store.thumb!=null&&fn:length(store.thumb)!=0}">
		<div class="app-image-default"><img src="${FTP_PATH}${store.thumb}" width="100%"></div>
		</c:when>
		<c:otherwise>
		<div class="app-image-default"><img src="${BASE_PATH}/static/app/img/default.jpg"></div>
		</c:otherwise>
		</c:choose>
		
		<div class="app-detail-item app-m-t-10">
			<label>营业时间：</label>
			<span class="app-detail-item-text">${store.beginTime} - ${store.endTime}</span>
		</div>
		
		<div class="app-detail-item app-m-t-10">
			<label>地址：</label>
			<span class="app-detail-item-text">${store.address}</span>
		</div>
		
		<div class="app-detail-item app-m-t-10">
			<label>联系人：</label>
			<span class="app-detail-item-text">${store.manager}</span>
		</div>
		
		<div class="app-detail-item app-m-t-10">
			<label class="am-fl">电话：</label>
			<span class="app-detail-item-text am-fl">${store.tel}</span>
			<span class="am-cf"></span>
		</div>
		
		<div class="app-detail-item app-m-t-10">
			<label>邮箱：</label>
			<span class="app-detail-item-text">${store.mail}</span>
		</div>
		
		<div class="app-detail-item app-m-t-10">
			<label>配送说明：</label>
			<span class="app-detail-item-text">${store.deliverydes}</span>
		</div>
		
		<div class="app-detail-item app-m-t-10 app-m-b-10">
			<div>
				<label>商品质量：</label>
				<span>
					<i class="am-icon-star am-icon-md <c:if test="${store.eval1>=1}">app-text-red</c:if>"></i>
					<i class="am-icon-star am-icon-md app-m-l-5 <c:if test="${store.eval1>=2}">app-text-red</c:if>"></i>
					<i class="am-icon-star am-icon-md app-m-l-5 <c:if test="${store.eval1>=3}">app-text-red</c:if>"></i>
					<i class="am-icon-star am-icon-md app-m-l-5 <c:if test="${store.eval1>=4}">app-text-red</c:if>"></i>
					<i class="am-icon-star am-icon-md app-m-l-5 <c:if test="${store.eval1==5}">app-text-red</c:if>"></i>
				</span>
			</div>
			<div class="app-m-t-5">
				<label>配送速度：</label>
				<span>
					<i class="am-icon-star am-icon-md <c:if test="${store.eval2>=1}">app-text-red</c:if>"></i>
					<i class="am-icon-star am-icon-md app-m-l-5 <c:if test="${store.eval2>=2}">app-text-red</c:if>"></i>
					<i class="am-icon-star am-icon-md app-m-l-5 <c:if test="${store.eval2>=3}">app-text-red</c:if>"></i>
					<i class="am-icon-star am-icon-md app-m-l-5 <c:if test="${store.eval2>=4}">app-text-red</c:if>"></i>
					<i class="am-icon-star am-icon-md app-m-l-5 <c:if test="${store.eval2==5}">app-text-red</c:if>"></i>
				</span>
			</div>
			<div class="app-m-t-5">
				<label>服务质量：</label>
				<span>
					<i class="am-icon-star am-icon-md <c:if test="${store.eval3>=1}">app-text-red</c:if>"></i>
					<i class="am-icon-star am-icon-md app-m-l-5 <c:if test="${store.eval3>=2}">app-text-red</c:if>"></i>
					<i class="am-icon-star am-icon-md app-m-l-5 <c:if test="${store.eval3>=3}">app-text-red</c:if>"></i>
					<i class="am-icon-star am-icon-md app-m-l-5 <c:if test="${store.eval3>=4}">app-text-red</c:if>"></i>
					<i class="am-icon-star am-icon-md app-m-l-5 <c:if test="${store.eval3==5}">app-text-red</c:if>"></i>
				</span>
			</div>
		</div>
		
		<div data-am-widget="gotop" class="am-gotop am-gotop-fixed" >
			<a href="#top"><span class="am-gotop-title">回到顶部</span><i class="am-gotop-icon am-icon-chevron-up"></i></a>
		</div>
		
		<!-- include -->
		<%@include file="../include.jsp"%>
	</body>
</html>
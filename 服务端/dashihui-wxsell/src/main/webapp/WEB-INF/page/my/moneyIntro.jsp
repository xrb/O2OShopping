<%@ page language="java" contentType="text/html; charset=UTF-8" import="cn.com.dashihui.wx.common.UserCode" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>我的钱包</title>
		<!-- amazeui -->
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/ui/css/amazeui.min.css" />
		<!-- app -->
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/app/css/ui.css" />
		<link rel="stylesheet" type="text/css" href="${BASE_PATH }/static/app/css/m_my_money.css">
		<style type="text/css">
			.app-part-share{
				background-color: #fff;
			}
			.app-share-title{
				color: #e63f17;
				font-size: 18px;
				font-weight: bold;
			}
			.app-share-intro{
				font-size: 13px;
			}
			.app-share-url, .app-rule-url{
				color: #e63f17;
				font-size: 13px;
				text-decoration: underline;
			}
			p{
				margin: 1.0rem 0 0;
			}
		</style>
	</head>
	<body>
		<header class="am-header app-header">
			<div class="am-header-left am-header-nav">
          		<a href="javascript:history.go(-1);" class="app-title">
          			<i class="am-icon-angle-left am-icon-md"></i>
          		</a>
          	</div>
			<h1 class="am-header-title app-title">什么是实惠币</h1>
		</header>
		
		<div class="app-part app-part-share app-m-t-10">
			<span class="app-share-title">什么是实惠币？</span>
			<p class="app-share-intro">
				实惠币是您升级为白金会员后，您消费的每一笔金额，都将转化成不等额的实惠币，1实惠币等于1元人民币，同时您推荐的朋友每消费一笔，
				也都将为您存下一笔钱，推荐的朋友越多，您存的钱也就越多，边消费边理财的模式，让您成为持家小能手。
			</p>
			<c:if test="${currentUser.level == 2}">
				<a class="app-share-url" href="${BASE_PATH}/my/share">点击分享给好友</a>
			</c:if>
			<c:if test="${currentUser.level == 1}>">
				<a class="app-rule-url" href="${BASE_PATH}/my/rule">如何成为白金会员</a>
			</c:if>
		</div>
		
		<!-- include -->
		<%@include file="../include.jsp"%>
	</body>
</html>
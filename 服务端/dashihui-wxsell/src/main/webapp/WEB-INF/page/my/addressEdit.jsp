<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>${STORE.title}</title>
		<!-- amazeui -->
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/ui/css/amazeui.min.css" />
		<!-- app -->
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/app/css/ui.css" />
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/app/css/m_my_addressAdd.css" />
	</head>
	<body>
		<div class="app-part">
			<form class="am-form am-form-horizontal">
			<div class="app-part-item am-form-group">
				<label class="am-u-sm-2 am-form-label">姓名：</label>
				<div class="am-u-sm-10">
					<input type="text" id="linkNameInpt" name="linkName" value="${address.linkName}" placeholder="请填写收货人姓名" maxlength="20">
				</div>
			</div>
			<hr/>
			<div class="app-part-item am-form-group">
				<label class="am-u-sm-2"></label>
				<div class="am-u-sm-10">
					<a id="sex1" href="javascript:doChoose('sex',1,2);" class="am-fl checkbox <c:if test="${address.sex==1}">checked</c:if>"></a> <span class="am-fl app-m-l-5">先生</span>
					<a id="sex2" href="javascript:doChoose('sex',2,2);" class="am-fl app-m-l-20 checkbox <c:if test="${address.sex==2}">checked</c:if>"></a> <span class="am-fl app-m-l-5">女士</span>
					<span class="am-cf"></span>
				</div>
			</div>
			<hr/>
			<div class="app-part-item am-form-group">
				<label class="am-u-sm-2 am-form-label">电话：</label>
				<div class="am-u-sm-10">
					<input type="text" id="telInpt" name="tel" value="${address.tel}" placeholder="请填写电话号码" maxlength="15">
				</div>
			</div>
			<hr/>
			<div class="app-part-item am-form-group">
				<label class="am-u-sm-2 am-form-label">当前：</label>
				<div class="am-u-sm-10">
					<span>${STORE.title}</span>
				</div>
			</div>
			<hr/>
			<div class="app-part-item am-form-group">
				<label class="am-u-sm-2 am-form-label">地址：</label>
				<div class="am-u-sm-10">
					<input type="text" id="addressInpt" name="address" value="${address.address}" placeholder="请填写收货地址" maxlength="30">
				</div>
			</div>
			
			<hr/>
			<div class="app-part-item am-form-group">
				<label class="am-u-sm-2"></label>
				<div class="am-u-sm-10">
					<a id="isDefaultChk" href="javascript:doCheck();" class="am-fl checkbox <c:if test="${address.isDefault==1}">checked</c:if>"></a> <span class="am-fl app-m-l-5">设为默认收货地址</span>
					<span class="am-cf"></span>
				</div>
			</div>
			</form>
		</div>
		<div class="app-addr-btn">
			<a href="javascript:doSubmit();" class="am-btn am-btn-danger app-m-t-10" style="width:100%;">保存</a>
		</div>
		
		<!-- include -->
		<%@include file="../include.jsp"%>
		<!-- lib -->
		<script type="text/javascript" src="${BASE_PATH}/static/lib/jquery/jquery-1.11.2.min.js"></script>
		<!-- amazeui -->
		<script type="text/javascript" src="${BASE_PATH}/static/ui/js/amazeui.min.js"></script>
		<!-- lib -->
		<script type="text/javascript" src="${BASE_PATH}/static/lib/layer/layer.js"></script>
		<!-- app -->
		<script type="text/javascript" src="${BASE_PATH}/static/app/js/kit.js"></script>
		<script type="text/javascript">
			var addressid = "${address.id}";
			function doChoose(tab,target,count){
				for(var i=1;i<=count;i++){
					if(target==i){
						$("#"+tab+i).addClass("checked");
					}else{
						$("#"+tab+i).removeClass("checked");
					}
				}
			}
			function doCheck(){
				var checkbox = $("#isDefaultChk");
				if(checkbox.hasClass("checked")){
					checkbox.removeClass("checked");
				}else{
					checkbox.addClass("checked");
				}
			}
			function doSubmit(){
				var linkName = $("#linkNameInpt").val();
				var sex = $("#sex1").hasClass("checked")?1:2;
				var tel = $("#telInpt").val();
				var address = $("#addressInpt").val();
				var isDefault = $("#isDefaultChk").hasClass("checked")?1:0;
				if(Kit.validate.isBlank(linkName)){
					Kit.ui.alert("请填写收货人姓名");
				}else if(Kit.validate.isBlank(tel)){
					Kit.ui.alert("请填写电话号码");
				}else if(Kit.validate.isBlank(address)){
					Kit.ui.alert("请填写收货地址");
				}else{
					Kit.ajax.post("${BASE_PATH}/my/updateAddress",{addressid:addressid,linkName:linkName,sex:sex,tel:tel,address:address,isDefault:isDefault},function(result){
						if(result.flag == 2){
							Kit.ui.alert("请填写收货人姓名！");
						}else if(result.flag == 3){
							Kit.ui.alert("收货人姓名过长！");
						}else if(result.flag == 6){
							Kit.ui.alert("请填写电话号码！");
						}else if(result.flag == 7){
							Kit.ui.alert("电话号码格式不正确！");
						}else if(result.flag==0){
							Kit.ui.alert("保存成功！",function(){
								Kit.render.redirect("${BASE_PATH}/my/address?from=0");
							});
						}
					});
				}
			}
		</script>
	</body>
</html>
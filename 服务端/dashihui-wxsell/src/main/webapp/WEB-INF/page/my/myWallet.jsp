<%@ page language="java" contentType="text/html; charset=UTF-8" import="cn.com.dashihui.wx.common.UserCode" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>我的钱包</title>
		<!-- amazeui -->
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/ui/css/amazeui.min.css" />
		<!-- app -->
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/app/css/ui.css" />
		<link rel="stylesheet" type="text/css" href="${BASE_PATH }/static/app/css/m_my_wallet.css">
	</head>
	<body>
		<header class="am-header app-header">
			<div class="am-header-left am-header-nav">
          		<a href="javascript:history.go(-1);" class="app-title">
          			<i class="am-icon-angle-left am-icon-md"></i>
          		</a>
          	</div>
			<h1 class="am-header-title app-title">我的钱包</h1>
			<a class="am-header-right am-header-nav" href="${BASE_PATH }/my/moneyIntro">
				<img src="${BASE_PATH }/static/app/img/mywallet_question_mark.png" style="width:25px;height:25px;"/>
			</a>
		</header>
		
		<div data-am-sticky>
			<div class="app-part">
				<div class="app-part-top am-fl app-part-border">
					<div class="app-part-title">${currentUser.money }</div>
					<a class="app-part-text" href="${BASE_PATH }/my/myMoney">我的实惠币</a>
				</div>
				<div class="app-part-top am-fr">
					<div class="app-part-title">${invitedUserCount.inviteCount }个</div>
					<a class="app-part-text" href="${BASE_PATH }/my/myFriends">推荐的好友</a>
				</div>
				<div class="am-cf"></div>
			</div>
			
			<div class="app-part" style="background-color: #fff;padding: 0;">
				<div class="app-part-header">
					<div class="am-fl app-part-select" onclick="javascript:queryData('1');" id="type1">我的购物收益</div>
					<div class="am-fr" onclick="javascript:queryData('2');" id="type2">好友购物收益</div>
					<div class="am-cf"></div>
				</div>
			</div>
		</div>
		
		<div class="app-part" style="background-color: #fff;padding: 0;">
			<div class="app-part-main" id="dataList"></div>
		</div>
		
		<!-- include -->
		<%@include file="../include.jsp"%>
		<script type="text/javascript" src="${BASE_PATH}/static/lib/jquery/jquery-1.11.2.min.js"></script>
		<script type="text/javascript" src="${BASE_PATH}/static/ui/js/amazeui.min.js"></script>
		<script type="text/javascript" src="${BASE_PATH}/static/app/js/kit.js"></script>
		<script type="text/javascript" src="${BASE_PATH}/static/lib/layer/layer.js"></script>
		<script type="text/javascript" src="${BASE_PATH}/static/lib/template/template.js"></script>
		<script type="text/javascript" src="${BASE_PATH}/static/lib/template/template-ext.js"></script>
		<script type="text/html" id="myIncome">
		{{each list as myIncome index}}
			{{if index == 0}}<div>订单号：{{myIncome.fromOrderNum}}</div>
			{{else}}<div class="app-m-t-10">订单号：{{myIncome.fromOrderNum}}</div>
			{{/if}}
			<div class="am-fl app-m-t-5" style="color:#bbb;font-size:13px;">{{myIncome.createDate | dateFormat:'yyyy年MM月dd日 hh:mm:ss'}}</div>
			<div class="am-fr" style="color:#e63f17;font-size:18px;">+{{myIncome.amount}}<span style="font-size:13px;"> 实惠币</span></div>
			<div class="am-cf"></div>
			<hr class="app-m-t-10">
		{{/each}}
		</script>
		<script type="text/html" id="friendIncome">
		{{each list as myIncome index}}
			{{if index == 0}}<div>获得好友{{myIncome.nickName}}购物收益</div>
			{{else}}<div class="app-m-t-10">获得好友<span color="red">{{myIncome.nickName}}</span>购物收益</div>
			{{/if}}
			<div class="am-fl app-m-t-5" style="color:#bbb;font-size:13px;">{{myIncome.createDate | dateFormat:'yyyy年MM月dd日 hh:mm:ss'}}</div>
			<div class="am-fr" style="color:#e63f17;font-size:18px;">+{{myIncome.amount}}<span style="font-size:13px;"> 实惠币</span></div>
			<div class="am-cf"></div>
			<hr class="app-m-t-10">
		{{/each}}
		</script>
		<script type="text/javascript">
			var pageNum = 0,totalPage = 1,loading = false,searchPage,templateId;
			$(function(){
				queryData('1');
			})
			function queryData(type){
				$("#dataList").empty();
				if(type == '1'){
					$("#type1").addClass("app-part-select");
					$("#type2").removeClass("app-part-select");
					pageNum = 0,searchPage = 1,templateId = "myIncome";
				} else {
					$("#type1").removeClass("app-part-select");
					$("#type2").addClass("app-part-select");
					pageNum = 0,searchPage = 2,templateId = "friendIncome";
				}
				Kit.util.onPageEnd(function(){
					if(pageNum < totalPage && !loading){
						loading = true;
						$("#dataList").append("<div class=\"app-loading\">正在加载</div>");
						Kit.ajax.post("${BASE_PATH}/my/expenseRecord",{pageSize:10,pageNum:pageNum+1,searchType:searchPage},function(result){
							$("#dataList").append(template(templateId,result.object));
							pageNum = result.object.pageNumber;
							totalPage = result.object.totalPage;
						});
						$(".app-loading","#dataList").remove();
						loading = false;
					};
				});
			}
		</script>
	</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8" import="cn.com.dashihui.wx.common.OrderCode" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>我的订单</title>
		<!-- amazeui -->
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/ui/css/amazeui.min.css" />
		<!-- app -->
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/app/css/ui.css" />
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/app/css/m_my_order.css" />
	</head>
	<body>
		<header class="am-header am-header-default app-header">
			<div class="am-header-left am-header-nav">
          		<a href="${BASE_PATH}/my" class="app-title">
          			<i class="am-icon-angle-left am-icon-md"></i>
          		</a>
          	</div>
			<h1 class="am-header-title">我的订单</h1>
		</header>
		<div data-am-sticky>
			<ul class="am-avg-sm-4 app-tab">
				<li <c:if test="${state==1}">class="active"</c:if>><a href="${BASE_PATH}/order/list?state=1"><span>全部</span></a></li>
				<li <c:if test="${state==2}">class="active"</c:if>><a href="${BASE_PATH}/order/list?state=2"><span>待付款</span></a></li>
				<li <c:if test="${state==3}">class="active"</c:if>><a href="${BASE_PATH}/order/list?state=3"><span>待收货</span></a></li>
				<li <c:if test="${state==4}">class="active"</c:if>><a href="${BASE_PATH}/order/list?state=4"><span>待评价</span></a></li>
			</ul>
		</div>
		<div id="emptyPart" style="display:none;">
			<div>
				<img src="${BASE_PATH}/static/app/img/myorder_icon_empty.png" style="margin:0 auto;display:block;" width="140">
				<div class="am-text-center app-m-t-10">暂无订单，快去逛逛吧~</div>
				<a href="${BASE_PATH}/index" class="am-center am-radius am-btn-red-border app-m-t-10" style="width:120px;">立即逛逛</a>
			</div>
		</div>
		<div id="notEmptyPart" style="display:none;">
			<!-- 订单列表 -->
		    <div class="app-order-wrapper">
				<div id="orderList"></div>
			</div>
			
			<div data-am-widget="gotop" class="am-gotop am-gotop-fixed" style="bottom:50px;">
				<a href="#top"><span class="am-gotop-title">回到顶部</span><i class="am-gotop-icon am-icon-chevron-up"></i></a>
			</div>
		</div>
		
		<!-- include -->
		<%@include file="../include.jsp"%>
		<!-- lib -->
		<script type="text/javascript" src="${BASE_PATH}/static/lib/jquery/jquery-1.11.2.min.js"></script>
		<!-- amazeui -->
		<script type="text/javascript" src="${BASE_PATH}/static/ui/js/amazeui.min.js"></script>
		<!-- lib -->
		<script type="text/javascript" src="${BASE_PATH}/static/lib/jquery-lazyload/jquery.lazyload.min.js"></script>
		<script type="text/javascript" src="${BASE_PATH}/static/lib/layer/layer.js"></script>
		<script type="text/javascript" src="${BASE_PATH}/static/lib/template/template.js"></script>
		<!-- app -->
		<script type="text/javascript" src="${BASE_PATH}/static/app/js/kit.js"></script>
		<script type="text/javascript" src="${BASE_PATH}/static/lib/template/template-ext.js"></script>
		<script type="text/javascript">
			var pageNum = 0,totalPage = 1,loading = false,orderNum,state = '${empty state?0:state}';
			$(function(){
				//绑定“加载中”进入加载事件
				Kit.util.onPageEnd(function(){
	               	if(pageNum < totalPage && !loading){
	               		loading = true;
	               		$("#orderList").append("<div class=\"app-loading\">正在加载</div>");
	               		Kit.ajax.post("${BASE_PATH}/order/page",{pageNum:pageNum+1,pageSize:15,state:'${state}'},function(result){
							if(pageNum==0){
								if(result.object.totalRow>0){
									Kit.ui.toggle("#notEmptyPart","#emptyPart");
								}else{
									Kit.ui.toggle("#emptyPart","#notEmptyPart");
								}
							}
							$("#orderList").append(template("dataTpl",result.object));
							//图片延迟加载
							$("img.lazyload","#orderList").not(".lazyload-binded").lazyload({
								failurelimit : 100,
								effect : "show"
							}).addClass("lazyload-binded");
							$(".app-loading","#orderList").remove();
							pageNum = result.object.pageNumber;
							totalPage = result.object.totalPage;
							//重置加载标识
							loading = false;
						});
	               	}
				});
			});
			function toCancel(oNum){
				orderNum = oNum;
				Kit.ui.confirm("确定要取消订单？",function(){
					//弹出备注编辑弹出层
					/* var popup = '<div class="desc-dialog">'
							+ '<form class="am-form">'
							+ '<div class="am-form-group">'
							+ '<textarea id="reasonInpt" rows="4" placeholder="请输入取消理由，仅限100字以内" maxlength="100"></textarea>'
							+ '</div>'
							+ '<a href="javascript:doCancel();" class="am-btn am-btn-danger am-btn-block">确定</a>'
							+ '</form>'
							+ '</div>';
					Kit.ui.popup(popup); */
					doCancel();
				});
			}
			function doCancel(){
				var reason = $("#reasonInpt").val();
				Kit.ui.popdown();
				Kit.ajax.post("${BASE_PATH}/order/cancel",{orderNum:orderNum,reason:reason},function(result){
					if(result.flag==0){
						Kit.ui.toast("订单取消成功！");
						//判断当前列表如果是“全部”，则更新显示订单条目，如果不是，则移除该条目
						if(state!=1){
							$("#order"+orderNum).remove();
							//判断订单列表如果当前已经没有订单，则显示“无订单提示”
							if($(".app-order-item","#orderList").length==0){
								Kit.ui.toggle("#emptyPart","#notEmptyPart");
							}
						}else{
							$("#order"+orderNum).replaceWith(template("dataTpl",{"list":[result.object]}));
							//图片延迟加载
							$("img.lazyload","#order"+orderNum).not(".lazyload-binded").lazyload({
								failurelimit : 100,
								effect : "show"
							}).addClass("lazyload-binded");
						}
					}else{
						Kit.ui.alert(result.message);
					}
				});
			}
			function doDelete(oNum){
				orderNum = oNum;
				Kit.ui.confirm("确定要删除订单？",function(){
					Kit.ajax.post("${BASE_PATH}/order/delete",{orderNum:orderNum},function(result){
						if(result.flag!=1){
							Kit.ui.toast("订单删除成功！");
							$("#order"+orderNum).remove();
							//判断订单列表如果当前已经没有订单，则显示“无订单提示”
							if($(".app-order-item","#orderList").length==0){
								Kit.ui.toggle("#emptyPart","#notEmptyPart");
							}
						}else{
							Kit.ui.alert(result.message);
						}
					});
				});
			}
			function doReceive(oNum){
				orderNum = oNum;
				Kit.ui.confirm("确定签收订单？",function(){
					Kit.ajax.post("${BASE_PATH}/order/receive",{orderNum:orderNum},function(result){
						if(result.flag==0){
							Kit.ui.toast("订单签收成功！");
							//判断当前列表如果是“全部”，则更新显示订单条目，如果不是，则移除该条目
							/* if(state!=1){
								$("#order"+orderNum).remove();
								//判断订单列表如果当前已经没有订单，则显示“无订单提示”
								if($(".app-order-item","#orderList").length==0){
									Kit.ui.toggle("#emptyPart","#notEmptyPart");
								}
							}else{
								$("#order"+orderNum).replaceWith(template("dataTpl",{"list":[result.object]}));
								//图片延迟加载
								$("img.lazyload","#order"+orderNum).not(".lazyload-binded").lazyload({
									failurelimit : 100,
									effect : "show"
								}).addClass("lazyload-binded");
							} */
							Kit.render.redirect("${BASE_PATH}/order/tradeComplete/"+oNum);
						}else{
							Kit.ui.alert(result.message);
						}
					});
				});
			}
			function doUrge(oNum){
				orderNum = oNum;
				Kit.ajax.post("${BASE_PATH}/order/urge",{orderNum:orderNum},function(result){
					if(result.flag==0){
						Kit.ui.toast("催单成功，您的订单将很快安排配送");
						$("#order"+orderNum).find(".app-order-urge-btn").remove();
					}else{
						Kit.ui.alert(result.message);
					}
				});
			}
			function toTrack(oNum){
				Kit.render.redirect("${BASE_PATH}/order/track/"+oNum);
			}
			function toDetail(oNum){
				Kit.render.redirect("${BASE_PATH}/order/detail/"+oNum);
			}
		</script>
		<!-- tpl -->
		<script type="text/html" id="dataTpl">
			{{each list as order index}}
			<div id="order{{order.orderNum}}" class="app-order-item app-m-t-10">
				<div onclick="toDetail('{{order.orderNum}}')">
				{{if order.isSelf == 1}}<span>大实惠直营</span>
				{{else}}<span>{{order.storeName}}</span>
				{{/if}}
				{{if order.orderState==<%=OrderCode.OrderState.EXPIRE%>}}
				<span class="app-text-red am-fr">已过期</span>
				{{else if order.orderState==<%=OrderCode.OrderState.CANCEL%>}}
				<span class="app-text-red am-fr">已取消</span>
				{{else if order.orderState==<%=OrderCode.OrderState.FINISH%>}}
				<span class="app-text-red am-fr">交易完成</span>
				{{else if order.payType==<%=OrderCode.OrderPayType.ON_LINE%>&&order.takeType==<%=OrderCode.OrderTakeType.DELIVER%>}}
				<!-- 状态3：在线支付+送货上门 -->
					{{if order.payState==<%=OrderCode.OrderPayState.NO_PAY%>}}
					<span class="app-text-red am-fr">等待付款</span>
					{{else if order.packState==<%=OrderCode.OrderPackState.NO_ACCEPT%>}}
					<span class="app-text-red am-fr">付款成功</span>
					{{else if order.packState==<%=OrderCode.OrderPackState.HAD_ACCEPT%>}}
					<span class="app-text-red am-fr">打包中</span>
					{{else if order.packState==<%=OrderCode.OrderPackState.PACKING%>}}
					<span class="app-text-red am-fr">打包中</span>
					{{else if order.deliverState==<%=OrderCode.OrderDeliverState.NO_DELIVER%>}}
					<span class="app-text-red am-fr">待配送</span>
					{{else if order.deliverState==<%=OrderCode.OrderDeliverState.HAD_DELIVER%>}}
					<span class="app-text-red am-fr">配送中</span>
					{{/if}}
				{{else if order.payType==<%=OrderCode.OrderPayType.ON_LINE%>&&order.takeType==<%=OrderCode.OrderTakeType.TAKE_SELF%>}}
				<!-- 状态4：在线支付+门店自取 -->
					{{if order.payState==<%=OrderCode.OrderPayState.NO_PAY%>}}
					<span class="app-text-red am-fr">等待付款</span>
					{{else if order.packState==<%=OrderCode.OrderPackState.NO_ACCEPT%>}}
					<span class="app-text-red am-fr">付款成功</span>
					{{else if order.packState==<%=OrderCode.OrderPackState.HAD_ACCEPT%>}}
					<span class="app-text-red am-fr">打包中</span>
					{{else if order.packState==<%=OrderCode.OrderPackState.PACKING%>}}
					<span class="app-text-red am-fr">打包中</span>
					{{else if order.packState==<%=OrderCode.OrderPackState.PACK_FINISH%>}}
					<span class="app-text-red am-fr">等待取货</span>
					{{/if}}
				{{else if order.payType==<%=OrderCode.OrderPayType.ON_DELIVERY%>&&order.takeType==<%=OrderCode.OrderTakeType.DELIVER%>}}
				<!-- 状态5：货到付款+送货上门 -->
					{{if order.packState==<%=OrderCode.OrderPackState.NO_ACCEPT%>}}
					<span class="app-text-red am-fr">处理中</span>
					{{else if order.packState==<%=OrderCode.OrderPackState.HAD_ACCEPT%>}}
					<span class="app-text-red am-fr">打包中</span>
					{{else if order.packState==<%=OrderCode.OrderPackState.PACKING%>}}
					<span class="app-text-red am-fr">打包中</span>
					{{else if order.deliverState==<%=OrderCode.OrderDeliverState.NO_DELIVER%>}}
					<span class="app-text-red am-fr">待配送</span>
					{{else if order.deliverState==<%=OrderCode.OrderDeliverState.HAD_DELIVER%>}}
					<span class="app-text-red am-fr">配送中</span>
					{{/if}}
				{{else if order.payType==<%=OrderCode.OrderPayType.ON_DELIVERY%>&&order.takeType==<%=OrderCode.OrderTakeType.TAKE_SELF%>}}
				<!-- 状态5：货到付款+门店自取 -->
					{{if order.packState==<%=OrderCode.OrderPackState.NO_ACCEPT%>}}
					<span class="app-text-red am-fr">处理中</span>
					{{else if order.packState==<%=OrderCode.OrderPackState.HAD_ACCEPT%>}}
					<span class="app-text-red am-fr">打包中</span>
					{{else if order.packState==<%=OrderCode.OrderPackState.PACKING%>}}
					<span class="app-text-red am-fr">打包中</span>
					{{else if order.packState==<%=OrderCode.OrderPackState.PACK_FINISH%>}}
					<span class="app-text-red am-fr">等待取货</span>
					{{/if}}
				{{/if}}
				<span class="am-cf"></span>
				{{each order.goodsList as goods}}
				<hr/>
				<div class="am-g app-text-14px">
					<div class="am-u-sm-2 am-u-md-3 am-u-lg-3" style="padding:2px;"><img data-original="${FTP_PATH}{{goods.thumb}}" width="100%" class="lazyload"></div>
					<div class="am-u-sm-10 am-u-md-9 am-u-lg-9" style="padding:2px;">
						<div class="am-u-sm-12 am-u-md-12 am-u-lg-12">{{goods.name}}</div>
					</div>
					<span class="am-cf"></span>
				</div>
				{{/each}}
				</div>
				<hr/>
				<div class="app-m-t-10">
				<div class="am-fl app-p-5"><span>实付款：<i class="am-icon-rmb"></i> {{order.amount}}</span></div>
				{{if order.orderState==<%=OrderCode.OrderState.EXPIRE%>}}
				<div class="am-fr">
					<a href="javascript:doDelete('{{order.orderNum}}');" class="am-btn am-btn-hollow am-radius app-btn-order">删除订单</a>
				</div>
				{{else if order.orderState==<%=OrderCode.OrderState.CANCEL%>}}
				<div class="am-fr">
					<a href="javascript:doDelete('{{order.orderNum}}');" class="am-btn am-btn-hollow am-radius app-btn-order">删除订单</a>
				</div>
				{{else if order.orderState==<%=OrderCode.OrderState.FINISH%>}}
				<div class="am-fr">
					<a href="javascript:doDelete('{{order.orderNum}}');" class="am-btn am-btn-hollow am-radius app-btn-order">删除订单</a>
					{{if order.evalState==0}}
					<a href="${BASE_PATH}/order/eval/{{order.orderNum}}" class="am-btn am-btn-red am-radius app-btn-order">去评价</a>
					{{/if}}
				</div>
				{{else if order.payType==<%=OrderCode.OrderPayType.ON_LINE%>&&order.takeType==<%=OrderCode.OrderTakeType.DELIVER%>}}
				<!-- 状态3：在线支付+送货上门 -->
					{{if order.payState==<%=OrderCode.OrderPayState.NO_PAY%>}}
					<div class="am-fr">
						<a href="${BASE_PATH}/order/prepay/{{order.orderNum}}" class="am-btn am-btn-red am-radius app-btn-order">去支付</a>
					</div>
					{{else if order.packState==<%=OrderCode.OrderPackState.NO_ACCEPT%>}}
					<div class="am-fr">
						<a href="javascript:doUrge('{{order.orderNum}}');" class="am-btn am-btn-hollow am-radius app-btn-order">催单</a>
						<a href="${BASE_PATH}/order/track/{{order.orderNum}}" class="am-btn am-btn-hollow am-radius app-btn-order">订单跟踪</a>
					</div>
					{{else if order.packState==<%=OrderCode.OrderPackState.HAD_ACCEPT%>}}
					<div class="am-fr">
						<a href="javascript:doUrge('{{order.orderNum}}');" class="am-btn am-btn-hollow am-radius app-btn-order">催单</a>
						<a href="${BASE_PATH}/order/track/{{order.orderNum}}" class="am-btn am-btn-hollow am-radius app-btn-order">订单跟踪</a>
					</div>
					{{else if order.packState==<%=OrderCode.OrderPackState.PACKING%>}}
					<div class="am-fr">
						<a href="javascript:doUrge('{{order.orderNum}}');" class="am-btn am-btn-hollow am-radius app-btn-order">催单</a>
						<a href="${BASE_PATH}/order/track/{{order.orderNum}}" class="am-btn am-btn-hollow am-radius app-btn-order">订单跟踪</a>
					</div>
					{{else if order.deliverState==<%=OrderCode.OrderDeliverState.NO_DELIVER%>}}
					<div class="am-fr">
						<a href="javascript:doUrge('{{order.orderNum}}');" class="am-btn am-btn-hollow am-radius app-btn-order">催单</a>
						<a href="${BASE_PATH}/order/track/{{order.orderNum}}" class="am-btn am-btn-hollow am-radius app-btn-order">订单跟踪</a>
					</div>
					{{else if order.deliverState==<%=OrderCode.OrderDeliverState.HAD_DELIVER%>}}
					<div class="am-fr">
						<a href="javascript:doReceive('{{order.orderNum}}');" class="am-btn am-btn-red am-radius app-btn-order">确认收货</a>
						<a href="${BASE_PATH}/order/track/{{order.orderNum}}" class="am-btn am-btn-hollow am-radius app-btn-order">订单跟踪</a>
					</div>
					{{/if}}
				{{else if order.payType==<%=OrderCode.OrderPayType.ON_LINE%>&&order.takeType==<%=OrderCode.OrderTakeType.TAKE_SELF%>}}
				<!-- 状态4：在线支付+门店自取 -->
					{{if order.payState==<%=OrderCode.OrderPayState.NO_PAY%>}}
					<div class="am-fr">
						<a href="${BASE_PATH}/order/prepay/{{order.orderNum}}" class="am-btn am-btn-red am-radius app-btn-order">去支付</a>
					</div>
					{{else if order.packState==<%=OrderCode.OrderPackState.NO_ACCEPT%>}}
					{{else if order.packState==<%=OrderCode.OrderPackState.HAD_ACCEPT%>}}
					{{else if order.packState==<%=OrderCode.OrderPackState.PACKING%>}}
					{{else if order.packState==<%=OrderCode.OrderPackState.PACK_FINISH%>}}
					<div class="am-fr">
						<a href="javascript:doReceive('{{order.orderNum}}');" class="am-btn am-btn-red am-radius app-btn-order">确认取货</a>
						<a href="${BASE_PATH}/order/track/{{order.orderNum}}" class="am-btn am-btn-hollow am-radius app-btn-order">订单跟踪</a>
					</div>
					{{/if}}
				{{else if order.payType==<%=OrderCode.OrderPayType.ON_DELIVERY%>&&order.takeType==<%=OrderCode.OrderTakeType.DELIVER%>}}
				<!-- 状态5：货到付款+送货上门 -->
					{{if order.packState==<%=OrderCode.OrderPackState.NO_ACCEPT%>}}
					<div class="am-fr">
						<a href="javascript:doUrge('{{order.orderNum}}');" class="am-btn am-btn-hollow am-radius app-btn-order">催单</a>
						<a href="${BASE_PATH}/order/track/{{order.orderNum}}" class="am-btn am-btn-hollow am-radius app-btn-order">订单跟踪</a>
					</div>
					{{else if order.packState==<%=OrderCode.OrderPackState.HAD_ACCEPT%>}}
					<div class="am-fr">
						<a href="javascript:doUrge('{{order.orderNum}}');" class="am-btn am-btn-hollow am-radius app-btn-order">催单</a>
						<a href="${BASE_PATH}/order/track/{{order.orderNum}}" class="am-btn am-btn-hollow am-radius app-btn-order">订单跟踪</a>
					</div>
					{{else if order.packState==<%=OrderCode.OrderPackState.PACKING%>}}
					<div class="am-fr">
						<a href="javascript:doUrge('{{order.orderNum}}');" class="am-btn am-btn-hollow am-radius app-btn-order">催单</a>
						<a href="${BASE_PATH}/order/track/{{order.orderNum}}" class="am-btn am-btn-hollow am-radius app-btn-order">订单跟踪</a>
					</div>
					{{else if order.deliverState==<%=OrderCode.OrderDeliverState.NO_DELIVER%>}}
					<div class="am-fr">
						<a href="javascript:doUrge('{{order.orderNum}}');" class="am-btn am-btn-hollow am-radius app-btn-order">催单</a>
						<a href="${BASE_PATH}/order/track/{{order.orderNum}}" class="am-btn am-btn-hollow am-radius app-btn-order">订单跟踪</a>
					</div>
					{{else if order.deliverState==<%=OrderCode.OrderDeliverState.HAD_DELIVER%>}}
					<div class="am-fr">
						<a href="javascript:doReceive('{{order.orderNum}}');" class="am-btn am-btn-red am-radius app-btn-order">确认收货</a>
						<a href="${BASE_PATH}/order/track/{{order.orderNum}}" class="am-btn am-btn-hollow am-radius app-btn-order">订单跟踪</a>
					</div>
					{{/if}}
				{{else if order.payType==<%=OrderCode.OrderPayType.ON_DELIVERY%>&&order.takeType==<%=OrderCode.OrderTakeType.TAKE_SELF%>}}
				<!-- 状态5：货到付款+门店自取 -->
					{{if order.packState==<%=OrderCode.OrderPackState.NO_ACCEPT%>}}
					{{else if order.packState==<%=OrderCode.OrderPackState.HAD_ACCEPT%>}}
					{{else if order.packState==<%=OrderCode.OrderPackState.PACKING%>}}
					{{else if order.packState==<%=OrderCode.OrderPackState.PACK_FINISH%>}}
					<div class="am-fr">
						<a href="javascript:doReceive('{{order.orderNum}}');" class="am-btn am-btn-red am-radius app-btn-order">确认取货</a>
						<a href="${BASE_PATH}/order/track/{{order.orderNum}}" class="am-btn am-btn-hollow am-radius app-btn-order">订单跟踪</a>
					</div>
					{{/if}}
				{{/if}}
				<div class="am-cf"></div>
				</div>
			</div>
			{{/each}}
		</script>
	</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8" import="cn.com.dashihui.wx.common.OrderCode" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>交易完成</title>
		<!-- amazeui -->
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/ui/css/amazeui.min.css" />
		<!-- app -->
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/app/css/ui.css" />
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/app/css/m_my_order_payResult.css" />
	</head>
	<body>
		<header class="am-header app-header">
			<div class="am-header-left am-header-nav">
          		<a href="${BASE_PATH}/order/list" class="app-title">
          			<i class="am-icon-angle-left am-icon-md"></i>
          		</a>
          	</div>
			<h1 class="am-header-title app-title">交易完成</h1>
		</header>
		
		<div id="panel" class="app-m-b-10"></div>
		
		<div style="background-color: transparent;border:none;height:auto;padding:0 20px;margin-bottom:10px;">
			<a href="${BASE_PATH}/order/detail/${orderNum}" class="am-btn am-btn-white am-radius" style="padding:0.7em 1.5em;width:49%;">查看订单</a>
			<a href="${BASE_PATH}/index" class="am-btn am-btn-white am-radius" style="padding:0.7em 1.5em;width:49%;">返回首页</a>
		</div>
		
		<!-- include -->
		<%@include file="../include.jsp"%>
		<!-- lib -->
		<script type="text/javascript" src="${BASE_PATH}/static/lib/jquery/jquery-1.11.2.min.js"></script>
		<!-- amazeui -->
		<script type="text/javascript" src="${BASE_PATH}/static/ui/js/amazeui.min.js"></script>
		<!-- lib -->
		<script type="text/javascript" src="${BASE_PATH}/static/lib/layer/layer.js"></script>
		<script type="text/javascript" src="${BASE_PATH}/static/lib/template/template.js"></script>
		<script type="text/javascript" src="${BASE_PATH}/static/lib/jquery-lazyload/jquery.lazyload.min.js"></script>
		<!-- app -->
		<script type="text/javascript" src="${BASE_PATH}/static/app/js/kit.js"></script>
		<script type="text/javascript" src="${BASE_PATH}/static/lib/template/template-ext.js"></script>
		<!-- tpl -->
		<script type="text/html" id="dataTpl">
		<div class="app-result-wrapper">
			<div class="am-text-center">
				<img src="${BASE_PATH}/static/app/img/paysuccess.png">
				<div class="app-m-t-20">感谢您在大实惠购物<br/>欢迎您再次光临</div>
			</div>
			<a href="${BASE_PATH}/order/eval/{{orderNum}}" class="am-center am-btn am-radius am-btn-red am-btn-only-border app-m-t-10">去评价</a>
		</div>
		
		<div class="app-list app-m-t-10">
			<div class="app-list-item">
				<div class="am-u-sm-12 am-u-md-12 am-u-lg-12">订单号：{{orderNum}}</div>
			</div>
			{{each goodsList as goods}}
			<hr/>
			<div class="am-g">
				<div class="am-u-sm-2 am-u-md-3 am-u-lg-3" style="padding:2px;">
					<a href="${BASE_PATH}/goods/detail/{{goods.goodsid}}"><img data-original="${FTP_PATH}{{goods.thumb}}" width="100%" class="lazyload"></a>
				</div>
				<div class="am-u-sm-10 am-u-md-9 am-u-lg-9" style="padding:2px;">
					<div class="am-u-sm-11 am-u-md-11 am-u-lg-11">{{goods.name}}</div>
					<div class="am-u-sm-1 am-u-md-2 am-u-lg-1 ">x {{goods.count}}</div>
					<div class="am-u-sm-3 am-u-md-2 am-u-lg-1">
						<i class="am-icon-rmb"></i> {{goods.price}}
					</div>
					<span class="am-cf"></span>
				</div>
			</div>
			{{/each}}
			<hr/>
			<div class="app-list-item app-m-t-10">
				<div class="am-fr">总计：<i class="am-icon-rmb"></i> {{amount}}</div>
			</div>
		</div>
		</script>
		<script type="text/javascript">
			$(function(){
				//初始化显示订单信息
				Kit.ajax.post("${BASE_PATH}/order/getDetail",{orderNum:'${orderNum}'},function(result){
					if(result.flag==0){
						if(result.object.orderState==<%=OrderCode.OrderState.FINISH%>){
							//渲染订单信息
							$("#panel").empty().append(template("dataTpl",result.object));
							//图片延迟加载
							$("img.lazyload").not(".lazyload-binded").lazyload({
								failurelimit : 100,
								effect : "show"
							}).addClass("lazyload-binded");
							return;
						}
					}else{
						Kit.ui.toast(result.message);
					}
					setTimeout(function(){
						Kit.render.redirect("${BASE_PATH}/index");
					},2000);
				});
			});
		</script>
	</body>
</html>
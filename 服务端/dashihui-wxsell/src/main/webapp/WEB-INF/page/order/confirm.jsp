<%@ page language="java" contentType="text/html; charset=UTF-8" import="cn.com.dashihui.wx.common.OrderCode" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>确认订单</title>
		<!-- amazeui -->
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/ui/css/amazeui.min.css" />
		<!-- app -->
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/app/css/ui.css" />
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/app/css/m_order_confirm.css" />
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/app/css/switch_button.css" />
	</head>
	<body>
		<div id="addressWrapper2" class="app-addr-wrapper" onclick="toChooseAddress()" <c:if test="${addressDef==null}">style="display:none;"</c:if>>
			<div class="am-g">
				<div class="am-u-sm-11 am-u-md-11 am-u-lg-11 app-addr-left">
					<div class="am-text-truncate" id="address">${addressDef.address}</div>
					<div>
						<span id="linkName">${addressDef.linkName}</span><span class="app-m-l-20" id="sex">${addressDef.sex==1?'先生':'女士'}</span><span class="app-m-l-20" id="tel">${addressDef.tel}</span>
					</div>
				</div>
				<div class="am-u-sm-1 am-u-md-1 am-u-lg-1 app-addr-right">
					<i class="am-icon-angle-right"></i>
				</div>
				<div class="am-cf"></div>
			</div>
		</div>
		<div id="addressWrapper1" class="app-part" <c:if test="${addressDef!=null}">style="display:none;"</c:if>>
			<div class="app-part-item">
				<a href="javascript:toChooseAddress();">
					<label>选择收货地址</label>
					<i class="am-icon-angle-right am-fr"></i>
					<span class="am-cf"></span>
				</a>
			</div>
		</div>
		<c:if test="${fn:length(cart2.list) > 0}">
			<div class="app-part app-m-t-10">
				<div class="app-part-item">
					<div onclick="javascript:toChooseStorePayType();">
						<label>支付方式</label>
						<i class="am-icon-angle-down am-fr"></i>
						<span id="storePayType" class="am-fr" lang="<%=OrderCode.OrderPayType.ON_LINE%>">在线支付</span>
						<span class="am-cf"></span>
					</div>
				</div>
				<hr/>
				<div class="app-part-item">
					<div onclick="javascript:toChooseStoreTakeType();">
						<label>配送方式</label>
						<i class="am-icon-angle-down am-fr"></i>
						<span id="storeTakeType" class="am-fr" lang="<%=OrderCode.OrderTakeType.DELIVER%>">送货上门</span>
						<span class="am-cf"></span>
					</div>
				</div>
				<hr/>
				<div class="app-part-item">
					<div class="app_check_box">
						<label class="takeType">${STORE.title}</label>
						<span style="color:red;" class="item-delivery">${STORE.deliverydes}</span>
						<span style="color:red;display:none;" class="am-fr item-time">配送时间：${STORE.beginTime}-${STORE.endTime}</span>
					</div>
				</div>
				<hr/>
				<div class="app-part-item">
					<c:forEach items="${cart2.list}" var="goods" varStatus="status">
						<div class="am-g">
							<div class="am-u-sm-7 am-u-md-7 am-u-lg-9 am-text-left">${goods.name}</div>
							<div class="am-u-sm-2 am-u-md-2 am-u-lg-1 am-text-right">x ${goods.counter}</div>
							<div class="am-u-sm-3 am-u-md-3 am-u-lg-2 am-text-right"><i class="am-icon-rmb"></i> ${goods.total}</div>
						</div>
					</c:forEach>
				</div>
				<hr/>
				<div class="app-part-item am-fr">
					<div>总计：<i class="am-icon-rmb"></i> ${cart2.total }</div>
					<input type="hidden" id="storeAmount" value="${cart1.total }" />
				</div>
				<div class="am-cf"></div>
			</div>
		</c:if>
		<c:if test="${fn:length(cart1.list) > 0}">
			<div class="app-part app-m-t-10">
				<div class="app-part-item">
					<div class="app_check_box">
						<label class="takeType">大实惠直营</label>
						<span style="color:red;">在线支付+第三方配送</span>
					</div>
				</div>
				<hr/>
				<div class="app-part-item">
					<c:forEach items="${cart1.list}" var="goods" varStatus="status">
						<div class="am-g">
							<div class="am-u-sm-7 am-u-md-7 am-u-lg-9 am-text-left">${goods.name}</div>
							<div class="am-u-sm-2 am-u-md-2 am-u-lg-1 am-text-right">x ${goods.counter}</div>
							<div class="am-u-sm-3 am-u-md-3 am-u-lg-2 am-text-right"><i class="am-icon-rmb"></i> ${goods.total}</div>
						</div>
					</c:forEach>
				</div>
				<hr/>
				<div class="app-part-item am-fr">
					<div>总计：<i class="am-icon-rmb"></i> ${cart1.total }</div>
					<input type="hidden" id="selfAmount" value="${cart1.total }" />
				</div>
				<div class="am-cf"></div>
			</div>
		</c:if>
		
		<div class="app-part app-m-t-10" id="chooseRedeem">
			<div class="app-part-item">
				<div class="am-fl">可用${userMoney }实惠币抵用${userMoney }元</div>
				<div class="am-fr">
					<input class="mui-switch mui-switch-anim" type="checkbox" checked/>
				</div>
				<div class="am-cf"></div>
			</div>
		</div>
		
		<div class="app-part app-m-t-10">
			<div class="app-part-item">
				<div onclick="javascript:toEditDesc();" class="app_check_box">
					<label>备注</label>
					<i class="am-icon-angle-right am-fr"></i>
					<span id="describe" class="am-text-sm app-text-gray am-fr am-text-truncate" style="max-width:180px" data-value="">可输入特殊要求（可选）</span>
					<span class="am-cf"></span>
				</div>
			</div>
		</div>
		
		<div class="app-total am-g normalModel">
			<div class="am-fl app-m-l-10">
				<div class="am-fl app-m-l-10 app-amount">总计：<i class="am-icon-rmb"></i> ${cartTotal}</div>
				<div class="am-cf"></div>
			</div>
			<div class="app-submit-wrapper am-fr">
				<a href="javascript:doSubmit();" class="app-submit-btn">提交订单</a>
			</div>
			<div class="am-cf"></div>
		</div>
		
		<!-- include -->
		<%@include file="../include.jsp"%>
		<!-- lib -->
		<script type="text/javascript" src="${BASE_PATH}/static/lib/jquery/jquery-1.11.2.min.js"></script>
		<!-- amazeui -->
		<script type="text/javascript" src="${BASE_PATH}/static/ui/js/amazeui.min.js"></script>
		<!-- lib -->
		<script type="text/javascript" src="${BASE_PATH}/static/lib/layer/layer.js"></script>
		<script type="text/javascript" src="${BASE_PATH}/static/lib/template/template.js"></script>
		<!-- app -->
		<script type="text/javascript" src="${BASE_PATH}/static/app/js/kit.js"></script>
		<script type="text/javascript" src="${BASE_PATH}/static/lib/template/template-ext.js"></script>
		<script type="text/html" id="addressTpl">
		{{each list as address}}
		<div class="am-container" onclick="doChooseAddress({{address.id}})">
			<div class="am-text-truncate" id="address{{address.id}}">{{address.address}}</div>
			<div>
				<span id="linkName{{address.id}}">{{address.linkName}}</span><span class="app-m-l-20" id="sex{{address.id}}">{{address.sex | flagTransform:1,'先生',2,'女士'}}</span><span class="app-m-l-20" id="tel{{address.id}}">{{address.tel}}</span>
			</div>
		</div>
		<hr/>
		{{/each}}
		</script>
		<script type="text/javascript">
			$(function(){
				var cart = '${cart.list}';
				doChooseRedeem();
			})
			function doChooseStorePayType(type){
				if(type == 1) {
					$("#storePayType").attr('lang','1').empty().append('在线支付');
				} else {
					$("#storePayType").attr('lang','2').empty().append('货到付款');
				}
				doChooseRedeem();
				Kit.ui.popdown();
			}
			//1:送货上门；2：门店自取
			function doChooseStoreTakeType(type){
				if(type == 1){
					$("#storeTakeType").attr('lang','1').empty().append('送货上门');
					$(".item-delivery").show();
					$(".item-time").hide();
				} else {
					$("#storeTakeType").attr('lang','2').empty().append('上门自取');
					$(".item-delivery").hide();
					$(".item-time").show();
				}
				Kit.ui.popdown();
			}
			//弹出备注编辑弹出层
			function toEditDesc(){
				var popup = '<div class="desc-dialog">'
						+ '<form class="am-form">'
						+ '<div class="am-form-group">'
						+ '<textarea id="descInpt" rows="4" placeholder="请输入您的要求，仅限100字以内" maxlength="100">'+$("#describe").attr("data-value")+'</textarea>'
						+ '</div>'
						+ '<a href="javascript:doConfirmDesc();" class="am-btn am-btn-danger am-btn-block">确定</a>'
						+ '</form>'
						+ '</div>';
				Kit.ui.popup(popup);
			}
			//选择店铺支付方式
			function toChooseStorePayType(){
				var popup = '<div class="desc-dialog">'
					+ '<form class="am-form">'
					+ '<div class="app-part app-part-popup">'
					+ '<div class="app-part-item" onclick="javascript:doChooseStorePayType(\'1\');">在线支付</div><hr/>'
					+ '<div class="app-part-item" onclick="javascript:doChooseStorePayType(\'2\');">货到付款（仅支持门店）</div>'
					+ '</div>'
					+ '<div class="app-part app-part-popup app-m-t-10">'
					+ '<div class="app-part-item app-part-popup" onclick="javascript:Kit.ui.popdown();">取消</div>'
					+ '</div>'
					+ '</form>'
					+ '</div>';
				Kit.ui.popup(popup);
			}
			//选择店铺取货方式
			function toChooseStoreTakeType(){
				var popup = '<div class="desc-dialog">'
					+ '<form class="am-form">'
					+ '<div class="app-part app-part-popup">'
					+ '<div class="app-part-item" onclick="javascript:doChooseStoreTakeType(\'1\');">送货上门</div><hr/>'
					+ '<div class="app-part-item" onclick="javascript:doChooseStoreTakeType(\'2\');">上门自取（仅支持门店）</div>'
					+ '</div>'
					+ '<div class="app-part app-part-popup app-m-t-10">'
					+ '<div class="app-part-item app-part-popup" onclick="javascript:Kit.ui.popdown();">取消</div>'
					+ '</div>'
					+ '</form>'
					+ '</div>';
				Kit.ui.popup(popup);
			}
			//确认备注内容
			function doConfirmDesc(){
				var descInpt = $("#descInpt").val();
				if(!Kit.validate.isBlank(descInpt)){
					$("#describe").text(descInpt);
				}
				$("#describe").attr("data-value", descInpt);
				Kit.ui.popdown();
			}
			//弹出收货地址选择列表弹出层
			function toChooseAddress(){
				Kit.ajax.get("${BASE_PATH}/my/addressList",function(result){
					if(result.flag==0){
						var list = result.object;
						if(list.length>0){
							var popup = '<div class="app-addr-wrapper">'+template("addressTpl",{"list":result.object})+'</div><a href="javascript:Kit.ui.popdown();" class="am-btn am-btn-danger am-btn-block">取消</a>';
							Kit.ui.popup(popup,"position:fixed; bottom:0; left:0; width:100%; padding:0; border:none; padding:5px; max-height:60%;");
						}else{
							Kit.ui.alert("还没有收货地址，现在去设置！",function(){
								Kit.render.redirect("${BASE_PATH}/my/address?from=1");
							});
						}
					}
				});
			}
			//选择收货地址内容
			function doChooseAddress(addressid){
				$("#address").text($("#address"+addressid).text());
				$("#linkName").text($("#linkName"+addressid).text());
				$("#sex").text($("#sex"+addressid).text());
				$("#tel").text($("#tel"+addressid).text());
				Kit.ui.toggle("#addressWrapper2","#addressWrapper1");
				Kit.ui.popdown();
			}
			//提交订单
			function doSubmit(){
				var isRedeem = 0;
				if($("input[type=checkbox]").is(':checked')){
					isRedeem = 1;
				}
				var storePayType = $("#storePayType").attr('lang');
				var storeTakeType = $("#storeTakeType").attr('lang');
				var selfPayType = 1;
				var selfTakeType = 1;
				var linkName = $("#linkName").text();
				var sex = $("#sex").text();
				var tel = $("#tel").text();
				var address = $("#address").text();
				var describe = $("#describe").attr("data-value");
				var params = {choosedGoodsid:"${choosedGoodsid}",storePayType:storePayType,storeTakeType:storeTakeType,selfPayType:selfPayType,selfTakeType:selfTakeType,
						linkName:linkName,sex:sex,tel:tel,address:address,describe:describe,isRedeem:isRedeem};
				var selfAmount = $("#selfAmount").val();
				if(Kit.validate.isBlank(linkName)||Kit.validate.isBlank(sex)||Kit.validate.isBlank(tel)||Kit.validate.isBlank(address)){
					Kit.ui.alert("请选择收货地址");
				}else{
					Kit.ajax.post("${BASE_PATH}/order/save",params,function(result){
						if(result.flag==-1){
							Kit.ui.alert(result.message);
						}else if(result.flag==1){
							Kit.ui.alert("购物车已经结算过了，去订单列表？",function(){
								Kit.render.redirect("${BASE_PATH}/order/list");
							});
						}else if(result.flag==0){
							if(result.object.forwardFlag == 1){
								Kit.render.redirect("${BASE_PATH}/order/prepay/"+result.object.orderNum);
							} else {
								Kit.ui.toast("订单提交成功！");
								setTimeout(function(){
									//货到付款时，跳转到订单全部页面
									Kit.render.redirect("${BASE_PATH}/order/list");
								},2000);
							}
						}
					});
				}
			}
			function doChooseRedeem(){
				var storePayType = $("#storePayType").attr('lang');
				var cart1_counter = '${cart1.counter}';
				if(cart1_counter > 0 || storePayType == 1){
					$("input[type=checkbox]").attr("checked");
					$("#chooseRedeem").show();
				} else {
					$("input[type=checkbox]").removeAttr("checked");
					$("#chooseRedeem").hide();
				}
			}
		</script>
	</body>
</html>
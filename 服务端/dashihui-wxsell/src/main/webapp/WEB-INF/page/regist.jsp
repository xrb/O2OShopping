<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>注册</title>
		<!-- amazeui -->
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/ui/css/amazeui.min.css" />
		<!-- app -->
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/app/css/ui.css" />
		<style type="text/css">
			.app-logo{
				margin-top:30px;
				margin-bottom:30px;
			}
			.app-btn-link{
				color:#555;
				font-size:12px;
			}
			.app-info{
				font-size:12px;
				color:#888;
			}
		</style>
	</head>
	<body>
		<img src="${BASE_PATH}/static/app/img/logo.png" class="am-center app-logo" width="170">
		<div class="am-g app-m-t-10">
			<div class="am-u-md-8 am-u-sm-centered">
				<div class="am-fl app-btn-link"><a href="${BASE_PATH}/login">已有帐号登录</a></div>
				<div class="am-cf"></div>
				<form class="am-form">
					<div class="am-form-group am-form-icon">
						<i class="am-icon-mobile"></i>
						<input id="phoneInpt" type="text" class="am-form-field" placeholder="手机号" maxlength="11">
					</div>
					<div class="am-form-group am-form-icon">
						<i class="am-icon-lock"></i>
						<input id="passwordInpt" type="password" class="am-form-field" placeholder="密码" maxlength="18">
					</div>
					<div class="am-form-group am-form-icon">
						<i class="am-icon-lock"></i>
						<input id="passwordConfirmInpt" type="password" class="am-form-field" placeholder="确认密码" maxlength="18">
					</div>
					<div class="am-form-group am-form-icon">
						<i class="am-icon-lock"></i>
						<input id="inviteCode" class="am-form-field" placeholder="邀请码" maxlength="6" value="${inviteCode}">
					</div>
					<div class="am-g">
						<div class="am-form-group am-u-sm-7">
							<input id="codeInpt" type="text" class="am-form-field" placeholder="验证码" maxlength="4">
						</div>
						<div class="am-u-sm-5">
							<input id="sendCode" type="button" class="am-btn am-btn-success" style="width:100%;" value="获取验证码" onclick="doSendCode()">
							<input id="sendedCode" type="button" class="am-btn am-btn-success" style="width:100%;display:none;" value="" disabled="disabled">
						</div>
					</div>
					<input type="button" class="am-btn am-btn-danger am-btn-block" value="注册并绑定" onclick="doRegist()">
				</form>
				<span class="app-info">温馨提示：<br/>密码要求数字、字母、下划线组合，长度保持在6-18位之间</span>
			</div>
		</div>

		<!-- include -->
		<%@include file="include.jsp"%>
		<!-- lib -->
		<script type="text/javascript" src="${BASE_PATH}/static/lib/jquery/jquery-1.11.2.min.js"></script>
		<!-- amazeui -->
		<script type="text/javascript" src="${BASE_PATH}/static/ui/js/amazeui.min.js"></script>
		<!-- lib -->
		<script type="text/javascript" src="${BASE_PATH}/static/lib/layer/layer.js"></script>
		<!-- app -->
		<script type="text/javascript" src="${BASE_PATH}/static/app/js/kit.js"></script>
		<script type="text/javascript">
			var counterNum = 60,counter;
			function doSendCode(){
				var phone = $("#phoneInpt").val();
				Kit.ajax.post("${BASE_PATH}/sendRegCode",{phone:phone},function(result){
					if(result.flag==1){
						Kit.ui.alert("手机号不能为空！");
					}else if(result.flag==2){
						Kit.ui.alert("手机号已经注册！");
					}else if(result.flag==0){
						Kit.ui.toast("发送成功！");
						Kit.ui.toggle("#sendedCode","#sendCode");
						counter = setInterval(function(){
							if(counterNum>0){
								$("#sendedCode").val((counterNum--)+"秒后获取");
							}else{
								clearInterval(counter);
								counterNum = 60;
								Kit.ui.toggle("#sendCode","#sendedCode");
							}
						}, 1000);
					}
				});
			}
			function doRegist(){
				var phone = $("#phoneInpt").val();
				var password = $("#passwordInpt").val();
				var passwordConfirm = $("#passwordConfirmInpt").val();
				var code = $("#codeInpt").val();
				Kit.ajax.post("${BASE_PATH}/doRegist",{phone:phone,password:password,passwordConfirm:passwordConfirm,code:code},function(result){
					if(result.flag==1){
						Kit.ui.alert("手机号不能为空！");
					}else if(result.flag==2){
						Kit.ui.alert("手机号不正确！");
					}else if(result.flag==3){
						Kit.ui.alert("手机号已经注册！");
					}else if(result.flag==4){
						Kit.ui.alert("密码不能为空！");
					}else if(result.flag==5){
						Kit.ui.alert("密码只能填写数字、字母、下划线组合！");
					}else if(result.flag==6){
						Kit.ui.alert("密码输入过短！");
					}else if(result.flag==7){
						Kit.ui.alert("确认密码不能为空！");
					}else if(result.flag==8){
						Kit.ui.alert("再次密码输入不一致！");
					}else if(result.flag==9){
						Kit.ui.alert("验证码不能为空！");
					}else if(result.flag==10){
						Kit.ui.alert("请重新获取验证码！");
					}else if(result.flag==11){
						Kit.ui.alert("验证码错误！");
					}else if(result.flag==0){
						Kit.ui.toast("注册成功！");
						setTimeout(function(){
							Kit.render.redirect("${BASE_PATH}/login");
						}, 1000);
					}
				});
			}
		</script>
	</body>
</html>
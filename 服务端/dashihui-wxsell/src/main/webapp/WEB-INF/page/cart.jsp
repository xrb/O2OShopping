<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>购物车</title>
		<!-- amazeui -->
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/ui/css/amazeui.min.css" />
		<!-- app -->
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/app/css/ui.css" />
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/app/css/m_cart.css" />
	</head>
	<body style="padding-bottom:100px;">
		<header class="am-header am-header-default app-header" data-am-sticky>
			<h1 class="am-header-title">购物车</h1>
			<div class="am-header-right am-header-nav notEmptyPart" style="display:none;">
				<a href="javascript:toEditModel();" class="normalModel"><i class="am-icon-edit"></i></a>
				<a href="javascript:doComplete();" class="editModel app-hide"><i class="am-icon-check"></i></a>
			</div>
		</header>
		
		<div class="emptyPart" style="display:none;">
			<div>
				<img src="${BASE_PATH}/static/app/img/shopping_cart.png" style="margin:0 auto;display:block;" width="140">
				<div class="am-text-center app-m-t-10">购物车空空如也，赶紧装满~</div>
				<a href="${BASE_PATH}/index" class="am-center am-radius am-btn-red-border app-m-t-10" style="width:120px;">立即逛逛</a>
			</div>
		</div>
		<div id="panel" class="notEmptyPart" style="display:none;"></div>
		
		<div class="am-navbar am-cf am-no-layout app-navbar">
			<ul class="am-navbar-nav am-cf am-avg-sm-3">
				<li>
					<a href="${BASE_PATH }/index">
						<img src="${BASE_PATH}/static/app/img/menu_bot02.png"/>
						<span class="am-navbar-label">便利店</span>
					</a>
				</li>
				<li class="app-navbar-cart">
					<a href="javascript:void(0);" class="app-active">
						<img src="${BASE_PATH}/static/app/img/menu_bot23.png"/>
						<span class="am-navbar-label">购物车</span>
					</a>
					<span id="counterAll" class="am-badge am-round" style="display:none;"></span>
				</li>
				<li>
					<a href="${BASE_PATH}/my">
						<img src="${BASE_PATH}/static/app/img/menu_bot05.png"/>
						<span class="am-navbar-label">我的</span>
					</a>
				</li>
			</ul>
		</div>
		
		<!-- include -->
		<%@include file="include.jsp"%>
		<!-- lib -->
		<script type="text/javascript" src="${BASE_PATH}/static/lib/jquery/jquery-1.11.2.min.js"></script>
		<!-- amazeui -->
		<script type="text/javascript" src="${BASE_PATH}/static/ui/js/amazeui.min.js"></script>
		<!-- lib -->
		<script type="text/javascript" src="${BASE_PATH}/static/lib/layer/layer.js"></script>
		<script type="text/javascript" src="${BASE_PATH}/static/lib/template/template.js"></script>
		<script type="text/javascript" src="${BASE_PATH}/static/lib/jquery-lazyload/jquery.lazyload.min.js"></script>
		<!-- app -->
		<script type="text/javascript" src="${BASE_PATH}/static/app/js/kit.js"></script>
		<script type="text/javascript" src="${BASE_PATH}/static/lib/template/template-ext.js"></script>
		<script type="text/javascript">
			$(function(){
				Kit.ajax.post("${BASE_PATH}/cart/detail",{},function(result){
					var cart1 = result.object.cart1;
					var cart2 = result.object.cart2;
					if(cart1.list.length!=0||cart2.list.length!=0){
						$("#panel").append(template("dataTpl",{"cartList":result.object}));
						reCalcTotal();
						//图片延迟加载
						$("img.lazyload","#panel").not(".lazyload-binded").lazyload().addClass("lazyload-binded");
						Kit.ui.toggle(".notEmptyPart",".emptyPart");
						//显示大菜单中“购物车”右上角的角标
						if(cart1.counter!=0||cart2.counter!=0){
							$("#counterAll").text(cart1.counter+cart2.counter).show();
						}
						$(window).trigger("scroll");
					}else{
						Kit.ui.toggle(".emptyPart",".notEmptyPart");
					}
				});
			});
			//切换为编辑模式
			function toEditModel(){
				Kit.ui.toggle(".editModel",".normalModel");
			}
			//完成编辑
			function doComplete(){
				Kit.ui.toggle(".normalModel",".editModel");
			}
			//单选选中或取消
			function doCheck(goodsid,suffix,count,price){
				var $ck = $("#check"+suffix+"_"+goodsid), isChecked = $ck.hasClass("checked"), $all = $("#checkAll"+suffix);
				if(isChecked){
					$ck.removeClass("checked");
					$all.removeClass("checked");
				}else{
					$ck.addClass("checked");
					if($(".check"+suffix).not(".checked").length==0){
						$all.addClass("checked");
					}
				}
				reCalcTotal();
			}
			//选中所有
			function doCheckAll(suffix){
				var $all = $("#checkAll"+suffix), isCheckedAll = $all.hasClass("checked");
				if(isCheckedAll){
					$(".check"+suffix+",#checkAll"+suffix).removeClass("checked");
				}else{
					$(".check"+suffix+",#checkAll"+suffix).addClass("checked");
				}
				reCalcTotal();
			}
			//删除选中的商品
			function doDelAll(){
				var $all = $(".checkItem1.checked");
				if($all.length!=0){
					Kit.ui.confirm("确认要删除这"+$all.length+"种商品吗？",function(){
						var goodsids = new Array();
						$all.each(function(index,item){
							goodsids.push($(item).data("goodsid"));
						});
						//移除选中的商品
						Kit.ajax.post("${BASE_PATH}/cart/delete",{goodsid:goodsids.join(",")},function(result){
							if(result.flag==0){
								$.each(goodsids,function(index,goodsid){
									$("#item"+goodsid).remove();
								});
								if($(".app-cart-item").length!=0){
									reCalcTotal();
									//判断两个购物车版块（自营或店铺版块）是否还有其他商品，如果没有了，则移除
									$(".cart").each(function(index,cart){
										if($(".app-cart-item",cart).length==0){
											$(cart).remove();
										}
									});
								}else{
									//购物车中没有其他商品时，刷新页面
									Kit.render.refresh();
								}
							}
						});
					});
				}
			}
			//减少数量
			function doReduce(goodsid){
				var counterEl = $("#counter"+goodsid), counterNum = parseInt(counterEl.text());
				if(counterNum>1){
					Kit.ajax.post("${BASE_PATH}/cart/update",{goodsid:goodsid,operate:0,num:1},function(result){
						//修改商品数量
						counterEl.text(counterNum-1);
						reCalcTotal();
					});
				}else{
					Kit.ui.confirm("从购物车中移除该商品？",function(){
						Kit.ajax.post("${BASE_PATH}/cart/update",{goodsid:goodsid,operate:0,num:1},function(result){
							if(result.flag==0){
								//移除商品
								$("#item"+goodsid).remove();
								//判断两个购物车版块（自营或店铺版块）是否还有其他商品，如果没有了，则移除
								$(".cart").each(function(index,cart){
									cart = $(cart);
									if(cart.find(".app-cart-item").length==0){
										cart.remove();
									}
								});
								//修改购物车商品数量和总金额
								if($(".app-cart-item").length!=0){
									reCalcTotal();
								}else{
									//购物车中没有其他商品时，刷新页面
									Kit.render.refresh();
								}
							}
						});
					});
				}
			}
			//增加数量
			function doAdd(goodsid){
				var counterEl = $("#counter"+goodsid), counterNum = parseInt(counterEl.text());
				Kit.ajax.post("${BASE_PATH}/cart/update",{goodsid:goodsid,operate:1,num:1},function(result){
					//修改商品数量
					counterEl.text(counterNum+1);
					reCalcTotal();
				});
			}
			//重新计算当前应付价格和商品数量
			function reCalcTotal(){
				var goodsTotal = 0, goodsCounter = 0;
				$(".app-cart-item").each(function(index,goods){
					var check = $(".checkItem2",goods), price = parseFloat($(".price",goods).text().trim()), counter = parseInt($(".counter",goods).text());
					if(check.hasClass("checked")){
						goodsTotal = goodsTotal + price*100*counter;
						goodsCounter = goodsCounter + counter;
					}
				});
				$("#cartCounterNum").text(goodsCounter);
				$("#cartTotal").text(goodsTotal/100+"");
				if(goodsCounter>0){
					$("#confirmBtn").removeClass("disabled");
				}else{
					$("#confirmBtn").addClass("disabled");
				}
				//修改底部购物车商品数量
				if(goodsCounter!=0){
					$("#counterAll").text(goodsCounter);
				}else{
					$("#counterAll").text("").hide();
				}
			}
			function toConfirm(){
				var temp = new Array();
				$(".checkItem2.checked").each(function(index,goods){
					temp.push($(goods).data("goodsid"));
				});
				if(temp.length>0){
					Kit.render.redirect("${BASE_PATH}/order/confirm?choosedGoodsid="+temp.join(","));
				}
			}
		</script>
		<script type="text/html" id="dataTpl">
			{{if cartList.cart1.list.length!=0}}
			<div class="cart">
			<div class="app-p-10">大实惠直营</div>
			<div class="app-cart-list">
				{{each cartList.cart1.list as goods index}}
				<div id="item{{goods.id}}" class="app-cart-item am-g">
					<div class="am-u-sm-1 am-u-md-2 am-u-lg-2 editModel app-hide" style="padding-top:9%;">
						<a id="checkItem1_{{goods.id}}" href="javascript:doCheck({{goods.id}},'Item1');" data-goodsid="{{goods.id}}" class="editModel checkbox checkItem1"></a>
					</div>
					<div class="am-u-sm-1 am-u-md-2 am-u-lg-2 normalModel" style="padding-top:9%;">
						<a id="checkItem2_{{goods.id}}" href="javascript:doCheck({{goods.id}},'Item2',{{goods.counter}},{{goods.sellPrice}});" data-goodsid="{{goods.id}}" class="normalModel checkbox checkItem2 checked"></a>
					</div>
					<div class="am-u-sm-4 am-u-md-4 am-u-lg-4">
						<a href="${BASE_PATH }/goods/detail/{{goods.id}}">
							<img data-original="${FTP_PATH}{{goods.thumb}}" class="am-img-thumbnail lazyload" width="100%">
						</a>
					</div>
					<div class="am-u-sm-7 am-u-md-6 am-u-lg-6">
						<div class="app-cart-item-title">
							<a href="${BASE_PATH }/goods/detail/{{goods.id}}">
								{{goods.name}}
							</a>
						</div>
						<div class="app-cart-item-spec">{{goods.spec}}</div>
						<div>
							<div class="app-cart-item-price am-fl am-vertical-align">
								<span class="am-vertical-align-bottom price"><i class="am-icon-rmb"></i> {{goods.sellPrice}}</span>
							</div>
							<div class="app-cart-item-btn am-fr">
								<a class="app-btn app-btn-minus am-fl" href="javascript:doReduce({{goods.id}});"><i class="am-icon-minus"></i></a>
								<span id="counter{{goods.id}}" class="app-num am-fl counter">{{goods.counter}}</span>
								<a class="app-btn app-btn-plus am-fl" href="javascript:doAdd({{goods.id}});"><i class="am-icon-plus"></i></a>
								<div class="am-cf"></div>
							</div>
							<div class="am-cf"></div>
						</div>
					</div>
					<div class="am-cf"></div>
					<hr color="red"/>
				</div>
				{{/each}}
			</div>
			{{/if}}
			</div>
			<div class="cart">
			{{if cartList.cart2.list.length!=0}}
			<div class="app-p-10">${currentStore.title}</div>
			<div class="app-cart-list">
				{{each cartList.cart2.list as goods index}}
				<div id="item{{goods.id}}" class="app-cart-item am-g">
					<div class="am-u-sm-1 am-u-md-2 am-u-lg-2 editModel app-hide" style="padding-top:9%;">
						<a id="checkItem1_{{goods.id}}" href="javascript:doCheck({{goods.id}},'Item1');" data-goodsid="{{goods.id}}" class="editModel checkbox checkItem1"></a>
					</div>
					<div class="am-u-sm-1 am-u-md-2 am-u-lg-2 normalModel" style="padding-top:9%;">
						<a id="checkItem2_{{goods.id}}" href="javascript:doCheck({{goods.id}},'Item2',{{goods.counter}},{{goods.sellPrice}});" data-goodsid="{{goods.id}}" class="normalModel checkbox checkItem2 checked"></a>
					</div>
					<div class="am-u-sm-4 am-u-md-4 am-u-lg-4">
						<a href="${BASE_PATH }/goods/detail/{{goods.id}}">
							<img data-original="${FTP_PATH}{{goods.thumb}}" class="am-img-thumbnail lazyload" width="100%">
						</a>
					</div>
					<div class="am-u-sm-7 am-u-md-6 am-u-lg-6">
						<div class="app-cart-item-title">
							<a href="${BASE_PATH }/goods/detail/{{goods.id}}">
								{{goods.name}}
							</a>
						</div>
						<div class="app-cart-item-spec">{{goods.spec}}</div>
						<div>
							<div class="app-cart-item-price am-fl am-vertical-align">
								<span class="am-vertical-align-bottom price"><i class="am-icon-rmb"></i> {{goods.sellPrice}}</span>
							</div>
							<div class="app-cart-item-btn am-fr">
								<a class="app-btn app-btn-minus am-fl" href="javascript:doReduce({{goods.id}});"><i class="am-icon-minus"></i></a>
								<span id="counter{{goods.id}}" class="app-num am-fl counter">{{goods.counter}}</span>
								<a class="app-btn app-btn-plus am-fl" href="javascript:doAdd({{goods.id}});"><i class="am-icon-plus"></i></a>
								<div class="am-cf"></div>
							</div>
							<div class="am-cf"></div>
						</div>
					</div>
					<div class="am-cf"></div>
					<hr color="red"/>
				</div>
				{{/each}}
			</div>
			{{/if}}
			</div>
			<div class="app-total am-g normalModel">
				<div class="am-u-sm-1 am-u-md-2 am-u-lg-2" style="padding-top:15px;">
					<a id="checkAllItem2" href="javascript:doCheckAll('Item2');" class="checkbox checked"></a>
				</div>
				<div class="am-fl app-m-l-10">
					<span class="am-fl">全选</span>
					<div class="am-cf"></div>
				</div>
				<div class="am-fl app-m-l-10">
					<div class="am-fl app-m-l-10 app-amount">总计：<i class="am-icon-rmb"></i> <span id="cartTotal">{{cartList.total}}</span></div>
					<div class="am-cf"></div>
				</div>
				<div class="app-pay-wrapper am-fr">
					<a id="confirmBtn" href="javascript:toConfirm();" class="app-pay-btn">去结算 <small id="cartCounter">(<span id="cartCounterNum">{{cartList.counter}}</span>)</small></a>
				</div>
				<div class="am-cf"></div>
			</div>
			
			<div class="app-total am-g editModel app-hide">
				<div class="am-u-sm-1 am-u-md-2 am-u-lg-2" style="padding-top:15px;">
					<a id="checkAllItem1" href="javascript:doCheckAll('Item1');" class="checkbox"></a>
				</div>
				<div class="am-fl app-m-l-10">
					<span class="am-fl">全选</span>
					<div class="am-cf"></div>
				</div>
				<div class="app-del-wrapper am-fr">
					<a href="javascript:doDelAll()" class="app-del-btn">删除</a>
				</div>
				<div class="am-cf"></div>
			</div>
		</script>
	</body>
</html>
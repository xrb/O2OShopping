<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
		<title>${STORE.title}</title>
		<!-- amazeui -->
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/ui/css/amazeui.min.css" />
		<!-- app -->
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/lib/swiper/css/swiper.min.css" />
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/app/css/ui.css" />
		<link rel="stylesheet" type="text/css" href="${BASE_PATH}/static/app/css/m_search_index.css" />
	</head>
	<body style="position: relative;">
		<header id="top" class="am-header app-header" style="padding-top:6px;">
			<div class="am-g app-top-search-bar" style="margin:0 auto;">
				<input id="searchInpt" type="text" class="am-fl am-u-sm-10 am-u-md-11 am-u-lg-11 app-top-search-inpt" value="${keyword}">
				<div id="searchBtn" class="am-fl am-u-sm-2 am-u-md-1 am-u-lg-1 app-top-search-btn"><span class="am-icon-search"></span></div>
				<div class="am-cf"></div>
  			</div>
		</header>
		<div id="searchKeyword">
			<div class="app-keywords-title">
				<span>热搜关键字：</span>
			</div>
			<c:if test="${keywords!=null&&fn:length(keywords)!=0}">
			<div class="app-keywords-list">
				<c:forEach items="${keywords}" var="keyword">
				<a class="app-keyword-item am-fl" href="javascript:doSearch('${keyword.id}','${keyword.keyword}');"><span>${keyword.keyword}</span></a>
				</c:forEach>
				<div class="am-cf"></div>
			</div>
			</c:if>
		</div>
		
		<div data-am-sticky id="tab">
			<div style="position:relative;">
				<div class="app-tab">
					<div class="app-tab-item am-fl" style="width:30%;">
						<a href="javascript:sortByDefault();" class="rdriver" style="">
							<span id="app-defalult-title" style="color:red;color:red;font-weight:bold;">综合</span>
						</a>
					</div>
					<div class="app-tab-item am-fl" style="width:40%;">
						<a href="javascript:void(0);" class="rdriver" onclick="javascript:filterBySelf();">
							<span id="app-filter-self-title">大实惠直营</span>
							<img id="app-filter-self-false-icon" src="${BASE_PATH}/static/app/img/check_false_square.png" height="15" style="margin-top:-4px;">
							<img id="app-filter-self-true-icon" src="${BASE_PATH}/static/app/img/check_true_square.png" height="15" style="margin-top:-4px;display:none;">
						</a>
					</div>
					<div class="app-tab-item am-fl" style="width:30%;">
						<a href="javascript:void(0);" onclick="javascript:sortByPrice();">
							<span id="app-sort-price-title">价格</span>
							<i id="app-sort-price-icon"></i>
						</a>
					</div>
					<div class="am-cf"></div>
				</div>
				<div id="categoryPop" class="app-category-wrapper" style="display:none;">
					<div class="am-g app-category">
						<div class="am-u-sm-6 am-u-md-6 am-u-lg-6">
							<ul class="app-category-one">
								<c:forEach items="${categoryList}" var="one">
								<li <c:if test="${code==one.code}">class="active"</c:if>>
									<a href="javascript:void(0)" data-code="${one.code}">
										<img src="${BASE_PATH}/static/app/img/category_${one.code}.png" width="20">
										<span>${one.name}</span>
										<i class="am-icon-angle-right am-fr"></i>
									</a>
								</li>
								</c:forEach>
							</ul>
						</div>
						<div class="am-u-sm-6 am-u-md-6 am-u-lg-6">
							<c:forEach items="${categoryList}" var="one">
							<ul class="app-category-two" data-parent-code="${one.code}" <c:if test="${code!=one.code}">style="display:none;"</c:if>>
								<li <c:if test="${code==one.code}">class="active"</c:if>>
									<a href="javascript:void(0)" data-code="${one.code}" data-title="${one.name}">
										<span>全部</span>
										<span class="app-category-count am-fr">${one.goodscount}</span>
									</a>
								</li>
								<c:forEach items="${one.children}" var="two">
								<li>
									<a href="javascript:void(0)" data-code="${two.code}" data-title="${two.name}">
										<span>${two.name}</span>
										<span class="app-category-count am-fr">${two.goodscount}</span>
									</a>
								</li>
								</c:forEach>
							</ul>
							</c:forEach>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	    <div id="searchResult" style="display:none;">
		    <!-- 搜索结果 -->
		    <div class="app-part app-part-flow">
				<div id="goodsList" class="am-g app-goods-list"></div>
			</div>
			
			<!-- 购物车+1的动画效果，需要app-navbar设置z-index小于本控件 -->
			<span id="cartNumAnimate" style="color:red;font-weight:bold;font-size:18px;position:fixed;left:7%;bottom:50px;z-index:2;display:none;">+1</span>
			
			<div class="app-cart" >
				<span id="cartNum" class="am-badge am-round" style="display:none;"></span>
				<a href="${BASE_PATH }/cart"><img src="${BASE_PATH}/static/app/img/breakfast_bottom_left.png" style="width:50px;height:50px;"/></a>
			</div>
			
			<div data-am-widget="gotop" class="am-gotop am-gotop-fixed" >
				<a href="#top"><span class="am-gotop-title">回到顶部</span><i class="am-gotop-icon am-icon-chevron-up"></i></a>
			</div>
	    </div>
	    
	    <div class="emptyPart" style="display: none;">
			<div>
				<div>
					<img src="${BASE_PATH}/static/app/img/search_icon_ineffective.png" style="margin:0 auto;display:block;" width="140">
					<div class="am-text-center app-m-t-10 app-text-weight">没有搜索结果</div>
					<div class="am-text-center">换个关键词试试吧</div>
				</div>
			</div>
		</div>
				
		<!-- include -->
		<%@include file="../include.jsp"%>
		<!-- lib -->
		<script type="text/javascript" src="${BASE_PATH}/static/lib/jquery/jquery-1.11.2.min.js"></script>
		<!-- amazeui -->
		<script type="text/javascript" src="${BASE_PATH}/static/ui/js/amazeui.min.js"></script>
		<!-- lib -->
		<script type="text/javascript" src="${BASE_PATH}/static/lib/jquery-lazyload/jquery.lazyload.min.js"></script>
		<script type="text/javascript" src="${BASE_PATH}/static/lib/jquery-marquee/jquery.marquee.min.js"></script>
		<script type="text/javascript" src="${BASE_PATH}/static/lib/layer/layer.js"></script>
		<script type="text/javascript" src="${BASE_PATH}/static/lib/template/template.js"></script>
		<!-- app -->
		<script type="text/javascript" src="${BASE_PATH}/static/app/js/kit.js"></script>
		<!-- tpl -->
		<script type="text/html" id="dataTpl">
			{{each list as goods}}
			<div class="am-u-sm-6 am-u-md-4 am-u-lg-3 app-goods-item">
            	<dl>
            		<dt><a href="${BASE_PATH}/goods/detail/{{goods.id}}"><img data-original="${FTP_PATH}{{goods.thumb}}" class="am-center lazyload" width="100%"></a></dt>
            		{{if goods.isSelf==1}}
            		<dd class="app-item-title">
						<img src="${BASE_PATH}/static/app/img/goods_self.png" height="13" style="margin-top:-3px;"/>{{goods.name}}
					</dd>
					{{else}}
					<dd class="app-item-title">{{goods.name}}</dd>
					{{/if}}
            		<dd>
            			<span class="app-item-price-normal"><i class="am-icon-rmb"></i> {{goods.sellPrice}}</span>
            			<span class="app-item-price-del"><i class="am-icon-rmb"></i> {{goods.marketPrice}}</span>
						<span>
							<a onclick="javascript:addCart('{{goods.id}}','{{goods.isSelf}}','{{goods.name}}','{{goods.spec}}','{{goods.thumb}}','{{goods.sellPrice}}','{{goods.marketPrice}}');">
								<img src="${BASE_PATH }/static/app/img/add_car.png" style="width:30px;heigt:30px;padding-top:6px;"/>
							</a>
						</span>
            		</dd>
            	</dl>
            </div>
			{{/each}}
		</script>
		<script type="text/javascript">
			var loading = false;
			var pageNum = 0,totalPage = 1, keyword, keywordid, isSelf = 0, orderBy = 1;
			$(function(){
				$("#searchBtn").on("click",function(){
					keyword = $("#searchInpt").val();
					if(Kit.validate.isBlank(keyword)){
						Kit.ui.toast("请输入关键字");
					}else{
						keywordid = "";
						loadInit();
					}
				});
				if(!Kit.validate.isBlank('${keyword}')) {
					keyword = '${keyword}';
					loadInit();
				}
				
				//查询购物车状态
				Kit.ajax.post("${BASE_PATH}/cart/state",{},function(result){
					if(result.object.CART_COUNTER!=0)
						$("#cartNum").text(result.object.CART_COUNTER).show();
				});
			});
			function doSearch(id,text){
				keywrodid = id;keyword = text;
				loadInit();
			}
			function loadInit(){
				Kit.ui.toggle("#searchResult","#searchKeyword");
				pageNum = 0;totalPage = 1;
				$("#goodsList").empty();
				loadBind();
			}
			function loadBind() {
				//绑定“加载中”进入加载事件
				Kit.util.onPageEnd(function(){
	               	if(pageNum < totalPage && !loading){
	               		loading = true;
	               		$("#goodsList").append("<div class=\"app-loading\">正在加载</div>");
	                	Kit.ajax.post("${BASE_PATH}/search/page",{pageNum:pageNum+1,pageSize:10,keywrodid:keywordid,keyword:keyword,isSelf:isSelf,orderBy:orderBy},function(result){
							$("#goodsList").append(template("dataTpl",result.object));
							//amazeui要求在最后一个元素上添加am-u-end样式，否则默认为右浮动
							$(".app-goods-item","#goodsList").removeClass("am-u-end").last().addClass("am-u-end");
							//图片延迟加载
							$("img.lazyload","#goodsList").not(".lazyload-binded").lazyload({
								failurelimit : 100,
								effect : "show"
							}).addClass("lazyload-binded");
							$(".app-loading","#goodsList").remove();
							pageNum = result.object.pageNumber;
							totalPage = result.object.totalPage;
							if(totalPage == 0){
								$(".emptyPart").show();
								$("#searchResult").hide();
								$("#tab").hide();
								$(".am-sticky-placeholder").hide();
							} else {
								$(".emptyPart").hide();
								$("#searchResult").show();
								$("#tab").show();
								$(".am-sticky-placeholder").show();
							}
							//重置加载标识
							loading = false;
						});
	               	}
				});
			}
			function addCart(id,isSelf,name,spec,thumb,sellPrice,marketPrice){
				var goods = {
					"id":id,
					"isSelf":isSelf,
					"name":name,
					"spec":spec,
					"thumb":thumb,
					"sellPrice":sellPrice,
					"marketPrice":marketPrice
				};
				Kit.ajax.post("${BASE_PATH}/cart/add",goods,function(result){
					$("#cartNum").show().text(result.object.counter);
					$("#cartNumAnimate").show().animate({bottom:70,opacity:0},500,"linear",function(){
						$("#cartNumAnimate").css({"bottom":50+"px","opacity":150}).hide();
					});
				});
			}
			
			//按自营搜索
			function filterBySelf() {
				var title = $("#app-filter-self-title");
				var checkboxTrue = $("#app-filter-self-true-icon");
				var checkboxFalse = $("#app-filter-self-false-icon");
				if(checkboxTrue.is(":hidden")){
					isSelf = 1;
					title.attr("style", "color:red;color:red;font-weight:bold;");
					checkboxTrue.show();
					checkboxFalse.hide();
				}else{
					isSelf = 0;
					title.removeAttr("style");
					checkboxFalse.show();
					checkboxTrue.hide();
				}
				//初始化滚动加载分页
				loadInit();
			}
			//默认排序
			function sortByDefault(){
				$("#app-sort-price-title").removeAttr("style");
				$("#app-sort-price-icon").removeAttr("style");
				$("#app-sort-price-icon").removeAttr("class");
				$("#app-defalult-title").attr("style", "color:red;color:red;font-weight:bold;");
				orderBy = 1;
				//初始化滚动加载分页
				loadInit();
			}
			//按照价格排序
			function sortByPrice() {
				if(totalPage == 0){
					totalPage = 1;
				}
				$("#app-sort-price-title").attr("style", "color:red;color:red;font-weight:bold;");
				$("#app-sort-price-icon").attr("style", "color:red;color:red;font-weight:bold;");
				$("#app-defalult-title").removeAttr("style");
				if(orderBy == 0 || orderBy ==1 || orderBy == 3) {
					orderBy = 2;//价格从低到高排序
					$("#app-sort-price-icon").remove("class");
					$("#app-sort-price-icon").attr("class","am-icon-angle-up");
				} else if(orderBy == 2) {
					orderBy = 3;//价格从高到低排序
					$("#app-sort-price-icon").remove("class");
					$("#app-sort-price-icon").attr("class","am-icon-angle-down");
				}
				loadInit();
			}
		</script>
	</body>
</html>